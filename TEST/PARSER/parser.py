import sys, os, re, hashlib, MySQLdb, codecs, hashlib, time, datetime
from urllib import unquote
from dateutil import parser
from BeautifulSoup import BeautifulSoup


comAppPat = re.compile(r'<link rel="canonical" href="/app/.+/([^"]+)">')


comAppNamePat1 = re.compile("<h1 class=\"item\"><img .*/>\n<span class=\"fn\">(.*)</span> </h1>",re.UNICODE|re.DOTALL)
comAppNamePat2 = re.compile("<h1><img .*/>([^\"]+) </h1>",re.UNICODE|re.DOTALL)

iconPat = re.compile("<h1 ?[^<]+<img src=\"(.*)\" style=\"float:left;margin-right:10px\"",re.UNICODE|re.DOTALL)

sizePat = re.compile(r'<span>(\d+) kb</span>')

#verPat = re.compile(r'Latest version: +(\d+\.?\d*)[\D]*(\d+\.?\d*)')    ##[0-9]+(?:\.[0-9]*)?
verPat = re.compile("Latest version: (.*) \(for Android version (.*) and higher(, supports App2SD)?\)",re.UNICODE|re.DOTALL)
varPat1 = re.compile("Latest version: (.*) \(for all Android versions(, supports App2SD)?\)",re.UNICODE|re.DOTALL)

#categPat = re.compile(r'&#187; <a href="/apps/[^/]+/([^"]+)"')
categPat = re.compile("&#187; <a href=\"/apps/[^/]+/[^/]+\">(.*)</a> &#187;",re.UNICODE|re.DOTALL)
devPat = re.compile(r'<a href="/browse/dev/([^"]+)"')
cPat = re.compile(r'\\\"([a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})\\\"')
pgp = re.compile(r'permission-group\.(\w+)\\')
pp = re.compile(r'permission\.(\w+)\\')
relatedComIdPat = re.compile(r'/app/([^"]+)/([^"]+)')
youtubePat = re.compile(r'http://www.youtube.com/embed/([\w\d]+)\?wmode=opaque')

jsPat = re.compile("window.pageData = \"(.*)\";",re.UNICODE|re.DOTALL)
officialPat = re.compile("android.permission.*((?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|])",re.UNICODE|re.DOTALL)

freePat = re.compile("<span class=('|\")priceFree('|\")>([^\"]+)</span>",re.UNICODE|re.DOTALL)

conn = MySQLdb.connect(host= "localhost",
	use_unicode = True, 
	charset = "utf8",
	user="root",
	passwd="iv@n1008",
	db="appstorez")

cursor = conn.cursor()
cursor.execute(r"SET NAMES utf8;")

def main(argv):
	path = "/home/login/Desktop/mirror/http/www.appbrain.com/app"
	#i = 0
	for (path, dirs, files) in os.walk(path):
		if files:
			try:
				file = codecs.open(os.path.join(path, files[0]), "r", "utf-8" )
				html = file.read()
			except IOError:
				print 'Cannot open', path, files[0]
				
			pageData = PageData(html)
		#i += 1
		#if i >= 300:
		#	break

	conn.close()

class PageData:
	def __init__(self, html):		

		##############DEVELOPER##############
		self.developer = ""
		self.developerUsername = ""
		self.officialPage = ""
		self.contact = ""
		self.password = ""
		self.emailsalt = ""
		self.usercreated = ""
		self.userID = ""
		self.userProfileID = ""
		self.userLogo = ""
		##############CATEGORY###############
		self.categoryID = ""
		self.categ = ""
		##############APPLICATION###############
		self.appHash = ""
		self.comAppName = ""
		self.appName = ""
		self.appID = ""			
		self.screenshots = []
		self.desc = ""		
		self.android_version = ""
		self.app2sd = ""				
		self.permissions = []
		self.permGroupID = ""
		self.permPermID = ""
		self.priceFree = ""
		self.pricePaid = ""
		self.price = ""
		
		self.MetaoperatingSystems = ""
		
		self.Metaimage = "0"
		self.icon = ""
		self.MetaratingValue = "0"
		self.rating = ""
		self.Metavotes = "0"
		self.votes = ""
		self.MetadatePublished = "0"
		self.added = ""	
		self.MetasoftwareVersion = "0"
		self.version = ""		
		self.MetanumDownloads = "0"
		self.installs = ""
		self.MetafileSize = "0"
		self.size = ""
		
		self.youtube = ""
		self.related = ""
		self.relatedAppHash = ""
		self.marketcomments = ""

		try:
			self.soup = BeautifulSoup(html)
		except Exception, e:
			print e
			return

		
		############## DEVELOPER 1. INSERT ##############
		# Contact
		cMatch = cPat.search(html)
		if cMatch:
			self.contact = unicode(cMatch.group(1)).encode('utf-8')
			self.password = hashlib.sha512(self.contact).hexdigest()

			self.emailsalt = hashlib.md5(self.contact).hexdigest()

		else:
			self.contact = str(time.time()) + "@email.com"
			self.password = hashlib.sha512("DevElOpeR").hexdigest()

			self.emailsalt = hashlib.md5("unknown@email.com").hexdigest()
			
		# Developer
		devMatch = devPat.search(html)
		if devMatch:
			self.developer = unicode(devMatch.group(1)).encode('utf-8')	
			devCheck = 1
		else:
			self.developer = str(time.time()) + "NONE"
			self.developerUsername = str(time.time()) + "NONE"
			devCheck = 0
			
		if self.emailsalt != "6b08a0d645c12fdaf167c5b54436338b":
			self.developerUsername = self.contact
		elif devCheck != 0 and self.emailsalt == "6b08a0d645c12fdaf167c5b54436338b":
			self.developerUsername = self.unquote_u(self.developer)
		else:
			self.developerUsername = "Unknown"
				
		# OfficialPage		
		jsMatch = jsPat.search(html)
		if jsMatch:		
			offMatch = officialPat.search(jsMatch.group(1))
			if offMatch:
				self.officialPage = offMatch.group(1)
			else:
				self.officialPage = "NONE"
		else:
			self.officialPage = "NONE"
				
		self.usercreated = time.strftime('%Y-%m-%d %H:%M:%S')
		self.userLogo = hashlib.md5("DEFAULT").hexdigest()

		############## DEVELOPER INSERT  ###################
		try:
			cursor.execute(r"""SELECT user_id  FROM `users` WHERE `user_username` = %s LIMIT 1;""",(self.developerUsername))
			
			if cursor.rowcount == 0:
				try:
					cursor.execute(r"""INSERT IGNORE INTO `appstorez`.`users` 
					(`user_id`, `user_type`, `user_username`, `user_password`, `user_email`, `user_verified`, `user_salt`, `user_lastIP`, `user_created`, `user_accessed`) VALUES 
					(NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s);""",('6', self.developerUsername, self.password, self.contact, '0', self.emailsalt, '1308875047', self.usercreated, self.usercreated,))
					conn.commit()
					self.userID = cursor.lastrowid
					if self.userID != "":
						try:										
							cursor.execute(r"""INSERT IGNORE INTO `appstorez`.`users_profile` 
							(`user_prof_id`, `user_prof_guid`, `user_prof_name`, `user_prof_surname`, `user_prof_country`, `user_prof_city`, `user_prof_age`, `user_prof_gender`, `user_prof_postal_code`, `user_prof_address`, `user_prof_phone`, `user_prof_mobile`, `user_prof_web`, `user_prof_logo`, `user_prof_devices`, `user_prof_payment_type`, `user_prof_earned`) VALUES 
							(NULL, %s, %s, '0', '0', '0', '0', '0', '0', '0', '0', '0',  %s, %s, 'NULL', '0', '0');""",(self.userID, self.developer, self.officialPage, self.userLogo))
							conn.commit()
							self.userProfileID = cursor.lastrowid
							
						except MySQLdb.Error, e:
							print "Error %d: %s" % (e.args[0], e.args[1])
					else:
						return
							
				except MySQLdb.Error, e:
					print "Error %d: %s" % (e.args[0], e.args[1])
					return
			else:
				self.userID = cursor.fetchone()[0]
		except MySQLdb.Error, e:
			print "Error %d: %s" % (e.args[0], e.args[1])
			return
		############## DEVELOPER END ###################
		
		############## CATEGORY 2. INSERT ##############
		# Category
		categMatch = categPat.search(html)
		if categMatch:
			self.categ = unicode(categMatch.group(1)).encode('utf-8')
			self.categ = self.unquote_u(self.categ)
		else:
			self.categ = "Uncategorised"	
		
		try:
			cursor.execute(r"""SELECT cat_id FROM `list_categories` WHERE `cat_name` = %s LIMIT 1;""",(self.categ))
			
			if cursor.rowcount == 0:
				try:
					cursor.execute(r"""INSERT INTO `appstorez`.`list_categories` (`cat_id`, `cat_parent`, `cat_name`) VALUES 
								(NULL, (SELECT MAX(cat_id+1) FROM `list_categories` as maxid), %s)""",(self.categ))
					conn.commit()
					self.categoryID = cursor.lastrowid
					
				except MySQLdb.Error, e:
					print "Error %d: %s" % (e.args[0], e.args[1])
			else:
				self.categoryID = cursor.fetchone()[0]
					
		except MySQLdb.Error, e:
			print "Error %d: %s" % (e.args[0], e.args[1])
			
		############## CATEGORY 2. END ###################
		
		############## APPLICATION 3.INSERT ##############		
		#Com App Name & Hash
		comAppName = comAppPat.search(html)
		if comAppName:
			self.comAppName = unicode(comAppName.group(1)).encode('utf-8')
			self.appHash = hashlib.md5(self.comAppName).hexdigest()
		else:
			print "#####***No ComApp Name!***#####"
			return
		print "*******************************"
		print self.comAppName
		
		##### Catch Meta Values
		self.CatchMeta()
		
		# App Name
		appNameMatch = comAppNamePat1.search(html)
		if appNameMatch:
			self.appName = unicode(appNameMatch.group(1)).encode('utf-8')
		else:
			appNameMatch = comAppNamePat2.search(html)
			if appNameMatch:
				self.appName = unicode(appNameMatch.group(1)).encode('utf-8')
			else:
				self.appName = "NONE"
						
		# App Info => INSTALLS, VOTES, RATING, SIZE
		appInfo = self.soup.find('div', attrs={'class': 'appInfo'})
		
		if appInfo:	
			if self.MetanumDownloads == "0":
				installsElem = appInfo.find('b')
				if installsElem: 
					self.installs = unicode(installsElem.string).encode('utf-8')
				else:
					self.installs = "0"
			else:
				self.install = self.MetanumDownloads 
			
			if self.Metavotes == "0":
				votes = appInfo.find('span', attrs={'class': 'votes'})
				if votes: 
					self.votes = unicode(votes.string).encode('utf-8')
				else:
					self.votes = "0"
			else:
				self.votes = self.Metavotes
			
			if self.MetaratingValue == "0":
				rating = appInfo.find('span', {'class': 'rating'})
				if rating: 
					self.rating = unicode(rating.string).encode('utf-8')
				else:
					self.rating = "0"
			else:
				self.rating = self.MetaratingValue
		
		if self.MetafileSize == "0":
			sizeMatch = sizePat.search(html)
			if sizeMatch:
				self.size = unicode(sizeMatch.group(1)).encode('utf-8')
			else:
				self.size = "0"
		else:
			self.size = self.MetafileSize

		# Description
		appDesc = self.soup.find('div', attrs={'id': 'description_inner'})

		if appDesc:			
			self.desc = ''.join(appDesc.findAll(text=True))
			self.desc = self.desc.strip()
		else:
			self.desc = "NONE"

		## Version Search 2 Patterns

		verMatch = verPat.search(html)
		if verMatch:
			self.version = unicode(verMatch.group(1)).encode('utf-8')
			self.android_version = unicode(verMatch.group(2)).encode('utf-8')
			if verMatch.group(3):
				self.app2sd = "1"
			else:
				self.app2sd = "0"
		else:
			verMatch1 = varPat1.search(html)
			if verMatch1:
				self.version = unicode(verMatch1.group(1)).encode('utf-8')
				self.android_version = "0"
				
				if verMatch1.group(2):
					self.app2sd = "1"
				else:
					self.app2sd = "0"
			else:	
				self.version = "0"
				self.android_version = "0"
				self.app2sd = "0"
		
		if self.MetasoftwareVersion != "0":
			self.version = self.MetasoftwareVersion
			
		# Added
		if self.MetadatePublished == "0":
			addedUl = self.soup.find('ul', attrs={'class': 'clList'})
			if addedUl:
				addedLi = addedUl.find('li').contents[1]
				if addedLi:
					lastversiontime = unicode(addedLi.string).encode('utf-8')
					try:
						self.added = time.strptime(lastversiontime,"%b %d, %Y")
						self.added = time.strftime("%Y-%m-%d %H:%M:%S", self.added)
					except ValueError, e:
						try:
							addedLi = addedUl.find('li').contents[2]
							if addedLi:
								lastversiontime = unicode(addedLi.string).encode('utf-8')
								try:
									self.added = time.strptime(lastversiontime,"%b %d, %Y")
									self.added = time.strftime("%Y-%m-%d %H:%M:%S", self.added)
								except ValueError, e:
									try:
										addedLi = addedUl.find('li').contents[3]
										if addedLi:
											lastversiontime = unicode(addedLi.string).encode('utf-8')
											try:
												self.added = time.strptime(lastversiontime,"%b %d, %Y")
												self.added = time.strftime("%Y-%m-%d %H:%M:%S", self.added)
											except ValueError, e:
												self.added = time.strftime('%Y-%m-%d %H:%M:%S')
												
									except ValueError, e:
										self.added = time.strftime('%Y-%m-%d %H:%M:%S')
						except ValueError, e:
							self.added = time.strftime('%Y-%m-%d %H:%M:%S')											
						
			else:
				self.added = time.strftime('%Y-%m-%d %H:%M:%S')
		else:
			self.added = self.MetadatePublished
					
		# Icon
		if self.Metaimage == "0":
			iconMatch = iconPat.search(html)
			if iconMatch: 
				self.icon = unicode(iconMatch.group(1)).encode('utf-8')
			else:
				self.icon = "NONE"
		else:
			self.icon = self.Metaimage

		# Price Free
		
		freematch = freePat.search(html)
		if freematch: 
			self.priceFree = "FREE"
		else:
			self.priceFree = "NONE"
		
		# Price Paid
		paid = self.soup.find('span', attrs={'class': 'pricePaid'})
		if paid: 
			self.pricePaid = unicode(paid.string).encode('utf-8')
		else:
			self.pricePaid = "NONE"
		
		if self.priceFree == "NONE" and self.pricePaid == "NONE":
			self.price = "0"
		elif self.priceFree == "NONE" and self.pricePaid != "NONE":
			self.price = self.pricePaid
		elif self.pricePaid == "NONE" and self.priceFree != "NONE":
			self.price = "0"
		else:
			self.price = "0"
		
		# Youtube
		youtubeMatch = youtubePat.search(html)
		if youtubeMatch:
			self.youtube = unicode(youtubeMatch.group(1)).encode('utf-8')
		else:
			self.youtube = "0"
		try:
			cursor.execute(r"""SELECT app_id FROM `apps` WHERE `app_hash` = %s LIMIT 1;""",(self.appHash))
			
			if cursor.rowcount == 0:
				try:
					cursor.execute(r"""INSERT IGNORE INTO `appstorez`.`apps` (`app_id`, `dev_id`, `app_hash`, `app_com`, `app_name`, `app_size`, `app_installs`, `app_rating`, `app_rating_avg`, `app_description`, `app_youtube_id`, `app_version`, `app_App2SD`, `app_os`, `app_os_version`, `app_added`, `app_category`, `app_icon`, `app_price`) VALUES
					(NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);""",(self.userID, self.appHash, self.ESC(self.comAppName), self.ESC(self.appName), self.ESC(self.size), self.ESC(self.installs), self.ESC(self.votes), self.ESC(self.rating), self.ESC(self.desc), self.ESC(self.youtube), self.ESC(self.version), self.app2sd, '1', self.ESC(self.android_version), self.added, self.categoryID, self.ESC(self.icon), self.ESC(self.price)))

					conn.commit()			
					self.appID = cursor.lastrowid
					appInserted = 1
				except MySQLdb.Warning, e:
					self.printAll()
					sys.exit()
					
				except MySQLdb.Error, e:
					print "Error %d: %s" % (e.args[0], e.args[1])
					return
			else:
				appInserted = 0

		except MySQLdb.Error, e:
			print "Error %d: %s" % (e.args[0], e.args[1])
			return	
		########### APPLICATION INSERT END ########### 
		
		########### IMAGES INSERT START ############## 
		if appDesc:
			images = appDesc.find('div', attrs={'class': 'screenshots'})
			if images:
				a = images.findAll('img')
				s = ""
				for img in a:
					self.screenshots = unicode(img['src']).encode('utf-8')
					if appInserted == 1:
						try:
							cursor.execute(r"""INSERT INTO `appstorez`.`apps_images` (`id`, `apps_id`, `image_link`) VALUES 
												(NULL, %s, %s);""",(self.appID, self.screenshots))
							conn.commit()						
						except MySQLdb.Error, e:
							print "Error %d: %s" % (e.args[0], e.args[1])
		
		if not self.screenshots:
			self.screenshots = "0"
			
		########### IMAGES INSERT END ############## 

		########### RELATED APPS INSERT ############
		# Related Apps INESRT INTO NEW TABEL
		relatedLis = self.soup.findAll('a', attrs={'class': 'app-item'})
		relatedId = ""
		
		for li in relatedLis:
			relatedLi = unicode(li['href']).encode('utf-8')
			relatedComIdMatch = relatedComIdPat.search(relatedLi)
			if relatedComIdMatch:
				self.related = relatedComIdMatch.group(2)
				self.relatedAppHash = hashlib.md5(relatedComIdMatch.group(2)).hexdigest()
				if appInserted == 1:	
					try:
						cursor.execute(r"""INSERT INTO `appstorez`.`apps_related` (`related_id`, `app_id`, `related_app_hash`) VALUES 
						(NULL, %s, %s);""",(self.appID, self.relatedAppHash))					
						conn.commit()						
					except MySQLdb.Error, e:
						print "Error  %d: %s" % (e.args[0], e.args[1])
		########### RELATED APPS END ############
	
		# Permissions
		if appInserted == 1:
			self.permissions = self.getPerms(html)
			s = ""
			for p in self.permissions:
				s += unicode(p).encode('utf-8')
			
			if s:
				self.permissions = s
			else:
				self.InsertPerms('0', '0', '0')

		#Market Comments
		#self.marketcomments = self.Comments(html)

		#self.printAll()
		#self.insert()
	
	def CatchMeta(self):		
		count = 0
		
		self.Metaimage = self.soup.find("meta", attrs={"itemprop":"image"})
		if self.Metaimage:
			self.Metaimage = self.Metaimage['content']
			count += 1
		else:
			self.Metaimage = "0"
			
		self.MetaratingValue = self.soup.find("div", attrs={"class":"ratings rating"})
		if self.MetaratingValue:
			self.MetaratingValue = self.MetaratingValue['content']
			count += 1
		else:
			self.MetaratingValue = "0"
			
		self.Metavotes = self.soup.find("meta", attrs={"class":"votes"})
		if self.Metavotes:
			self.Metavotes = self.Metavotes['content']
			count += 1
		else:
			self.Metavotes = "0"
			
		self.MetadatePublished = self.soup.find("meta", attrs={"itemprop":"datePublished"})
		if self.MetadatePublished:
			self.MetadatePublished = self.MetadatePublished['content']
			self.MetadatePublished = time.strptime(self.MetadatePublished, "%Y-%m-%d")
			self.MetadatePublished = time.strftime("%Y-%m-%d %H:%M:%S", self.MetadatePublished)
			count += 1
		else:
			self.MetadatePublished = "0"
			
		self.MetasoftwareVersion = self.soup.find("meta", attrs={"itemprop":"softwareVersion"})
		if self.MetasoftwareVersion:
			self.MetasoftwareVersion = self.MetasoftwareVersion['content']
			count += 1
		else:
			self.MetasoftwareVersion = "0"
			
		self.MetaoperatingSystems = self.soup.find("meta", attrs={"itemprop":"operatingSystems"})
		if self.MetaoperatingSystems:
			self.MetaoperatingSystems = self.MetaoperatingSystems['content']
			count += 1
			if self.MetaoperatingSystems == 'Android':
				self.MetaoperatingSystems = '0'
		else:
			self.MetadatePublished = "0"
			
		self.MetanumDownloads = self.soup.find("meta", attrs={"itemprop":"numDownloads"})
		if self.MetanumDownloads:
			self.MetanumDownloads = self.MetanumDownloads['content']
			count += 1
		else:
			self.MetanumDownloads = "0"
			
		self.MetafileSize = self.soup.find("meta", attrs={"itemprop":"fileSize"})
		if self.MetafileSize:
			self.MetafileSize = self.MetafileSize['content']
			self.MetafileSize = self.MetafileSize[:-1]
			count += 1
		else:
			self.MetafileSize = "0"
			
		if count >= 4:
			try:
				cursor.execute(r"""INSERT IGNORE INTO `appstorez`.`apps_meta` (`apps_meta_id`, `app_hash`, `apps_meta_image`, `apps_meta_published`, `apps_meta_version`, `apps_meta_os`, `apps_meta_no_download`, `apps_meta_filesize`, `apps_meta_rating`, `apps_meta_votes`) VALUES 
				(NULL, %s, %s, %s, %s, %s, %s, %s, %s, %s);""",(self.appHash, self.Metaimage, self.MetadatePublished, self.MetasoftwareVersion, self.MetaoperatingSystems, self.MetanumDownloads, self.MetafileSize, self.MetaratingValue, self.Metavotes))					
				conn.commit()						
			except MySQLdb.Error, e:
				print "Error  %d: %s" % (e.args[0], e.args[1])
		
		"""
		print "MATA INFO: "
		print 'image: ', self.Metaimage
		print 'datePublished: ', self.MetadatePublished
		print 'softwareVersion: ', self.MetasoftwareVersion
		print 'operatingSystems: ', self.MetaoperatingSystems
		print 'numDownloads: ', self.MetanumDownloads
		print 'fileSize: ', self.MetafileSize
		"""
		
		

	
	def InsertPerms(self, group, perm, type):
		if type == '1':
			# Check if Group Exsist if not insert
			try:
				cursor.execute(r"""SELECT list_perm_id FROM `list_permissions` WHERE `list_perm_name` = %s LIMIT 1;""",(group))
				
				## If Group Not Exsist Insert
				if cursor.rowcount == 0:
					try:
						cursor.execute(r"""INSERT INTO `appstorez`.`list_permissions` (`list_perm_id`, `list_perm_parent`, `list_perm_name`, `list_perm_description`, `list_perm_operating_system`) VALUES 
				(NULL, %s, %s, %s, %s);""",('0', group, group, '0'))
						conn.commit()
						self.permGroupID = cursor.lastrowid
											
					except MySQLdb.Error, e:
						print "Error  %d: %s" % (e.args[0], e.args[1])
				else:
					self.permGroupID = cursor.fetchone()[0]					
						
			except MySQLdb.Error, e:
				print "Error %d: %s" % (e.args[0], e.args[1])
				
			# Check if Perm Exsist
			try:
				cursor.execute(r"""SELECT list_perm_id FROM `list_permissions` WHERE `list_perm_name` = %s LIMIT 1;""",(perm))
				
				if cursor.rowcount > 0:
					permPermID0 = cursor.fetchone()[0]
				else:
					permPermID0 = "0"
						
				
				## If Perm Not Exsist Insert
				if cursor.rowcount == 0:
					try:
						cursor.execute(r"""INSERT INTO `appstorez`.`list_permissions` (`list_perm_id`, `list_perm_parent`, `list_perm_name`, `list_perm_description`, `list_perm_operating_system`) VALUES 
				(NULL, %s, %s, %s, %s);""",(self.permGroupID, perm, perm, '0'))
						conn.commit()
						self.permPermID = cursor.lastrowid
											
					except MySQLdb.Error, e:
						print "Error  %d: %s" % (e.args[0], e.args[1])
				else:
					self.permPermID = permPermID0		
								
						
			except MySQLdb.Error, e:
				print "Error %d: %s" % (e.args[0], e.args[1])
				
			# After Group And Perm Insert in App Details
			try:
				#print "Group: ", self.permGroupID
				#print "Perm: ", self.permPermID
				
				cursor.execute(r"""INSERT INTO `appstorez`.`apps_permissions` (`perm_id`, `apps_id`, `perm_parent`, `perm_child`) VALUES 
				(NULL, %s, %s, %s);""",(self.appID, self.permGroupID, self.permPermID))
				conn.commit()
											
			except MySQLdb.Error, e:
				print "Error  %d: %s" % (e.args[0], e.args[1])			
				
				
	def unquote_u(self, source):
		result = unquote(source)
		if '%u' in result:
			result = result.replace('%u','\\u').decode('unicode_escape')
		if '+' in result:
			result = result.replace('+','').decode('unicode_escape')
			
		return result
    
	def Comments(self, html):
		data = ""		
		data = self.soup.findAll('div', attrs={'class': 'comment'})
		for da in data:
			relatedLi = da.soup.find('div', attrs={'class': 'c_rating'})
			print relatedLi			
		return

	def getPerms(self, text):
		groups = []
		perms = []
		groupInfos = []
		index = 0

		# Find all permission groups and their Names and End_Indexes
		while True:
			pgm = pgp.search(text, index)
			if pgm:
				groupInfos.append( [pgm.group(1), pgm.end()] )
				#print pgm.group(1)
				index = pgm.end()
			else: break
		ln = len(groupInfos)
		for i in xrange(ln):
			if i < ln -1:
				groupInfos[i].append( groupInfos[i+1][1] )		
				
			else:
				groupInfos[i].append( len(text)-1 )
				
			

		# Then look for all permissions for each group
		for g in groupInfos:
			start = g[1]
			end = g[2]
			perms = []
			while True:
				pm = pp.search(text, start, end)
				if pm:
					## GROUP print g[0]
					perms.append( pm.group(1) )					
					## PERM print pm.group(1)
					if g[0] or pm.group(1):
						self.InsertPerms(g[0], pm.group(1), '1')
					start = pm.end()
				else:
					break
			groups.append( (g[0], perms) )
			

		return groups

	def printAll(self):
		print "***********************************************"
		print "COM Name:", self.comAppName
		print "Hash:", self.appHash
		print "App Name:", self.appName		
		print 'Installs: ', self.installs
		print 'Votes: ', self.votes
		print 'Rating: ', self.rating
		print 'Size: ', self.size		
		#print 'Screenshots: ', self.screenshots
		print 'Desc: ', self.desc		
		print 'Version: ', self.version
		print 'Android Version: ', self.android_version
		print 'App2SD: ', self.app2sd		
		print 'Added: ', self.added
		print 'Category: ', self.categ		
		print 'Icon: ', self.icon
		print 'Developer: ', self.developer
		print 'Price Free: ', self.priceFree
		print 'Price Paid: ', self.pricePaid				
		print 'Official Page: ', self.officialPage
		print 'Contact: ', self.contact
		print 'Youtube: ', self.youtube			
		#self.related	
		#print 'Permissions: ', self.permissions
		
	def ESC(self, string):
		string = string.strip()
		string = str(MySQLdb.escape_string(string))
		return string
		

if __name__ == '__main__':
	main(sys.argv)
