    <?php
    function strStripWhitespace($str) {
    return str_replace(' ', '', $str);
    }
    function regexStripExtraWhitespace($str) {
    return preg_replace('/\s*/m', ' ', $str);
    }
    function loopStripExtraWhitespace($str) {
    $len = strlen($str);
    for($i = 0; $i < $len; $i++) {
    $newstr .= substr($str, $i, 1);
    while(substr($str, $i + 1, 1) == ' ') {
    $i++;
    } 
    }
    return $newstr;
    }
    function loopStripExtraWhitespaceOptimized($str) {
    $len = strlen($str);
    for($i = 0; $i < $len; $i++) {
    $newstr .= $str[$i];
    while($str[$i] == ' ') {
    $i++;
    }
    }
    return $newstr;
    }
    function loopStripExtraWhitespace2($str) {
    while($str != ($_str = str_replace(' ', ' ', $str))) {
    $str = $_str;
    }
    return $str;
    }
    function benchMark($function, $arg = null, $loops = 1000) {
    $start = microtime(1);
    for($i = 0; $i < $loops; $i++) {
    $function($arg);
    }
    return microtime(1)-$start;
    }
    ini_set('max_execution_time', 0);
    $str = "\t hello this is a string.\r\n";
    for($i = 0; $i < 10; $i++) {
    $str .= $str;
    }
    echo benchMark('strStripWhitespace', $str, 1000);
    echo '<br />';
    echo benchMark('regexStripExtraWhitespace', $str, 1000);
    echo '<br />';
    echo benchMark('loopStripExtraWhitespace', $str, 1000);
    echo '<br />';
    echo benchMark('loopStripExtraWhitespaceOptimized', $str, 1000);
    echo '<br />';
    echo benchMark('loopStripExtraWhitespace2', $str, 1000);
    echo '';
    ?>