<?php 
/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
require ('../../include/config.inc.php');
include ROOT.'include/functions.inc.php';

/*==================================================*/
/*=========       CSS/JS Minify Include  ===========*/
/*=== min/?f=template/js/jquery.js,scripts/site.js==*/
/*=== min/?f=template/css/LogReg/login.css        ==*/
/*==================================================*/
require ROOT.'/min/utils.php';
$cssUri = Minify_getUri(array(
     '//template/css/LogReg/login.css'
    ,'//template/css/captcha.css'
)); // a list of files


/*============================*/
/*=== Get Language From Url ==*/
/*============================*/
if($_GET['lng'] && $_GET['lng'] != ''){
    if(strlen(trim($_GET['lng'])) >= 4){
        $siteLanguage = CleanUrlData($_GET['lng']);
    }else{
        $siteLanguage = 'GB';
    }
}
/*============================*/
/*=== Get Country  From Url ==*/
/*============================*/
if($_GET['c'] && $_GET['c'] != ''){
    if(strlen(trim($_GET['c'])) >= 4){
        $country = CleanUrlData($_GET['c']);        
    }else{
        $country = 'United Kingdom';
    }        
}
/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
        <link rel="stylesheet" href="<?php echo $cssUri; ?>" media="all" />       
        <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/modernizr.js"></script>
        <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script>window.jQuery || document.write(' <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/jquery.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
        <script>window.jQuery || document.write(' <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/jquery-ui.min.js"><\/script>')</script>
        <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/LogReg/jquery.inputfocus-0.9.min.js"></script>
        <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/LogReg/loginsteps.php?lng=<?=$siteLanguage?>&amp;c=<?=urlencode($country)?>"></script>
</head>
<body>
	
<div id="container">
<form action="<?php echo WEB_URL; ?>" method="post">
    <!-- #first_step -->
    <div id="first_step">
        <h1><?php echo LOGIN_H1_1; ?><span><?php echo WEB_NAME; ?></span><?php echo LOGIN_H1_2; ?></h1>

        <div class="form">
            <input class="form_user_field" type="text" name="username" id="username" value="<?php echo USERNAME; ?>" />
            <label class="form_user_label_field" for="username"><?php echo LOGIN_UNAME_MESS; ?></label>
            <div class="clear"></div>
            <input class="form_user_field" type="password" name="password" id="password" value="<?php echo PASSWORD; ?>" />
            <label class="form_user_label_field" for="password"><?php echo LOGIN_PASSW_MESS; ?></label>
            <div class="clear"></div>
            <table align="center">
                <tr>
                    <td width="300" align="left" style="text-align: left; display:table-cell; vertical-align:middle;">
                        <span id="loginerror" style="display: none;"></span>
                    </td>
                    <td width="55" align="right" style="text-align: right; display:table-cell; vertical-align:middle; padding-right: 5px;">
                        <input type="checkbox" checked="checked" name="rememberme" id="rememberme" value="1" />
                    </td>
                    <td align="left" style="text-align: left;">
                        <label class="form_user_label_field" for="rememberme"><?php echo LOGIN_REMEM_MESS; ?></label>
                    </td>
                </tr>
            </table>   
            <div id="captcha-outwrap"></div>
        <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
        <input class="send submit" type="submit" name="user_done" id="user_done" value="" /> 
        </div>      
 
    </div>      
    <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
</form>
</div>
<div id="progress_bar">
<div id="progress"></div>
<div id="progress_text">0% <?php echo PERCENTAGE; ?></div>
</div>
</body>
</html>