<?php
/*===============================*/
/*===  Include Global Config ====*/
/*===============================*/
require ('../../include/config.inc.php');
require (ROOT.'include/databse.inc.php');
/*=======================*/
/*===  DB Connection ====*/
/*=======================*/
$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect(); 

/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
include ROOT.'include/functions.inc.php';

/*============================*/
/*=== Get UNIQUE user Salt ===*/
/*============================*/

if($_GET['salt'] && $_GET['salt'] != ''){
    if(strlen(trim($_GET['salt'])) == 32){
        /*============================*/
        /*=== Populate User Object ===*/
        /*============================*/
        $data_users = array();
        $data_users = userObject($_GET['salt']);
        
        /*===================================================*/
        /*=== How Many Days has passed since registration ===*/
        /*===================================================*/
        if($data_users != false){
            /*==============================================*/
            /*=== Get Registered and NOW time in seconds ===*/
            /*==============================================*/
            $data_users['strtotime_created'] = strtotime($data_users['user_created']);
            $data_users['strtotime_now'] = time();
            /*============================*/
            /*=== Calculate Difference ===*/
            /*============================*/
            $data_users['strtotime_diff'] = $data_users['strtotime_created'] - $data_users['strtotime_now'];            
            $data_users['strtotime_passed'] = abs(floor($data_users['strtotime_diff'] / 86400));
        }

    }else{
        $data_users = false;
    }        
}else{
    $data_users = false;
}

/*===================================*/
/*=== If no User or X days passed ===*/
/*===================================*/
if($data_users == FALSE || $data_users['strtotime_passed'] > USER_ACTIVATE){
    echo "Unknown User!";
    exit(0);
}else{
    $data = array();
    $data['verified'] = 1;
    $data['accessed'] = 'NOW()';
    $data['lastIP'] = "INET_ATON(".$_SERVER['REMOTE_ADDR'].")";    
    $db->update(TABLE_USERS, $data, "id='".$data_users['user_id']."'");
    echo '<meta http-equiv="refresh" content="0;url='.WEB_URL.'">';
    exit(0);
}

?>