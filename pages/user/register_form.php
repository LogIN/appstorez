<?php 
/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
require ('../../include/config.inc.php');

/*=======================*/
/*===  DB Connection ====*/
/*=======================*/
require (ROOT.'include/databse.inc.php');
$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect(); 

include ROOT.'include/functions.inc.php';

/*============================*/
/*=== Get Language From Url ==*/
/*============================*/
if($_GET['lng'] && $_GET['lng'] != ''){
    if(strlen(trim($_GET['lng'])) >= 4){
        $siteLanguage = CleanUrlData($_GET['lng']);
    }else{
        $siteLanguage = 'GB';
    }
}
/*============================*/
/*=== Get Country  From Url ==*/
/*============================*/
if($_GET['c'] && $_GET['c'] != ''){
    if(strlen(trim($_GET['c'])) >= 4){
        $country = CleanUrlData($_GET['c']);        
    }else{
        $country = 'United Kingdom';
    }        
}
/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}
/*==================================================*/
/*=========       CSS/JS Minify Include  ===========*/
/*=== min/?f=template/js/jquery.js,scripts/site.js==*/
/*=== min/?f=template/css/LogReg/login.css        ==*/
/*==================================================*/
require ROOT.'/min/utils.php';
$cssUri = Minify_getUri(array(
     '//template/css/LogReg/register.css'
)); // a list of files

?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
        <link rel="stylesheet" href="<?php echo $cssUri; ?>" type="text/css" media="all" />
        <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/modernizr.js"></script>
        <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
        <script>window.jQuery || document.write(' <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/jquery.min.js"><\/script>')</script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.6/jquery-ui.min.js"></script>
        <script>window.jQuery || document.write(' <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/jquery-ui.min.js"><\/script>')</script>
        <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/LogReg/jquery.inputfocus-0.9.min.js"></script>
        <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/LogReg/registersteps.php?lng=<?=$siteLanguage?>&amp;c=<?=urlencode($country)?>"></script>
</head>
<body>
	
<div id="container">
<form action="#" method="post">

    <!-- #first_step -->
    <div id="first_step">
        <h1>Register free <span><?php echo WEB_NAME; ?></span> ACCOUNT</h1>

        <div class="form">
            <input type="image" src="<?php echo WEB_URL; ?>template/img/LogReg/form/user.png" id="user" name="user" value="user" />
            <input style="margin-left: 5px;" type="image" src="<?php echo WEB_URL; ?>template/img/LogReg/form/developers.png" id="developer" name="developer" value="developer" />
            <div class="clear"></div>
            <table align="center" width="620" align="center">
                <tr>
                    <td width="310" align="center" style="text-align: center; display:table-cell; vertical-align:middle;">
                        <span>USER</span>
                    </td>
                    <td width="310" align="center" style="text-align: center; display:table-cell; vertical-align:middle;">
                        <span>DEVELOPER</span>
                    </td>
                </tr>
            </table>   
        </div>      
    </div>      
    <!-- clearfix --><div class="clear"></div><!-- /clearfix -->

    <!-- #second_step -->
    <div id="second_step">
        <div class="form">
            <h1>Register free <span><?php echo WEB_NAME; ?></span> ACCOUNT</h1>
                <input type="text" name="username" id="username" value="username" />
                <label for="username">At least 4 characters. Uppercase letters, lowercase letters and numbers only.</label>
                <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input type="password" name="password" id="password" value="password" />
                <label for="password">At least 4 characters. Use a mix of upper and lowercase for a strong password.</label>
                <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input type="password" name="cpassword" id="cpassword" value="password" />
                <label for="cpassword">If your passwords aren't equal, you won’t be able to continue with signup.</label>
                <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
                <input type="email" name="email" id="email" value="email address" />
                <label for="email">Your email address. We send important administration notices to this address</label>        
                <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <input class="submit" type="submit" name="submit_second" id="submit_second" value="" />
        </div>
    </div>      
    <!-- clearfix --><div class="clear"></div><!-- /clearfix -->


    <!-- #third_step -->
    <div id="third_step">
        <div class="form">
            <h1>Register free <span><?php echo WEB_NAME; ?></span> ACCOUNT</h1>

            <input type="text" name="name" id="name" value="first name" />
            <label for="name">Please enter your name.</label>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <input type="text" name="surname" id="surname" value="last name" />
            <label for="surname">At least 2 characters.</label>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <input type="number" name="age" id="age" value="" />
            <label for="age">Your current age</label>        
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <select id="gender" name="gender">
                <option>Male</option>
                <option>Female</option>
            </select>
            <label for="gender">Your Gender. </label> 
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <input class="submit" type="submit" name="submit_third" id="submit_third" value="" />
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
        </div>
    </div> 


    <!-- #fourth_step -->
    <div id="fourth_step">
        <div class="form">
            <h1>Register free <span><?php echo WEB_NAME; ?></span> ACCOUNT</h1>

            <select id="country" name="country"><?php print_r(countryDropDown($country)); ?></select>
            <label for="country">Your country. </label>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <input type="text" name="city" id="city" value="city" />
            <label for="city">Your City!</label>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <input type="number" name="postal" id="postal" value="postal" />
            <label for="postal">Postal Code!</label>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <div id="adress_placeholder"></div>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <input class="submit" type="submit" name="submit_fourth" id="submit_fourth" value="" />
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
        </div>
    </div>
    
    <div id="fifth_step">
        <div class="form">
            <h1>Register free <span><?php echo WEB_NAME; ?></span> ACCOUNT</h1>
            
            <input type="tel" name="phone" id="phone" value="phone" />
            <label for="phone">Phone Number!</label>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            
            <input type="tel" name="mobile" id="mobile" value="mobile" />
            <label for="mobile">Mobile number!</label>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            
            <input type="url" name="web" id="web" value="web" />
            <label for="web">Web Adress!</label>
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
         
            <input class="submit" type="submit" name="submit_fifth" id="submit_fifth" value="" />
            <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
        </div>
    </div>
    
    <div id="user_done">
        <div class="form">
            <h1>USER Registration DONE!</h1>

            <div class="form">
                <h2>Summary</h2>
                <table id="user_done_table">
                    <tr>
                        <td>Username</td>
                        <td id="Username"></td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td id="Password"></td>
                    </tr>
                    <tr>
                        <td>Email</td>
                        <td id="Email"></td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td id="Name"></td>
                    </tr>
                    <tr>
                        <td>Surname</td>
                        <td id="Surname"></td>
                    </tr>
                    <tr>
                        <td>Age</td>
                        <td id="Age"></td>
                    </tr>
                    <tr>
                        <td>Gender</td>
                        <td id="Gender"></td>
                    </tr>
                    <tr>
                        <td>Country</td>
                        <td id="Country"></td>
                    </tr>
                    <tr>
                        <td>City</td>
                        <td id="City"></td>
                    </tr>
                    <tr>
                        <td>Postal</td>
                        <td id="Postal"></td>
                    </tr>
                </table>
            </div>      <!-- clearfix --><div class="clear"></div><!-- /clearfix -->
            <input class="send submit" type="submit" name="user_done" id="user_done" value="" />   
        </div>
    </div>
</form>
    
</div>
<div id="progress_bar">
<div id="progress"></div>
<div id="progress_text">0% Complete</div>
</div>
</body>
</html>