<div id="nav_wrapper">
    <span id="menu_start"><a href="<?php echo WEB_URL;?>" class="home">Home</a></span>
    <ul id="topnav">	    	
        <li class="liSub">
            <a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES); ?>/1" title="<?php echo BROWSE." ".GAMES." ".APP; ?>">
                <span class="dd_wrapper">
                    <?php echo GAMES; ?>
                    <span class="dd_action"><img src="http://appstorez.com/template/img/topmenu/arrow.png" alt=""/></span>
                </span>
            </a>
            <div style="opacity: 0; display: none; width: 340px;" class="sub">
                <ul>                	
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_ARCADE); ?>/1" title="<?php echo BROWSE." ".GAMES_ARCADE." ".APP; ?>"><?php echo GAMES_ARCADE; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_BRAIN); ?>/1" title="<?php echo BROWSE." ".GAMES_BRAIN." ".APP; ?>"><?php echo GAMES_BRAIN; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_CARDS); ?>/1" title="<?php echo BROWSE." ".GAMES_CARDS." ".APP; ?>"><?php echo GAMES_CARDS; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_CASUAL); ?>/1" title="<?php echo BROWSE." ".GAMES_CASUAL." ".APP; ?>"><?php echo GAMES_CASUAL; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_WALLPAPER); ?>/1" title="<?php echo BROWSE." ".GAMES_WALLPAPER." ".APP; ?>"><?php echo GAMES_WALLPAPER; ?></a></li>                        
                </ul>
                <ul>                	
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_RACING); ?>/1" title="<?php echo BROWSE." ".GAMES_RACING." ".APP; ?>"><?php echo GAMES_RACING; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_SPORTS); ?>/1" title="<?php echo BROWSE." ".GAMES_SPORTS." ".APP; ?>"><?php echo GAMES_SPORTS; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_WIDGETS); ?>/1" title="<?php echo BROWSE." ".GAMES_WIDGETS." ".APP; ?>"><?php echo GAMES_WIDGETS; ?></a></li> 
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(GAMES)."/".SEO(GAMES_OTHER); ?>/1" title="<?php echo BROWSE." ".GAMES_OTHER." ".APP; ?>"><?php echo GAMES_OTHER; ?></a></li>                     
                </ul>                                
            </div>
        </li>
        <li class="liSub">
            <a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP); ?>" title="<?php echo BROWSE." ".APP; ?>/1">
                <span class="dd_wrapper">
                <?php echo APP; ?>
                <span class="dd_action"><img src="http://appstorez.com/template/img/topmenu/arrow.png" alt="" /></span>
                </span>
            </a>
            <div style="opacity: 0; display: none; width: 340px;" class="sub">
                <ul>                	
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_BOOKS); ?>/1" title="<?php echo BROWSE." ".APP_BOOKS." ".APP; ?>"><?php echo APP_BOOKS; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_BUSINESS); ?>/1" title="<?php echo BROWSE." ".APP_BUSINESS." ".APP; ?>"><?php echo APP_BUSINESS; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_COMICS); ?>/1" title="<?php echo BROWSE." ".APP_COMICS." ".APP; ?>"><?php echo APP_COMICS; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_COMMUNICATION); ?>/1" title="<?php echo BROWSE." ".APP_COMMUNICATION." ".APP; ?>"><?php echo APP_COMMUNICATION; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_EDUCATION); ?>/1" title="<?php echo BROWSE." ".APP_EDUCATION." ".APP; ?>"><?php echo APP_EDUCATION; ?></a></li>   
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_ENTERTAINMENT); ?>/1" title="<?php echo BROWSE." ".APP_ENTERTAINMENT." ".APP; ?>"><?php echo APP_ENTERTAINMENT; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_FINANCE); ?>/1" title="<?php echo BROWSE." ".APP_FINANCE." ".APP; ?>"><?php echo APP_FINANCE; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_HELTH); ?>/1" title="<?php echo BROWSE." ".APP_HELTH." ".APP; ?>"><?php echo APP_HELTH; ?></a></li> 
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_LIBRARIES); ?>/1" title="<?php echo BROWSE." ".APP_LIBRARIES." ".APP; ?>"><?php echo APP_LIBRARIES; ?></a></li>                       
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_LIFESTYLE); ?>/1" title="<?php echo BROWSE." ".APP_LIFESTYLE." ".APP; ?>"><?php echo APP_LIFESTYLE; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_WALLPAPER); ?>/1" title="<?php echo BROWSE." ".APP_WALLPAPER." ".APP; ?>"><?php echo APP_WALLPAPER; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_MEDIA); ?>/1" title="<?php echo BROWSE." ".APP_MEDIA." ".APP; ?>"><?php echo APP_MEDIA; ?></a></li> 
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_MEDICAL); ?>/1" title="<?php echo BROWSE." ".APP_MEDICAL." ".APP; ?>"><?php echo APP_MEDICAL; ?></a></li>  
                </ul>
                <ul>           	
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_MUSIC); ?>/1" title="<?php echo BROWSE." ".APP_MUSIC." ".APP; ?>"><?php echo APP_MUSIC; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_NEWS); ?>/1" title="<?php echo BROWSE." ".APP_NEWS." ".APP; ?>"><?php echo APP_NEWS; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_PERSONAL); ?>/1" title="<?php echo BROWSE." ".APP_PERSONAL." ".APP; ?>"><?php echo APP_PERSONAL; ?></a></li> 
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_PHOTOGRAPHY); ?>/1" title="<?php echo BROWSE." ".APP_PHOTOGRAPHY." ".APP; ?>"><?php echo APP_PHOTOGRAPHY; ?></a></li>                       
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_PRODUCTIVITY); ?>/1" title="<?php echo BROWSE." ".APP_PRODUCTIVITY." ".APP; ?>"><?php echo APP_PRODUCTIVITY; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_SHOOPING); ?>/1" title="<?php echo BROWSE." ".APP_SHOOPING." ".APP; ?>"><?php echo APP_SHOOPING; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_SOCIAL); ?>/1" title="<?php echo BROWSE." ".APP_SOCIAL." ".APP; ?>"><?php echo APP_SOCIAL; ?></a></li> 
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_SPORTS); ?>/1" title="<?php echo BROWSE." ".APP_SPORTS." ".APP; ?>"><?php echo APP_SPORTS; ?></a></li> 
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_TOOLS); ?>/1" title="<?php echo BROWSE." ".APP_TOOLS." ".APP; ?>"><?php echo APP_TOOLS; ?></a></li>                       
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_TRANSPORT); ?>/1" title="<?php echo BROWSE." ".APP_TRANSPORT." ".APP; ?>"><?php echo APP_TRANSPORT; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_TRAVEL); ?>/1" title="<?php echo BROWSE." ".APP_TRAVEL." ".APP; ?>"><?php echo APP_TRAVEL; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_WEATHER); ?>/1" title="<?php echo BROWSE." ".APP_WEATHER." ".APP; ?>"><?php echo APP_WEATHER; ?></a></li> 
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(APP)."/".SEO(APP_WIDGETS); ?>/1" title="<?php echo BROWSE." ".APP_WIDGETS." ".APP; ?>"><?php echo APP_WIDGETS; ?></a></li> 
                </ul>  
            </div>
        </li>
        <li class="liSub">
            <a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT; ?>">
                <span class="dd_wrapper">
                    <?php echo CAT; ?>
                    <span class="dd_action">
                        <img src="http://appstorez.com/template/img/topmenu/arrow.png" alt=""/>
                    </span>
                </span>
            </a>
            <div style="opacity: 0; display: none; width: 340px;" class="sub">
                <ul>                	
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT_T_P); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT_T_P; ?>"><?php echo CAT_T_P; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT_T_F); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT_T_F; ?>"><?php echo CAT_T_F; ?></a></li>
                    </ul>
                    <ul>                	
                    <!--
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT_M_V); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT_M_V; ?>"><?php echo CAT_M_V; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT_M_C); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT_M_C; ?>"><?php echo CAT_M_C; ?></a></li>
                    -->
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT_T_N_P); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT_T_N_P; ?>"><?php echo CAT_T_N_P; ?></a></li>
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT_T_N_F); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT_T_N_F; ?>"><?php echo CAT_T_N_F; ?></a></li>                      
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT_M_R); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT_M_R; ?>"><?php echo CAT_M_R; ?></a></li> 
                    <!--
                    <li><a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(APPS)."/".SEO(CAT_M_D); ?>/1" title="<?php echo BROWSE." ".BY." ".CAT_M_D; ?>"><?php echo CAT_M_D; ?></a></li>
                    -->
                </ul>                                
            </div>
        </li>
        <?php if ($user[isValid] === 1) { ?>
        <li>
            <a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(MY_APPS); ?>" title="<?php echo BROWSE." ".MY_APPS; ?>">        	
                <span class="single_wrapper"><?php echo MY_APPS; ?></span>
            </a>
        </li>
        <?php } ?>
        <!--
        <li>
            <a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(COMMUNITY); ?>" title="<?php echo COMMUNITY; ?>">        	
                <span class="single_wrapper"><?php echo COMMUNITY; ?></span>
            </a>
        </li>
        <li>
            <a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(PARTNERS); ?>" title="<?php echo PARTNERS; ?>">        	
                <span class="single_wrapper"><?php echo PARTNERS; ?></span>
            </a>
        </li>
        <li id="last-li">        	
            <a href="<?php echo WEB_URL.SEO($siteLanguage)."/".SEO(OUR_MARKET); ?>" title="<?php echo OUR_MARKET; ?>">        	
                <span class="single_wrapper"><?php echo OUR_MARKET; ?></span>
            </a>
        </li>
        -->
    </ul>
    <span id="menu_end"><img src="http://appstorez.com/template/img/topmenu/last_li.png" width="5" height="46" alt="" /></span>
</div>
