<div id="footer">
    <div id="footer-nav">
            <a href="<?php echo WEB_URL; ?>" title="Homepage">Home</a>     
            <span>&nbsp;|</span>  
            <a href="<?php echo WEB_URL; ?>" title="FAO">FAQ</a>
            <span>&nbsp;|</span>
            <a href="<?php echo WEB_URL; ?>" title="Partners">Partners</a>
            <span>&nbsp;|</span>
            <a href="<?php echo WEB_URL; ?>" title="Developers">Developers</a>
            <span>&nbsp;|</span>  
            <a href="<?php echo WEB_URL; ?>" title="Statistics">Statistics</a>  
            <span>&nbsp;|</span>
            <a href="<?php echo WEB_URL; ?>" title="Facebook">Facebook</a>  
            <span>&nbsp;|</span>
            <a href="<?php echo WEB_URL; ?>" title="Google+">Google+</a>  
            <span>&nbsp;|</span> 
            <a href="<?php echo WEB_URL; ?>" title="Twitter">Twitter</a> 
            <span>&nbsp;|</span>
            <a href="<?php echo WEB_URL; ?>" title="Terms of Use">Terms of Use </a> 
            <span>&nbsp;|</span>
            <a href="<?php echo WEB_URL; ?>" title="Privacy Policy">Privacy Policy</a>
            <span>&nbsp;|</span>
            <a href="<?php echo WEB_URL; ?>" title="Media">Media</a> 
            <span>&nbsp;|</span>    
            <a href="<?php echo WEB_URL; ?>" title="About">About</a>
            <span>&nbsp;|</span>
            <a href="<?php echo WEB_URL; ?>" title="Contact">Contact</a>
    </div>      
</div>