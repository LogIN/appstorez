<?php 
/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
require ('../../include/config.inc.php');

/*=======================*/
/*===  DB Connection ====*/
/*=======================*/
require (ROOT.'include/databse.inc.php');
$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect(); 

include ROOT.'include/functions.inc.php';
// Gloab Details Array
$user = array();
$page = array();
$page[limit] = "50";    
$page[salt] = "8809bef6b5c7ffcf41be279a127587f3";
/*============================*/
/*=== Get Language From Url ==*/    
/*============================*/
if($_GET['action'] && $_GET['action'] != ''){ $page[action] = CleanUrlData($_GET['action']); }else{ $page[action] = ''; }
if($_GET['tdlcat'] && $_GET['tdlcat'] != ''){ $page[category][0] = CleanUrlData($_GET['tdlcat']); }else{$page[category][0] = '1';}
if($_GET['cat'] && $_GET['cat'] != ''){ $page[category][1] = CleanUrlData($_GET['cat']); }else{$page[category][1] = '1';}
if($_GET['page'] && $_GET['page'] != ''){ $page[number] = CleanUrlData($_GET['page']); }else{$page[number] = '1';}

#var_dump($page);   
#exit(0);
/*============================*/
/*=== Get Current Page URL  ==*/    
/*============================*/
if(is_numeric($page[category][1])){
    $page[url] = WEB_URL.$_GET['lng']."/".$page[action]."/".$page[category][0]."/".$page[number];
    $page[url_no_number] = WEB_URL.$_GET['lng']."/".$page[action]."/".$page[category][0];
}elseif(is_numeric($page[category][0])){
    $page[url] = WEB_URL.$_GET['lng']."/".$page[action]."/".$page[category][1]."/".$page[number];
    $page[url_no_number] = WEB_URL.$_GET['lng']."/".$page[action]."/".$page[category][1];
}elseif(!is_numeric($page[category][1]) && !is_numeric($page[category][0])){
    $page[url] = WEB_URL.$_GET['lng']."/".$page[action]."/".$page[category][0]."/".$page[category][1]."/".$page[number];
    $page[url_no_number] = WEB_URL.$_GET['lng']."/".$page[action]."/".$page[category][0]."/".$page[category][1];
}else{
   $page[url] = WEB_URL.$_SERVER['REQUEST_URI'];
   $page[url_no_number] = cut_string_using_last('/', $page[url], 'left', false); 
}

/*============================*/
/*=== Get Language From Url ==*/
/*============================*/
if($_GET['lng'] && $_GET['lng'] != ''){
    if(strlen(trim($_GET['lng'])) >= 4){
        $siteLanguage = strtoupper(CleanUrlData($_GET['lng']));
    }else{
        $siteLanguage = 'GB';
    }
}

/*=====================================*/
/*=== HTML Space Compression Buffer ===*/
/*=====================================*/
ob_start('replace_whitespace');
/*=====================*/
/*=== Session Start ===*/
/*=====================*/
session_name(md5(WEB_NAME));
session_start();
/*=========================================================*/
/*=== Check If User is LogedIn or Not (Destroy Session) ===*/
/*=========================================================*/
if($_SESSION['id'] && !isset($_COOKIE[session_name()]) && !$_SESSION['rememberMe']){
    $_SESSION = array();
    session_destroy();
    $user[isValid] = FALSE; 
}elseif($_SESSION['id'] && isset($_COOKIE[session_name()]) && $_SESSION['rememberMe']){
    $user[isValid] = 1;  
    $user[username] = $_SESSION['username'];  
}elseif($_SESSION['id'] && isset($_COOKIE[session_name()])){
    $user[isValid] = 1;  
    $user[username] = $_SESSION['username'];  
}elseif(!$_SESSION['id']){
    $user[isValid] = FALSE;  
}

/*=================================================*/
/*== visitor.info.inc.php calculate visitor info: =*/
/*== SiteLanguage, Visitor IP, Orgin Country etc  =*/
/*=================================================*/
// TODO : FIX Calculate Desired LNG ($countryCode $UserCountryCode)
// MAKE LANGUAGE IN DIFFERENT URL like: www.url.com/en/content.com // NO COOKIES OR SESSIONS
// http://googlewebmastercentral.blogspot.com/2010/03/working-with-multilingual-websites.html
include ROOT.'include/visitor.info.inc.php';


/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}
// Add language to Page Array
$page[lng] = $siteLanguage;

/*==================================================*/
/*=========       CSS/JS Minify Include  ===========*/
/*=== min/?f=template/js/jquery.js,scripts/site.js==*/
/*=== min/?f=template/css/LogReg/login.css        ==*/
/*==================================================*/
require ROOT.'/min/utils.php';
$jsMinUri = Minify_getUri(array(
     '//template/js/modernizr.js',
     '//template/js/jquery.simplemodal.1.4.2.min.js',
     '//template/js/hoverIntent.js'
)); // a list of files

if($page[action] != '2'){
    // Does Requested Page Exsist, IF OK do Search, else give homepage!
    if (isset($category[$page[category][1]]) && isset($page[number])) {    
        
        $search = DoSearch("", $page[number], $page[limit], $page[salt], 1, $category[$page[category][1]], 'NONE', 'NONE', 'NONE', 'NONE', 'PHPSearch', 'app_added', 'DESC');    
        // If requested main category like: WEB_URL/LNG/apps/games or /LNG/apps/applications


    }elseif(!isset($category[$page[category][1]]) && isset($category[$page[category][0]])){     
        
        //If Requested page in COMMON SORTED, like "Top New Free"
        if (isset($common_sorted[$category[$page[category][0]]])){
            $search = DoSearch("", $page[number], $page[limit], $page[salt], 1, $category[$page[category][0]], 'NONE', 'NONE', 'NONE', 'NONE', 'PHPSearch', 'app_added', 'DESC');    
        }else{
            // FIX: Get ids of all chiled categories on parent categories (17)
            $search = DoSearch("", $page[number], $page[limit], $page[salt], 1, $category[$page[category][0]], 'NONE', 'NONE', 'NONE', 'NONE', 'PHPSearch', 'app_added', 'DESC');    
        }
    }else{
        echo '<meta http-equiv="refresh" content="0;url='.WEB_URL.'">';
        exit(0);
    }    
    
}else{
    echo '<meta http-equiv="refresh" content="0;url='.WEB_URL.'">';
    exit(0);
}
// if site offline redirect
if($user[isValid] != 1 && SITE_OFFILINE === 1) {
    echo '<meta http-equiv="refresh" content="0;url='.WEB_URL.'site-offline.html">';
    exit(0);
}
?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]--> 
<head>
    <meta charset="utf-8">
    <title><?php echo BROWSE." ".SEO_TAG($page[category][1])." ".ANDROID." ".SEO_TAG($page[category][0])." | ".WEB_NAME; ?></title>
    <base href="<?php echo WEB_URL; ?>" />
    <!--[if ie]>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <![endif]-->
    <meta name="robots" content="index, follow" />   
    <meta name="keywords" content="<?php echo SEO_TAG($page[category][1]).", ".META_KEYWORDS; ?>" />    
    <meta name="author" content="<?php echo WEB_NAME; ?>" />
    <meta name="description" content="<?php echo META_DESC; ?>" />    
    <!-- Mobile viewport optimized: h5bp.com/viewport -->
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <meta name="publisher" content="<?php echo WEB_NAME; ?>"/>
    <meta name="rating" content="general" />
    <meta name="google-site-verification" content="<?php echo GOOGLE_VER; ?>" />
    <link rel="canonical" href="<?php echo $page[url]; ?>">
    <link href="<?php echo WEB_URL; ?>template/img/favicon.ico" rel="shortcut icon" />
    <!--[if lt IE 9]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
    <link rel="stylesheet" href="<?php echo WEB_URL; ?>template/css/main.php" media="all" type="text/css" />
     <link rel="stylesheet" href="<?php echo WEB_URL; ?>template/css/browse.php" media="all" type="text/css" />
    <link rel="stylesheet" href="<?php echo WEB_URL; ?>template/css/slider/slider.css" type="text/css" media="screen" /> 
    <!-- Browser Outdated -->    
    <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/browser-detection.php?lng=<?=$siteLanguage?>"></script>
</head>
<?php
/*==============================================================*/
/*============== Flush the Buffer Early  =======================*/
/*=== http://developer.yahoo.com/performance/rules.html#flush ==*/
/*==============================================================*/
flush(); 
?>
<body>
    <div class="overfill">
        <div id="wrap">
            <div id="header">        
                <div id="logo"></div>
                <form id="searchbox" action="#">
                    <input id="search" type="text" placeholder="<?php echo PAGE_SEARCH_VALUE; ?>">
                    <input id="submit" type="submit" value="<?php echo PAGE_SEARCH_BUTTON; ?>">
                </form>
                <div id="top-lang">

                    <img class ="active" alt="<?php echo WEB_NAME; ?> in <?php echo $siteLanguage;?>" title="<?php echo WEB_NAME; ?> in <?php echo $siteLanguage;?>" src='<?php echo WEB_URL; ?>template/img/flags/<?php echo SEO($siteLanguage); ?>.png' width="17" /> 

                    <?php if($siteLanguage != 'GB') { ?>
                    <img alt="English" src='<?php echo WEB_URL; ?>template/img/flags/gb.png' width="20" />
                    <?php } ?>
                    <?php if($siteLanguage != 'DE') { ?>
                    <img alt="German" src='<?php echo WEB_URL; ?>template/img/flags/de.png' width="20" />
                    <?php } ?>
                    <?php if($siteLanguage != 'IT') { ?>
                    <img alt="Italian" src='<?php echo WEB_URL; ?>template/img/flags/it.png' width="20" />
                    <?php } ?>
                    <?php if($siteLanguage != 'CN') { ?>
                    <img alt="Chiniese" src='<?php echo WEB_URL; ?>template/img/flags/cn.png' width="20" />
                    <?php } ?>
                </div>
                <div id="top-links">
                    <ul class="links">
                        <?php if($user[isValid] === 1) { ?>

                        <li class="menu-1"><a title="<?php echo PAGE_WELLCOME; ?> <?=$user[username]?>" href="#" onClick="return false;"><span><?php echo PAGE_WELLCOME; ?> <?=$user[username]?></span></a></li>
                        <li class="menu-2"><a title="<?php echo PAGE_LOGOFF; ?>" href="<?php echo WEB_URL."checklogindata.html?LogOff=".genRandomString(); ?>"><span><?php echo PAGE_LOGOFF; ?></span></a></li>
                        <?php }else{ ?>
                        <li class="menu-1"><a id="login" title="<?php echo PAGE_LOGIN; ?>" href="#" onClick="return false;"><span><?php echo PAGE_LOGIN; ?></span></a></li>
                        <li class="menu-2"><a id="register" title="<?php echo PAGE_REGISTER; ?>" href="#" onClick="return false;"><span><?php echo PAGE_REGISTER; ?></span></a></li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
            <div id="nav">
                <?php include ROOT.'pages/main/main-menu.php'; ?>
            </div>        
            <div class="clear"></div> 

            <div id="main"> 
                <?php        
                    $browsehtml = SearchDetails($search);
                    if($browsehtml != false){
                        print_r($browsehtml);
                    }else{
                        include ROOT.'pages/main/not_found.php';
                    }
                ?>

            </div>
            <div id="sidebar">
                SIDEBAR
            </div>        
                <?php
                    if($browsehtml != false){
                        $pagination = pagination($page, $user, $search);
                        print_r($pagination);
                    }
                    include ROOT.'pages/main/footer.php'; 
                ?>
        </div>
    </div>
        <div id="beta">
            <img style="border: 0;" alt="<?php echo PAGE_BETA; ?>" src="<?php echo WEB_URL; ?>template/img/beta.png">
        </div>
        <?php if($user[userCheck] == FALSE){ ?>
        <div id="login_form" style='display:none'></div>
        <div id="register_form" style='display:none'></div>
        <?php } ?>
        
    <!-- JavaScript at the bottom for fast page loading -->
    <!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <script>window.jQuery || document.write(' <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/jquery.min.js"><\/script>')</script>
    <script type="text/javascript" src="<?php echo $jsMinUri; ?>"></script>  
    <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/slider.min.php?lng=<?=$siteLanguage?>"></script>
    <script type="text/javascript" src="<?php echo WEB_URL; ?>template/js/main.php?lng=<?=$siteLanguage?>&amp;c=<?=urlencode($user[country])?>"></script>
    <?php echo GOOGLE_ANALYTICS; ?>
    <!-- end scripts -->
</body>
</html>
<?php
/*==============================*/
/*=== Unset Arrays Variables ===*/
/*==============================*/
unset($user);
unset($page);   
/*=============================*/
/*=== Print Minified Buffer ===*/
/*=============================*/
ob_end_flush();
?>