<?php 
header("Content-type: text/css"); 
?>
/*====================*/
/*======= Main =======*/
/*====================*/
html, body {
    min-height: 100%;
    height: auto;
}
* {
    margin: 0;
    padding: 0;
}
img {border: 0;}

body {
    background: url("../img/test-04.png") no-repeat scroll center top #013002;
    background-repeat:repeat-x;
    background-attachment: fixed;
    
    background-color:#f2f3f4;
    color: #222222;    
    font-family: "Helvetica Neue",Arial,sans-serif;
    font-size: 13px;
    position: relative;
}

#wrap {
    width:1000px;
    margin:0 auto;
}
.overfill {
    background: url("http://terranet.md/img/shade.png") repeat-x scroll 0 0 transparent;
}
#header {
    height: 150px;
    margin: 0 auto;
    padding-top: 15px;
    text-align: center;
    position: relative;
    width: 100%;
    position: relative
}
#logo {
    background: url("../img/sooner.png") no-repeat scroll center center transparent;
    display: block;
    float: left;    
    height: 78px;
    margin: 0 auto;
    padding-top: 5px;
    text-indent: -9999px;
    width: 350px;
}
/*====================*/
/*====== Search ======*/
/*====================*/
#searchbox
{
    background: #eaf8fc;
    background-image: -moz-linear-gradient(#fff, #d4e8ec);
    background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0, #d4e8ec),color-stop(1, #fff));
    -moz-border-radius: 35px;
    border-radius: 35px;
    border-width: 1px;
    border-style: solid;
    border-color: #c4d9df #a4c3ca #83afb7;            
    width: 500px;
    height: 35px;
    padding: 10px;
    overflow: hidden;
    float: right;
    margin-top: 15px;
}

#search, #submit
{
    float: left;
}

#search
{
    padding: 5px 9px;
    height: 23px;
    width: 380px;
    border: 1px solid #a4c3ca;
    font: normal 13px 'trebuchet MS', arial, helvetica;
    background: #f1f1f1;
    -moz-border-radius: 50px 3px 3px 50px;
    border-radius: 50px 3px 3px 50px;
    -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25) inset, 0 1px 0 rgba(255, 255, 255, 1);
    -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25) inset, 0 1px 0 rgba(255, 255, 255, 1);
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25) inset, 0 1px 0 rgba(255, 255, 255, 1);            
}
#submit{		
    background: #6cbb6b;
    background-image: -moz-linear-gradient(#95d788, #6cbb6b);
    background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0, #6cbb6b),color-stop(1, #95d788));
    -moz-border-radius: 3px 50px 50px 3px;
    border-radius: 3px 50px 50px 3px;
    border-width: 1px;
    border-style: solid;
    border-color: #7eba7c #578e57 #447d43;
    -moz-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
    -webkit-box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;
    box-shadow: 0 0 1px rgba(0, 0, 0, 0.3), 0 1px 0 rgba(255, 255, 255, 0.3) inset;   		
    height: 35px;
    margin: 0 0 0 10px;
    padding: 0;
    width: 90px;
    cursor: pointer;
    font: bold 14px Arial, Helvetica;
    color: #23441e;
    text-shadow: 0 1px 0 rgba(255,255,255,0.5);
}

#submit:hover{		
    background: #95d788;
    background-image: -moz-linear-gradient(#6cbb6b, #95d788);
    background-image: -webkit-gradient(linear,left bottom,left top,color-stop(0, #95d788),color-stop(1, #6cbb6b));
}	

#submit:active{		
    background: #95d788;
    outline: none;
    -moz-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
    -webkit-box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;
    box-shadow: 0 1px 4px rgba(0, 0, 0, 0.5) inset;		
}

#submit::-moz-focus-inner{
    border: none;
}		

#search::-webkit-input-placeholder {
    color: #9c9c9c;
    font-style: italic;
}

#search:-moz-placeholder {
    color: #9c9c9c;
    font-style: italic;
}    

#search.placeholder {
    color: #9c9c9c !important;
    font-style: italic;
}  

#search:focus{
    border-color: #8badb4;
    background: #fff;
    outline: none;
}
/*====================*/
/*======= Login ======*/
/*====================*/
#top-links {
    right: 0;
    float: right;    
    position: absolute; 
    bottom: 25px;
}
ul.links {
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}
#top-links ul.links li {
    padding-bottom: 0;
    padding-left: 0;
    padding-right: 0;
    padding-top: 0;
}
ul.links li {
    margin-bottom: 0;
    margin-left: 0;
    margin-right: 0;
    margin-top: 0;
    display: inline;
    list-style-type: none;
}
ul.links li a:link {
    text-decoration: none;
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    color: #0088AA;
    font-weight: bold;
}
ul.links li a:visited {
    text-decoration: none;
    -moz-text-blink: none;
    -moz-text-decoration-color: -moz-use-text-color;
    -moz-text-decoration-line: none;
    -moz-text-decoration-style: solid;
    color: #0088AA;
    font-weight: bold;
}
#top-links ul li a:hover  {
    color: #000;
    font-weight: bold;
}
#top-links ul li a {
    text-decoration: none;
    background-attachment: scroll;
    background-clip: border-box;
    background-color: #403E3D;
    background-image: none;
    background-origin: padding-box;
    background-position: 0 0;
    background-repeat: repeat;
    background-size: auto auto;
    border-bottom-left-radius: 4px;
    border-bottom-right-radius: 4px;
    border-top-left-radius: 4px;
    border-top-right-radius: 4px;
    color: #FFFFFF;
    padding-bottom: 0.3em;
    padding-left: 0.5em;
    padding-right: 0.5em;
    padding-top: 0.3em;
    font-weight: bold;
    font-family: "Lucida Grande",Arial,Verdana,sans-serif;
    font-size: 14px;
}
/*====================*/
/*= Country CodeFlag =*/
/*====================*/
#top-lang {
    left: 0;
    float: left;    
    position: absolute; 
    bottom: 25px;
}
.active {
   padding:1px;
   border:1px solid #021a40;
   background-color:#ff0;
}
/*====================*/
/*= Navi & Top Menu ==*/
/*====================*/
#nav {
    width: 100%;
    padding-top: 5px;
    position: relative;
    margin: 0 auto;
    text-align: center;
}
#nav_wrapper {
    padding-bottom: 5px;
    width: 1000px;
    height: 50px;
    text-align: center;
}
span#menu_start {
    display:block;
    float:left;
    height:46px;
    width:52px;
}
ul#topnav {
    margin: 0; padding: 0;
    float:left;
    list-style: none;
    font-size: 1em;
    background: url("../img/topmenu/back_nav.png") repeat-x;
    width: 943px;
}
ul#topnav li {
    float: left;
    margin: 0; padding: 0;
    position: relative;
    background: url("../img/topmenu/li_border.png") no-repeat right top;	
}
ul#topnav li#last-li {
    background: none;
}
ul#topnav li#last-li a span.single_wrapper {
    float:left;
    margin:6px 6px 6px 10px;
    padding:0 10px;
}

ul#topnav li.liSub a, ul#topnav li a {
    color: white;
    float: left;
    height: 46px;
    line-height: 34px;
    padding: 0;
    text-decoration: none;
    text-shadow: 0 0 2px black;
    text-transform: uppercase;
    z-index: 200;
}
span#menu_start a {
    background: url("../img/topmenu/home_nav.png") no-repeat left top;
    width: 52px; height: 46px;
    text-indent: -9999px;
    padding:0; margin:0;
}
span#menu_start a:hover  { background-position: left -46px; }
span#menu_start a:active { background-position: left -92px; }
span#menu_start a {	display:block; line-height: 46px; }
ul#topnav li .sub {
    position: absolute;	
    top: 46px; left: 10px;
    background: #FFFFFF;
    background-color: #f8f8f8\9; /* le  \9  est un hack pour cibler IE8, IE7 et IE6 */ 
    border-bottom: 1px solid #cbcbcb\9;
    border-left: 1px solid #cbcbcb\9;
    border-right: 1px solid #cbcbcb\9;
    padding: 10px 10px 10px;
    float: left;
    border-bottom-right-radius:		    10px;
    -moz-border-radius-bottomright:     10px;
    -khtml-border-radius-bottomright:   10px;
    -webkit-border-bottom-right-radius: 10px;
    border-bottom-left-radius:		    10px;
    -moz-border-radius-bottomleft:      10px;
    -khtml-border-radius-bottomleft: 	10px;
    -webkit-border-bottom-left-radius:  10px;
    box-shadow:0px 0px 5px #5a5a5a;
    -moz-box-shadow:0px 0px 5px #5a5a5a;
    -webkit-box-shadow:0px 0px 5px #5a5a5a;
    display: none;
    z-index: 1000;
}
ul#topnav li .row {clear: both; float: left; width: 100%; margin-bottom: 10px;}
ul#topnav li .sub ul{
    list-style: none;
    margin: 0; padding: 0;
    width: 170px;
    float: left;
    text-align: left;
}
ul#topnav .sub ul li {
    width: 100%;
    color: #fff;
    background: none;
}
ul#topnav .sub ul li h2 {
    padding: 0;  margin: 0;
    font-size: 1.3em;
    font-weight: normal;
}
ul#topnav .sub ul li h2 a, ul#topnav .sub ul li h2 a:hover {
    padding: 5px 0;
    background-image: none;
    color: #c10707;
}
ul#topnav .sub ul li h2 a:hover {
    background-image: none;
    text-decoration: underline;
}
ul#topnav .sub ul li a {
    float: none; 
    text-indent: 0;
    height: auto;
    background: url("../img/topmenu/navlist_arrow.png") no-repeat 0px 10px;
    padding: 0 0 0 18px;
    margin: 0 10px;
    display: block;
    text-decoration: none;
    color: #757575;
    font-size:1.1em;
    text-shadow: none;
    border-bottom: 1px solid #e6e6d8;
    text-transform: none;
    line-height:27px;
}
ul#topnav .sub ul li a:hover {
    color: #c50a0a;
    background: url("../img/topmenu/navlist_arrow_hover.png") no-repeat 0px 10px;
    text-decoration: underline;
        
}
span#menu_end {
    display:block;
    float:left;
    height:46px;
    width:5px;
}
span.dd_action {
    height:6px;
    padding:0 0 0 10px;
    width:9px;
}
span.single_wrapper {
    float:left;
    margin:6px 10px;
    padding:0 15px;
}
span.single_wrapper:hover, li.current a span.single_wrapper {
    background: white;
    color: black;
    text-shadow: none;	
    filter: progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr=#FFFFFFFF, endColorstr=#FFe5e5e5);
    background-image: -moz-linear-gradient(	top, #FFFFFF, #e5e5e5);
    background-image: -webkit-gradient( linear,	left top, left bottom, from(#FFFFFF), to(#e5e5e5));
    border-radius: 	   7px; 
    -moz-border-radius:    7px; 
    -khtml-border-radius:  7px;
    -webkit-border-radius: 7px;
    box-shadow:         0px 0px 2px #343333; 
    -webkit-box-shadow: 0px 0px 2px #343333; 
    -moz-box-shadow:    0px 0px 2px #343333; 
}
span.single_wrapper:active {
    background: white;	
    color: black;
    text-shadow: none;	
    filter: progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr=#FFe5e5e5, endColorstr=#FFFFFFFF);
    background-image: -moz-linear-gradient(	top, #e5e5e5, #FFFFFF);
    background-image: -webkit-gradient( linear, left top, left bottom, from(#e5e5e5), to(#FFFFFF));
    border-radius:         7px;
    -moz-border-radius:    7px;
    -khtml-border-radius:  7px;
    -webkit-border-radius: 7px;	
    box-shadow:         0px 0px 2px #343333;
    -webkit-box-shadow: 0px 0px 2px #343333;
    -moz-box-shadow:    0px 0px 2px #343333;
}
span.dd_wrapper {
    float:left;
    margin:6px 10px 0;
    padding:0 11px 6px 18px;
}
li.liSub a:hover span.dd_wrapper, li.liSub:hover a span.dd_wrapper {
    z-index:      100;
    background:   white;
    color: 		  black;
    border-top:   1px solid #e5e5e5;	
    border-right: 1px solid #989898;
    padding:	  0 10px 6px 18px;	
    text-shadow: none;	
    filter: progid:DXImageTransform.Microsoft.gradient(gradientType=0, startColorstr=#dfdfdf, endColorstr=#f8f8f8); /*  IE */
    background-image: -moz-linear-gradient(	top, #e5e5e5, #FFFFFF); /* Firefox */
    background-image: -webkit-gradient( linear,	left top, left bottom, from(#e5e5e5), to(#FFFFFF)); /* Webkit */
    border-top-right-radius:		 7px;
    -moz-border-radius-topleft: 	 7px;
    -webkit-border-top-left-radius:  7px;
    -khtml-border-radius-topleft: 	 7px;
    border-top-left-radius:		     7px;
    -moz-border-radius-topright: 	 7px;
    -webkit-border-top-right-radius: 7px;
    -khtml-border-radius-topright: 	 7px;
}
/*====================*/
/*======= Main =======*/
/*====================*/
#main {
    height: 100% !important; 
    float: left;
    min-height: 550px;
    position: relative; 
    width: 750px;
    background: #FFF;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -khtml-border-radius: 5px;
    border-radius: 5px;
    border: 1px solid #DCDCDC !important;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.3);
}
#sidebar {
    min-height: 100%;
    height: 100% !important; 
    float:right;    
    position: relative;
    width:245px;
    background: #FFF;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -khtml-border-radius: 5px;
    border-radius: 5px;
    border: 1px solid #DCDCDC !important;
    box-shadow: 0 1px 2px rgba(0, 0, 0, 0.3);
}
/*====================*/
/*====== Footer ======*/
/*====================*/
#footer {
    clear:both;
    border: none;
    height: 40px;
    margin: auto;
    margin-bottom: 10px;
    margin-top: 10px;
    position:relative; 
    width: 990px;
    z-index: 75;
    float: left;
}
#footer-nav {
    position: relative;
    background-color: #f5f5f5;
    color: #222222;
    font-family: Arial;
    font-size: 14px;
    line-height: 14px;
    text-align: center;
    height: 30px;
    margin: auto;
    padding: 13px 0 0 10px;
    width: 990px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    -khtml-border-radius: 5px;
    border-radius: 5px;
    border: 1px solid #DCDCDC !important;
}

#footer-nav a:link {color:#222222; outline: 0 none; text-decoration: none;}      /* unvisited link */
#footer-nav a:visited {color:#222222; outline: 0 none; text-decoration: none;}  /* visited link */
#footer-nav a:hover {color:#222222; outline: 0 none; text-decoration: underline;}  /* mouse over link */
#footer-nav a:active {color:#222222; outline: 0 none; text-decoration: none;}  /* selected link */ 
/*====================*/
/*====== Other =======*/
/*====================*/
#beta {
    clear: both;
    height: 68px;
    left: 100%;
    margin-left: -65px;
    overflow-x: hidden;
    overflow-y: hidden;
    position: absolute;
    top: 0;
    width: 65px;
    z-index: 1000;
}
.clear {
    clear: both;
}

/*====================*/
/*====== Modal =======*/
/*====================*/
#simplemodal-overlay {background-color:#f3f3f3; cursor:wait;}
/* Container */
#simplemodal-container {background-color:#fff; border:0px; -moz-border-radius: 5px; }
#simplemodal-container a.modalCloseImg {background:url('../img/LogReg/x.png') no-repeat; width:25px; height:29px; display:inline; z-index:3200; position:absolute; top:-15px; left:-18px; cursor:pointer;}
#simplemodal-container #basicModalContent {padding:8px;}

/*====================*/
/*= Browser Warning ==*/
/*====================*/

#browser-detection {
	background: #FFFFE5;
	color: #333333;
	position: fixed;
	_position: absolute;
	padding: 10px 15px;
	font-size: 13px;
	font-family: "Trebuchet MS", "Segoe UI", Arial, Tahoma, sans-serif;
	border-radius: 5px;
	border: 1px solid #D6D6C1;
	-moz-border-radius: 5px;
	width: 700px;
	z-index:1001;
}
#browser-detection P {
	margin: 0;
	padding: 0;
	background: transparent;
	line-height: 135%;
	width: auto;
	float: none;
	border: none;
	text-align: left;
}
#browser-detection P.bd-title {
    padding-top: 0px;
    font-size: 25px;
    line-height: 100%;
}
#browser-detection P.bd-notice {
    padding-bottom: 5px;
    padding-top: 5px;
}
#browser-detection SPAN.bd-highlight { color: #B50E0E; }
#browser-detection A#browser-detection-close {
	width: 15px;
	height: 15px;
	outline: none;
	position: absolute;
	right: 10px;
	top: 10px;
	text-indent: -500em;
	line-height: 100%;
	background: url(../img/browser/close.gif) no-repeat center center;
}
#browser-detection A#browser-detection-close:HOVER { background-color: #F5F5DC; }
#browser-detection UL.bd-browsers-list, #browser-detection UL.bd-browsers-list LI,
#browser-detection UL.bd-skip-buttons, #browser-detection UL.bd-skip-buttons LI {
	padding: 0;
	margin: 0;
	float: left;
	list-style: none;
}
#browser-detection UL.bd-browsers-list { 
	clear: both;
	margin-top: 3px;
	padding: 7px 0;
	border-top: 1px solid #F5F5DC;
	border-bottom: 1px solid #F5F5DC;
	width: 100%;
}
#browser-detection UL.bd-browsers-list LI { text-align: left; }
#browser-detection UL.bd-browsers-list LI A {
	width: 60px;
	height: 55px;
	display: block;
	color: #666666;
	padding: 10px 10px 0 65px;
	text-decoration: none;
}
#browser-detection UL.bd-browsers-list LI A:HOVER {	text-decoration: underline; }
#browser-detection UL.bd-browsers-list LI.firefox A { background: url(../img/browser/firefox.gif) no-repeat left top; }
#browser-detection UL.bd-browsers-list LI.chrome A { background: url(../img/browser/chrome.gif) no-repeat left top; }
#browser-detection UL.bd-browsers-list LI.safari A { background: url(../img/browser/safari.gif) no-repeat left top; }
#browser-detection UL.bd-browsers-list LI.opera A { background: url(../img/browser/opera.gif) no-repeat left top; }
#browser-detection UL.bd-browsers-list LI.msie A { background: url(../img/browser/msie.gif) no-repeat left top; }
#browser-detection UL.bd-skip-buttons {	margin-top: 10px; }
#browser-detection UL.bd-skip-buttons LI {
	display: inline;
	margin-right: 10px;	
}
#browser-detection UL.bd-skip-buttons LI BUTTON { font-size: 13px; }
#browser-detection DIV.bd-poweredby {
	font-size: 9px;
	position: absolute;
	bottom: 10px;
	right: 10px;
	font-style: italic;
}
#browser-detection DIV.bd-poweredby, #browser-detection DIV.bd-poweredby A { color: #AAAAAA; }
#browser-detection DIV.bd-poweredby A { text-decoration: underline; }
#browser-detection DIV.bd-poweredby A:HOVER { text-decoration: none; }
#black_overlay {
        background-color: #F3F3F3;
        cursor: wait;        
        opacity: 0.5; 
        width: 100%;
	height: 100%;
        position: fixed; 
        left: 0px; 
        top: 0px; 
        z-index: 1001;       
        
} 