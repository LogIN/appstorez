<?php 
header("Content-type: text/css"); 
?>
#main {    
    text-align: center;
    background: url("http://terranet.md/img/wide-bg.gif") repeat-y scroll center 0 #FFFFFF;
    color: #555555;
    font: 100% Arial,Helvetica,sans-serif;
    text-align: left;
}
#wrap {
    min-height: 100%;
    height: auto;
}
#no-results-section{
    width: 100%;
    margin: auto;
    margin-top: 25px;
    margin-left: 15px;
    padding: auto;
    text-align:left;


}

.app {
    width: 355px;
    height: 125px;
    float: left;
    position: relative;
    border: 1px solid #FFFFFF;
    border-radius: 4px 4px 4px 4px;
    cursor: pointer;
    display: block;
    margin: 4px;
    padding: 4px;
}
.app:hover{
    width: 355px;
    height: 125px;
    float: left;
    position: relative;
    border-radius: 4px 4px 4px 4px;
    cursor: pointer;
    display: block;
    margin: 4px;
    padding: 4px;
    background-color: #D0E7A1;
    border: 1px solid #83AF2C;
    box-shadow: 0 0 3px #999999;
}

.app h1{
    margin-top: 0px;
    padding-left: 5px;
    width: 260px;
    height: 20px;
    
    text-overflow: clip ellipsis;
    overflow: hidden;
    white-space: nowrap;
    font-weight:normal;
    
    color: #2276EE;
    font-size: 13pt;
    text-align:left;
    font-family: Tahoma,Helvetica,Arial,sans-serif;
}
.app h2{
    margin-top: 0px;
    padding-left: 5px;
    position:relative;
    top: 0px;
    width: 260px;
    height: 15px;
    text-overflow: clip ellipsis;
    overflow: hidden;
    white-space: nowrap;
    font-weight:normal;
    color:#CCC;
    font-size:13px;
    text-align:left;
    font-family:arial, helvetica, sans-serif;
}
.app_icon{
    width: 90px;
    height: 90px;
    float: left;
}
.app .badges{
    position: absolute;
    top: 60px;
    left: 90px;
    float: right;
    width: 265px;
    height: 30px;
    font-weight:normal;
    font-size:12px;
    font-family:arial, helvetica, sans-serif;
}
.app .badges img{
    padding-left: 3px;
}
.app .rating{
    position: absolute;
    float: left;
    width: 90px;
    height: 20px;
    bottom: 7px;
    left: 0px;
}
.app .price_paid{
    position: absolute;
    float: right;
    height: 20px;
    bottom: 7px;
    right: 5px;
    font-size: 14pt;
    font-weight: bold;
    color: #333333;
    text-decoration: none;
}
.app .price_free{
    position: absolute;     
    float: right;
    height: 20px;
    bottom: 7px;
    right: 5px;
    font-size: 14pt;
    font-weight: bold;
    color: #A4C639;
    text-decoration: none;
}
.app .smallinfo{
    position: absolute; 
    height: 20px;
    bottom: 3px;
    left: 95px;
    color: #555555;
    display: block;
    font-size: 9pt;
}
.app .desc{
    margin-top: 0px;
    padding-left: 5px;
    position:relative;
    top: 0px;
    width: 260px;
    height: 15px;
    float: left;
    font-weight:normal;
    color:#000;
    font-size:9pt;
    text-align:left;
    font-family:arial, helvetica, sans-serif; 
}
.classification {
    position: relative;
    width: 80px;
    height: 15px;
}
.classification .cover {
    position: absolute;
    background: transparent url("../img/misc/stars_empty.png") top left no-repeat;
    top: 0px;
    left: 0px;
    width: 80px;
    height: 15px;
    z-index: 101;
}
.classification .progress {
    position: absolute;
    background: transparent url("../img/misc/stars_full.png") top left no-repeat;
    top: 0px;
    left: 0px;
    height: 15px;
    z-index: 102;
}

#paginate {
    clear: both;
    width: 750px;
    margin-top: 10px;
    float: left;
    text-align: center;
}
ul.pagination{
    margin:auto;
    padding:auto;       
    height:100%;
    width: 570px;
    overflow:hidden;
    font:12px 'Tahoma';
    list-style-type:none;	
}

ul.pagination li.details{
    padding:7px 10px 7px 10px;
    font-size:14px;
}

ul.pagination li.dot{padding: 3px 0;}

ul.pagination li{
	float:left;
	margin:0px;
	padding:0px;
	margin-left:5px;
}

ul.pagination li:first-child{
	margin-left:0px;
}

ul.pagination li a{
	color:black;
	display:block;
	text-decoration:none;
	padding:7px 10px 7px 10px;
}

ul.pagination li a img{
	border:none;
}
ul.pagination li.details{
    color:#3390CA;
}
ul.pagination li a
{
        border:solid 1px;
        border-radius:3px;	
        -moz-border-radius:3px;
        -webkit-border-radius:3px;
        padding:6px 9px 6px 9px;
}

ul.pagination li
{
        padding-bottom:1px;
}

ul.pagination li a:hover,
ul.pagination li a.current
{	
        color:#FFFFFF;
        box-shadow:0px 1px #EDEDED;
        -moz-box-shadow:0px 1px #EDEDED;
        -webkit-box-shadow:0px 1px #EDEDED;
        text-shadow:0px 1px #388DBE;
        border-color:#3390CA;
        background:#58B0E7;
        background:-moz-linear-gradient(top,#B4F6FF 1px,#63D0FE 1px,#58B0E7);
        background:-webkit-gradient(linear,0 0,0 100%,color-stop(0.02,#B4F6FF),color-stop(0.02,#63D0FE),color-stop(1,#58B0E7));        
}
ul.pagination li a
{
        color:#0A7EC5;
        border-color:#8DC5E6;
        background:#F8FCFF;
}	