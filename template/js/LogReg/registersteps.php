<?php 
header("Content-type: text/javascript");
/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
require ('../../../include/config.inc.php');
include ROOT.'include/functions.inc.php';

/*============================*/
/*=== Get Language From Url ==*/
/*============================*/
if($_GET['lng'] && $_GET['lng'] != ''){
    if(strlen(trim($_GET['lng'])) >= 4){
        $siteLanguage = CleanUrlData($_GET['lng']);
    }else{
        $siteLanguage = 'GB';
    }
}
/*============================*/
/*=== Get Country  From Url ==*/
/*============================*/
if($_GET['c'] && $_GET['c'] != ''){
    if(strlen(trim($_GET['c'])) >= 4){
        $country = CleanUrlData($_GET['c']);        
    }else{
        $country = 'United Kingdom';
    }        
}
/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}
?>
$(function(){   

    /*=============================*/
    /*= Reset Progress Bar OnLoad==*/
    /*=============================*/
    $('#progress').css('width','0');
    $('#progress_text').html('0% Complete');

    /*=============================*/
    /*=== First Submit Actions ====*/
    /*=============================*/
    $('form').submit(function(){ return false; });
    
     $('input[type=image]').click(function(){
    
        //remove classes
        $('#first_step input').removeClass('error').removeClass('valid');

        var image = $(this);
        
        /*====================*/
        /*= Define UserType ==*/
        /*====================*/
        usertype = $(this).val();
        
        /*=============================*/
        /*==== Update Progress Bar ====*/
        /*=============================*/
        $('#progress_text').html('25% Complete');
        $('#progress').css('width','85px');

        /*====================*/
        /*= Slide to Second ==*/
        /*====================*/
        $('#first_step').slideUp();
        $('#second_step').slideDown();                            

    });
    
    /*=============================*/
    /*=== Second Submit Actions ===*/
    /*=============================*/
    $('#submit_second').click(function(){
        /*======================*/
        /*= Main Field Values ==*/
        /*======================*/
        var field_values_1 =  []
        field_values_1 = {
                //id        :  value
                'username'  : 'username',
                'password'  : 'password',
                'cpassword' : 'password',
                'email'  : 'email address'
        };
        /*===============*/
        /*= InputFocus ==*/
        /*===============*/
        $('input#username').inputfocus({ value: field_values_1['username'] });
        $('input#password').inputfocus({ value: field_values_1['password'] });
        $('input#cpassword').inputfocus({ value: field_values_1['cpassword'] }); 
        $('input#email').inputfocus({ value: field_values_1['email'] }); 
        
        var value = '';
        var fields = '';
        var error = 0;
        
        /*======================*/
        /*=== Remove Classes ===*/
        /*======================*/
        $('#second_step input').removeClass('error').removeClass('valid');
        
        /*===================*/
        /*=== EMail REGEX ===*/
        /*===================*/
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;  
        
        /*==========================================*/
        /*=== Check field Values are not Default ===*/
        /*==========================================*/
        fields = $('#second_step :input');
        fields.each(function(){
            value = $(this).val();
            if (this.type != "submit"){
                if( value.length < 5 || value == field_values_1[$(this).attr('id')] || ( $(this).attr('id')=='email' && !emailPattern.test(value) ) ) {
                    $(this).addClass('error');
                    $(this).effect("shake", { times:3 }, 50);                
                    error++;

                } else {
                    $(this).addClass('valid');
                }
            }
        });
        
        /*======================*/
        /*===   If No Error  ===*/
        /*======================*/
        if(!error) {
                /*==========================*/
                /*== Update Progress Bar ===*/
                /*==========================*/
                $('#progress_text').html('50% Complete');
                $('#progress').css('width','159px');
                
                /*====================*/
                /*= Slide to Third ===*/
                /*====================*/
                $('#second_step').slideUp();
                $('#third_step').slideDown();     
        } else return false;

    });

    /*=============================*/
    /*===  Third Submit Actions ===*/
    /*=============================*/
    $('#submit_third').click(function(){
    
        /*======================*/
        /*= Main Field Values ==*/
        /*======================*/
        var field_values_2 =  []
        field_values_2 = {
            //id        :  value
            'name'  : 'first name',
            'surname'  : 'last name',
            'age' : ''
        };
        
        /*===============*/
        /*= InputFocus ==*/
        /*===============*/
        $('input#name').inputfocus({ value: field_values_2['name'] });
        $('input#surname').inputfocus({ value: field_values_2['surname'] });
        $('input#age').inputfocus({ value: field_values_2['age'] });     

        var value = '';
        var fields = '';
        var error = 0;
        
        /*======================*/
        /*=== Remove Classes ===*/
        /*======================*/
        $('#third_step input').removeClass('error').removeClass('valid');
        
        /*===================*/
        /*=== Age REGEX ===*/
        /*===================*/
        var agePattern = /([1-9][0-9])/;  
        
        /*==========================================*/
        /*=== Check field Values are not Default ===*/
        /*==========================================*/
        fields = $('#third_step :input');        
        fields.each(function(){
            value = $(this).val();
            if (this.type != "submit"){
                if( value.length < 2 || value == field_values_2[$(this).attr('id')] || ( $(this).attr('id')=='age' && !agePattern.test(value) ) ) {
                    $(this).addClass('error');
                    $(this).effect("shake", { times:3 }, 50);

                    error++;
                } else {
                    $(this).addClass('valid');
                }
             }
        });
        
        /*======================*/
        /*===   If No Error  ===*/
        /*======================*/
        if(!error) {
        
                /*==========================*/
                /*== Update Progress Bar ===*/
                /*==========================*/
                $('#progress_text').html('75% Complete');
                $('#progress').css('width','238px');
                
                /*==============================*/
                /*== If Developer Ask Adress ===*/
                /*==============================*/
                if(usertype == 'developer'){                
                    $('#adress_placeholder').html('<input type="adress" name="adress" id="adress" value="adress" /><label for="adress">Your Adress<\/label>');                
                }
                
                
                /*====================*/
                /*= Slide to Fourth ==*/
                /*====================*/
                $('#third_step').slideUp();
                $('#fourth_step').slideDown();     
        } else return false;

    });
    
    /*==============================*/
    /*===  Fourth Submit Actions ===*/
    /*==============================*/
    $('#submit_fourth').click(function(){
    
        /*======================*/
        /*= Main Field Values ==*/
        /*======================*/
        var field_values_3 =  []
        field_values_3 = {
            //id        :  value
            'city'  : 'city',
            'postal'  : 'postal'
        };
        
        /*===============*/
        /*= InputFocus ==*/
        /*===============*/
        $('input#city').inputfocus({ value: field_values_3['city'] });
        $('input#postal').inputfocus({ value: field_values_3['postal'] });
        
        var value = '';
        var fields = '';
        var error = 0;
        
        /*======================*/
        /*=== Remove Classes ===*/
        /*======================*/
        $('#fourth_step input').removeClass('error').removeClass('valid');
        
        /*===================*/
        /*=== Postal REGEX ===*/
        /*===================*/
        var postalPattern = /[0-9]+/;  
        
        
        /*==========================================*/
        /*=== Check field Values are not Default ===*/
        /*==========================================*/
        fields = $('#fourth_step :input');
        fields.each(function(){
            value = $(this).val();
            if (this.type != "submit"){
                if( value.length < 3 || value == field_values_3[$(this).attr('id')] || ( $(this).attr('id')=='postal' && !postalPattern.test(value) )) {
                    $(this).addClass('error');
                    $(this).effect("shake", { times:3 }, 50);

                    error++;
                } else {
                    $(this).addClass('valid');
                }
             }
        });
        /*======================*/
        /*===   If No Error  ===*/
        /*======================*/
        if(!error) {
        
                /*======================================*/
                /*===   If UserType User: do Submit  ===*/
                /*======================================*/
                if(usertype == 'user'){
                
                    /*=========================*/
                    /*== Update Progress Bar ==*/
                    /*=========================*/
                    $('#progress_text').html('100% Complete');
                    $('#progress').css('width','339px');
                    
                    /*=========================*/
                    /*== Get Entered Values  ==*/
                    /*=========================*/
                    var username = $('#username').val();
                    var password = $('#password').val();
                    var email = $('#email').val();
                    var name = $('#name').val();
                    var surname = $('#surname').val();
                    var age = $('#age').val();
                    var gender = $('#gender').val();
                    var country = $('#country').val();
                    var city = $('#city').val();
                    var postal = $('#postal').val();
                    
                    /*===============================*/
                    /*== Post Values to 'Register' ==*/
                    /*===============================*/
                    $.post( "<?php echo WEB_URL; ?>checklogindata.html", { 
                                                                            submit: "Register",
                                                                            type: "5",
                                                                            username: username, 
                                                                            password: password, 
                                                                            email: email,
                                                                            name: name,
                                                                            surname: surname,
                                                                            age: age,
                                                                            gender: gender,
                                                                            country: country,
                                                                            city: city,
                                                                            postal: postal},
                    
                        /*====================================*/
                        /*== Check 'Register' Response Data ==*/
                        /*====================================*/                                       
                        function( data ) {
                            if(data != 'OK' ){
                            
                                alert(data);
                                
                                $('#progress_text').html('0% Complete');
                                $('#progress').css('width','0px');
                            }else{
                            
                                $('#Username').html(username);
                                $('#Password').html('******');
                                $('#Email').html(email);
                                $('#Name').html(name);
                                $('#Surname').html(surname);
                                $('#Age').html(age);
                                $('#Gender').html(gender);
                                $('#Country').html(country);
                                $('#City').html(city);
                                $('#Postal').html(postal);
                                
                                /*=========================*/
                                /*== Slide to UserDone!  ==*/
                                /*=========================*/
                                $('#fourth_step').slideUp();
                                $('#user_done').slideDown();                            

                                /*
                                alert("Registration OK! Check Email!");
                                window.top.location.href = '<?php echo WEB_URL; ?>';
                                */
                            }
                        }
                    );               
                    
                /*====================================================*/
                /*== If UserType != 'User' (Developer) => Countinue ==*/
                /*====================================================*/
                }else{
                    //update progress bar
                    $('#progress_text').html('90% Complete');
                    $('#progress').css('width','305px');

                    //slide steps
                    $('#fourth_step').slideUp();
                    $('#fifth_step').slideDown();   
                }
  
        } else return false;

    });
    
    $('#submit_fifth').click(function(){  
    
        /*======================*/
        /*= Main Field Values ==*/
        /*======================*/
        var field_values_4 =  []
        field_values_4 = {
            //id        :  value
            'phone'  : 'phone',
            'mobile'  : 'mobile',
            'web'  : 'web'
        };
        
        /*===============*/
        /*= InputFocus ==*/
        /*===============*/
        $('input#phone').inputfocus({ value: field_values_4['phone'] });
        $('input#mobile').inputfocus({ value: field_values_4['mobile'] });
        
        var value = '';
        var fields = '';
        var error = 0;
        
        /*======================*/
        /*=== Remove Classes ===*/
        /*======================*/
        $('#fifth_step input').removeClass('error').removeClass('valid');
        
       
        /*==========================================*/
        /*=== Check field Values are not Default ===*/
        /*==========================================*/
        fields = $('#fourth_step :input');
        fields.each(function(){
            value = $(this).val();
            if (this.type != "submit"){
                if( value.length < 3 || value == field_values_4[$(this).attr('id')] ) {
                    $(this).addClass('error');
                    $(this).effect("shake", { times:3 }, 50);

                    error++;
                } else {
                    $(this).addClass('valid');
                }
             }
        });
        /*======================*/
        /*===   If No Error  ===*/
        /*======================*/
        if(!error) {
        
                /*===============================*/
                /*===   If UserType Developer ===*/
                /*===============================*/
                if(usertype == 'developer'){
                
                    /*=========================*/
                    /*== Update Progress Bar ==*/
                    /*=========================*/
                    $('#progress_text').html('100% Complete');
                    $('#progress').css('width','339px');
                    
                    /*=========================*/
                    /*== Get Entered Values  ==*/
                    /*=========================*/
                    var username = $('#username').val();
                    var password = $('#password').val();
                    var email = $('#email').val();
                    var name = $('#name').val();
                    var surname = $('#surname').val();
                    var age = $('#age').val();
                    var gender = $('#gender').val();
                    var country = $('#country').val();
                    var city = $('#city').val();
                    var postal = $('#postal').val();
                    var userIP = '<?php echo $_SERVER['REMOTE_ADDR']; ?>';
                    
                    var adress = $('#adress').val();
                    var phone = $('#phone').val();
                    var mobile = $('#mobile').val();
                    var web = $('#web').val();
                    
                    /*===============================*/
                    /*== Post Values to 'Register' ==*/
                    /*===============================*/
                    $.post( "<?php echo WEB_URL; ?>checklogindata.html", { 
                                                                            submit: "Register",
                                                                            type: "6",
                                                                            username: username, 
                                                                            password: password, 
                                                                            email: email,
                                                                            name: name,
                                                                            surname: surname,
                                                                            age: age,
                                                                            gender: gender,
                                                                            country: country,
                                                                            city: city,
                                                                            postal: postal,
                                                                            userIP: userIP,
                                                                            address: adress,
                                                                            phone: phone,
                                                                            mobile: mobile,
                                                                            web: web},
                    
                        /*====================================*/
                        /*== Check 'Register' Response Data ==*/
                        /*====================================*/                                       
                        function( data ) {
                            if(data != 'OK' ){
                            
                                alert(data);
                                
                                $('#progress_text').html('0% Complete');
                                $('#progress').css('width','0px');
                            }else{
                            
                                $('#Username').html(username);
                                $('#Password').html('******');
                                $('#Email').html(email);
                                $('#Name').html(name);
                                $('#Surname').html(surname);
                                $('#Age').html(age);
                                $('#Gender').html(gender);
                                $('#Country').html(country);
                                $('#City').html(city);
                                $('#Postal').html(postal);
                                
                                /*=========================*/
                                /*== Slide to UserDone!  ==*/
                                /*=========================*/
                                $('#fifth_step').slideUp();
                                $('#user_done').slideDown();                            

                                /*
                                alert("Developer Registration OK! Check Email!");
                                window.top.location.href = '<?php echo WEB_URL; ?>';
                                */
                            }
                        }
                    );      
                }              
                
        }
        
    });

});