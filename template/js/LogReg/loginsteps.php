<?php 
header("Content-type: text/javascript");
/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
require ('../../../include/config.inc.php');
include ROOT.'include/functions.inc.php';

/*============================*/
/*=== Get Language From Url ==*/
/*============================*/
if($_GET['lng'] && $_GET['lng'] != ''){
    if(strlen(trim($_GET['lng'])) >= 4){
        $siteLanguage = CleanUrlData($_GET['lng']);
    }else{
        $siteLanguage = 'GB';
    }
}
/*============================*/
/*=== Get Country  From Url ==*/
/*============================*/
if($_GET['c'] && $_GET['c'] != ''){
    if(strlen(trim($_GET['c'])) >= 4){
        $country = CleanUrlData($_GET['c']);        
    }else{
        $country = 'United Kingdom';
    }        
}
/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}
?>
$(function(){

    /*====================*/
    /*= Captcha Refresh ==*/
    /*====================*/

    $("#captcha-refresh").live("click", function(){
        document.getElementById('captcha').src="<?php echo WEB_URL; ?>captcha.jpg?r=" + Math.random();
    });
    /*============================*/
    /*= Simple Password Encrypt ==*/
    /*============================*/     
    String.prototype.rot13 = function(){
        return this.replace(/[a-zA-Z]/g, function(c){
            return String.fromCharCode((c <= "Z" ? 90 : 122) >= (c = c.charCodeAt(0) + 13) ? c : c - 26);
        });
    };  
    /*======================*/
    /*= Main Field Values ==*/
    /*======================*/
    var field_values = {
            //id        :  value
            'username'  : '<?php echo USERNAME; ?>',
            'password'  : '<?php echo PASSWORD; ?>',
    };
    
    /*========================*/
    /*= Max Login Atempts ====*/
    /*========================*/
    counter = 3;


    /*===============*/
    /*= InputFocus ==*/
    /*===============*/
    $('input#username').inputfocus({ value: field_values['username'] });
    $('input#password').inputfocus({ value: field_values['password'] });

    /*=============================*/
    /*= Reset Progress Bar OnLoad==*/
    /*=============================*/
    $('#progress').css('width','0');
    $('#progress_text').html('0% Complete');

    /*===================*/
    /*=== Form Submit ===*/
    /*===================*/
    $('form').submit(function(){ return false; });
    $('#user_done').click(function(){
        /*=================================*/
        /*=== RemoveError/Valid Classes ===*/
        /*=================================*/
        $('#first_step input').removeClass('error').removeClass('valid');
        
        /*===================================*/
        /*=== Check if Inputs Arent Empty ===*/
        /*===================================*/
        var fields = $('#first_step input[type=text], #first_step input[type=password]');
        var error = 0;
        fields.each(function(){
            var value = $(this).val();
            if( value.length<4 || value==field_values[$(this).attr('id')] ) {
                $(this).addClass('error');
                $(this).effect("shake", { times:3 }, 50);
                
                error++;
            } else {
                $(this).addClass('valid');
            }
        });        
        /*====================*/
        /*=== If Inputs OK ===*/
        /*====================*/
        if(!error) {
            if( $('#password').val() == '' ) {
                    $('#first_step input[type=password]').each(function(){
                        /*====================*/
                        /*== If Pass Empty ===*/
                        /*====================*/
                        $(this).removeClass('valid').addClass('error');
                        $(this).effect("shake", { times:3 }, 50);
                        $('#progress_text').html('33% Complete');
                        $('#progress').css('width','113px');
                    });
                    
                    return false;
            }else{   
                /*==========================*/
                /*== Update Progress Bar ===*/
                /*==========================*/
                $('#progress_text').html('100% Complete');
                $('#progress').css('width','339px');
                
                /*==============================*/
                /*== Get Input Values to var ===*/
                /*==============================*/
                var uname = $('#username').val();
                var pass = $('#password').val().rot13();
                var rememberMe = $("input:checked").length;
                
                /*==================================*/
                /*== If Captcha in DOM get Value ===*/
                /*==================================*/
                if($('#captcha-code').length > 0) {
                   var captcha = $('#captcha-code').val().rot13();
                }else{
                    var captcha = '1';
                }
                
                /*=======================================*/
                /*== Post/Check Data and set SESSIONS ===*/
                /*=======================================*/
                $.post( "<?php echo WEB_URL; ?>checklogindata.html", { submit: "Login", username: uname, password: pass, rememberMe: rememberMe, lng: '<?=$siteLanguage?>', c: '<?=$country?>', captcha: captcha},
                    function( data ) {
                        /*================================================*/
                        /*== Wrong UName/P Error set Atempt/Counter -1 ===*/
                        /*================================================*/
                        if(data != 'OK' ){
                            counter = counter - 1;
                            /*=====================================*/
                            /*== IF Counter expired set Captcha ===*/
                            /*=====================================*/
                            if(counter < 0 || counter == 0 && data != 'CAPTCHA_ERROR'){
                                $('#loginerror').empty();
                                $("#captcha-outwrap").empty();
                                $("#captcha-outwrap").append('<div id="captcha-wrap"><div class="captcha-box"><img src="<?php echo WEB_URL; ?>captcha.jpg" alt="" id="captcha" /></div><div class="text-box"><label>Type the two words:</label><input name="captcha-code" type="text" id="captcha-code"></div><div class="captcha-action"><img src="<?php echo WEB_URL; ?>template/img/captcha/refresh.jpg"  alt="Refresh Captcha" id="captcha-refresh" /></div></div>');
                                $("#captcha-outwrap").fadeIn(100);
                            }
                            if(counter > 0){
                                $('#loginerror').empty();
                                $("#loginerror").append(data + '<br />Click Here to reset your login data!<br />You have '+counter+' retries left.');
                                $("#loginerror").fadeIn(100);
                            }
                            if(data == 'CAPTCHA_ERROR'){
                                $('#loginerror').empty();
                                $("#loginerror").append('<br />Wrong Captcha');
                                $("#loginerror").fadeIn(100);
                            }
                            $('#progress_text').html('0% Complete');
                            $('#progress').css('width','0px');
                        }else{
                            alert("Login Ok!");
                            window.top.location.href = '<?php echo WEB_URL; ?>';
                        }
                    }
                );




            }               
        } else return false;
    });

});