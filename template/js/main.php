<?php 
if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')){ ob_start("ob_gzhandler"); }else{ ob_start();}
header("Content-type: text/javascript");
require ('../../include/config.inc.php');
include ROOT.'include/functions.inc.php';

// GET Language from URL
if($_GET['lng'] && $_GET['lng'] != ''){
    if(strlen(trim($_GET['lng'])) >= 4){
        $siteLanguage = CleanUrlData($_GET['lng']);
    }else{
        $siteLanguage = 'GB';
    }
}
// Get Country from URL
if($_GET['c'] && $_GET['c'] != ''){
    if(strlen(trim($_GET['c'])) >= 4){
        $country = CleanUrlData($_GET['c']);        
    }else{
        $country = 'United Kingdom';
    }        
}
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}
?>
$(document).ready(function(){	
    
        if (!Modernizr.input.placeholder)
        {

                var placeholderText = $('#search').attr('placeholder');

                $('#search').attr('value',placeholderText);
                $('#search').addClass('placeholder');

                $('#search').focus(function() {				
                        if( ($('#search').val() == placeholderText) )
                        {
                                $('#search').attr('value','');
                                $('#search').removeClass('placeholder');
                        }
                });

                $('#search').blur(function() {				
                        if ( ($('#search').val() == placeholderText) || (($('#search').val() == '')) )                      
                        {	
                                $('#search').addClass('placeholder');					  
                                $('#search').attr('value',placeholderText);
                        }
                });
        }    
	function megaHoverOver(){
		$(this).find(".sub").stop().fadeTo('fast', 1).show();
			
		//Calculate width of all ul's
		(function($) {
			jQuery.fn.calcSubWidth = function() {
				rowWidth = 0;
				//Calculate row
				$(this).find("ul").each(function() {					
					rowWidth += $(this).width(); 
				});	
			};
		})(jQuery); 
		
		if ( $(this).find(".row").length > 0 ) { //If row exists...
			var biggestRow = 0;	
			//Calculate each row
			$(this).find(".row").each(function() {							   
				$(this).calcSubWidth();
				//Find biggest row
				if(rowWidth > biggestRow) {
					biggestRow = rowWidth;
				}
			});
			//Set width
			$(this).find(".sub").css({'width' :biggestRow});
			$(this).find(".row:last").css({'margin':'0'});
			
		} else { //If row does not exist...
			
			$(this).calcSubWidth();
			//Set Width
			$(this).find(".sub").css({'width' : rowWidth});
			
		}
	}
	
	function megaHoverOut(){
	 $(this).find(".sub").stop().fadeTo('fast', 0, function() {
		  $(this).hide();
	  });
	}


	var config = {
		 sensitivity: 2,		// number = sensitivity threshold (must be 1 or higher)    
		 interval: 0, 			// number = milliseconds for onMouseOver polling interval    
		 over: megaHoverOver,   // function = onMouseOver callback (REQUIRED)    
		 timeout: 0, 			// number = milliseconds delay before onMouseOut    
		 out: megaHoverOut 		// function = onMouseOut callback (REQUIRED)    
	};

	$("ul#topnav li .sub").css({'opacity':'0'});
	$("ul#topnav li").hoverIntent(config);
	
	// Add class to the current element
	$("ul#topnav li").click(function(){
		$("ul#topnav").find("li.current").removeClass().removeAttr("class");
		$(this).addClass("current");
	});
	
	// Change arrow on hover
	$("li.liSub").hover(function(){
		$(this).find("span img").attr("src", "<?php echo WEB_URL; ?>template/img/topmenu/arrow_hover.png");
	},function(){
		$(this).find("span img").attr("src", "<?php echo WEB_URL; ?>template/img/topmenu/arrow.png");
	});
	
	// Delete the border for the last li of the submenu
	$("ul#topnav .sub ul li:last-child a").css("border", "none");
 
        $('#top-slider').coinslider({ width: 1000, height: 410, effect: 'random', navigation: true, links : true, delay: 5000, hoverPause: true});
        /*======================*/
        /*=== Slider Options ===*/
        /*======================*/ 
        /*
        width: 565, // width of slider panel
        height: 290, // height of slider panel
        spw: 7, // squares per width
        sph: 5, // squares per height
        delay: 3000, // delay between images in ms
        sDelay: 30, // delay beetwen squares in ms
        opacity: 0.7, // opacity of title and navigation
        titleSpeed: 500, // speed of title appereance in ms
        effect: '', // random, swirl, rain, straight
        navigation: true, // prev next and buttons
        links : true, // show images as links
        hoverPause: true // pause on hover
         */
        
        //http://www.ericmmartin.com/projects/simplemodal/
        $("#login").click(function(){        
            var src = "login_form.html";
            $.modal('<iframe src="' + src + '?lng=<?=$siteLanguage?>&c=<?=urlencode($country)?>" height="470" width="755" style="border:0" frameborder="0" scrolling="no">', {
                    minHeight:470,
                    minWidth: 755
            });
        });
        $("#register").click(function(){
            var src = "register_form.html";
            $.modal('<iframe src="' + src + '?lng=<?=$siteLanguage?>&c=<?=urlencode($country)?>" height="470" width="755" style="border:0" frameborder="0" scrolling="no">', {
                    minHeight:470,
                    minWidth: 755
            });
        });

});