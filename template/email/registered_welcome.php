<?php
/*===============================*/
/*===  Include Global Config ====*/
/*===============================*/
require ('../../include/config.inc.php');
require (ROOT.'include/databse.inc.php');
/*=======================*/
/*===  DB Connection ====*/
/*=======================*/
$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect(); 

/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
include ROOT.'include/functions.inc.php';

/*==================================================*/
/*=========       CSS/JS Minify Include  ===========*/
/*=== min/?f=template/js/jquery.js,scripts/site.js==*/
/*=== min/?f=template/css/LogReg/login.css        ==*/
/*==================================================*/
require ROOT.'/min/utils.php';
$cssUri = Minify_getUri(array(
     '//template/css/LogReg/email_welcome.css'
)); // a list of files


/*============================*/
/*=== Get Language From Url ==*/
/*============================*/
if($_GET['lng'] && $_GET['lng'] != ''){
    if(strlen(trim($_GET['lng'])) >= 4){
        $siteLanguage = CleanUrlData($_GET['lng']);
    }else{
        $siteLanguage = 'GB';
    }
}
/*============================*/
/*=== Get Country  From Url ==*/
/*============================*/
if($_GET['c'] && $_GET['c'] != ''){
    if(strlen(trim($_GET['c'])) >= 4){
        $country = CleanUrlData($_GET['c']);        
    }else{
        $country = 'United Kingdom';
    }        
}
/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}

/*============================*/
/*=== Get UNIQUE user Salt ===*/
/*============================*/

if($_GET['salt'] && $_GET['salt'] != ''){
    if(strlen(trim($_GET['salt'])) == 32){
        /*============================*/
        /*=== Populate User Object ===*/
        /*============================*/
        $data_users = array();
        $data_users = userObject($_GET['salt']);
        
        /*===================================================*/
        /*=== How Many Days has passed since registration ===*/
        /*===================================================*/
        if($data_users != false){
            /*==============================================*/
            /*=== Get Registered and NOW time in seconds ===*/
            /*==============================================*/
            $data_users['strtotime_created'] = strtotime($data_users['created']);
            $data_users['strtotime_now'] = time();
            /*============================*/
            /*=== Calculate Difference ===*/
            /*============================*/
            $data_users['strtotime_diff'] = $data_users['strtotime_created'] - $data_users['strtotime_now'];            
            $data_users['strtotime_passed'] = abs(floor($data_users['strtotime_diff'] / 86400));
        }

    }else{
        $data_users = false;
    }        
}else{
    $data_users = false;
}

/*===================================*/
/*=== If no User or X days passed ===*/
/*===================================*/
if($data_users == FALSE || $data_users['strtotime_passed'] > USER_ACTIVATE){
    echo "Unknown User!";
    exit(0);
}

?>
<!doctype html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />        
        <!-- Facebook sharing information tags -->
        <meta property="og:title" content="*|MC:SUBJECT|*" />
        <link rel="stylesheet" href="<?php echo $cssUri; ?>" type="text/css" media="all" />
        
        <title><?php echo WEL_EMAIL_SUBJ; ?></title>

	</head>
    <body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<center>
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="backgroundTable">
            	<tr>
                	<td align="center" valign="top">
                        <!-- // Begin Template Preheader \\ -->
                        <table border="0" cellpadding="10" cellspacing="0" width="600" id="templatePreheader">
                            <tr>
                                <td valign="top" class="preheaderContent">
                                	
                                	<!-- // Begin Module: Standard Preheader \ -->
                                    <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                    	<tr>
                                        	<td valign="top">
                                            	<div mc:edit="std_preheader_content">
                                                	 <?php echo WEL_EMAIL_SUBJ; ?>
                                                </div>
                                            </td>
                                            <!-- *|IFNOT:ARCHIVE_PAGE|* -->
											<td valign="top" width="190">
                                            	<div mc:edit="std_preheader_links">
                                                	Is this email not displaying correctly?<br /><a href="<?php echo WEB_URL."users/email/welcome/".$data_users['salt']; ?>" target="_blank">View it in your browser</a>.
                                                </div>
                                            </td>
											<!-- *|END:IF|* -->
                                        </tr>
                                    </table>
                                	<!-- // End Module: Standard Preheader \ -->
                                
                                </td>
                            </tr>
                        </table>
                        <!-- // End Template Preheader \\ -->
                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateContainer">
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Header \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateHeader">
                                        <tr>
                                            <td class="headerContent">
                                            
                                            	<!-- // Begin Module: Standard Header Image \\ -->
                                            	<img src="http://www.kisscs.net/wp-content/uploads/2012/03/hack1-600x150.jpg" style="max-width:600px;" id="headerImage campaign-icon" mc:label="header_image" mc:edit="header_image" mc:allowdesigner mc:allowtext />
                                            	<!-- // End Module: Standard Header Image \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Header \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Body \\ -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="templateBody">
                                    	<tr>
                                        	<td valign="top" width="400">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="400">
                                                	<tr>
                                                    	<td valign="top">
                                            				<table border="0" cellpadding="0" cellspacing="0" width="400">
                                                            	<tr>
                                                                	<td valign="top" class="bodyContent">
                                                            
                                                                        <!-- // Begin Module: Standard Content \\ -->
                                                                        <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                            <tr>
                                                                                <td valign="top">
 						                                                            <div mc:edit="std_content00">
						                                                                <h3 class="h3">Dear <?php echo $data_users['name']; ?> <?php echo $data_users['surname']; ?>,</h3>
						                                                                <strong>Getting started:</strong><br />
                                                                                                                You've entered <i><?php echo $data_users['email']; ?></i> as the contact email address for your <?php echo WEB_NAME; ?> ID. 
                                                                                                                To complete the process, we just need to verify that this email address belongs to you. 
                                                                                                                <br />Simply click the link below and sign in using your <?php echo WEB_NAME; ?> ID and password.
                                                                                                                <br />
						                                                                <br />
                                                                                                                <a href="<?php echo WEB_URL."users/activate/".$data_users['salt']; ?>">Verify Now &#62;</a>
						                                                                <br />
						                                                                <br />
						                                                                <strong>Wondering why you got this email?</strong><br />
                                                                                                                It's sent when someone adds or changes a contact email address for an <?php echo WEB_NAME; ?> ID account. 
                                                                                                                If you didn't do this, don't worry. 
                                                                                                                Your email address cannot be used as a contact address for an <?php echo WEB_NAME; ?> ID without your verification.
                                                                                                                <br />
						                                                                <br />
                                                                                                                <strong>Your Account Information:</strong> 
                                                                                                                <table id="user_done_table">
                                                                                                                        <tr>
                                                                                                                            <td>Username: </td>
                                                                                                                            <td id="Username"><?php echo $data_users['username']; ?></td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>Password: </td>
                                                                                                                            <td id="Password">******</td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>Email: </td>
                                                                                                                            <td id="Email"><?php echo $data_users['email']; ?></td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>Name: </td>
                                                                                                                            <td id="Name"><?php echo $data_users['name']; ?></td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>Surname: </td>
                                                                                                                            <td id="Surname"><?php echo $data_users['surname']; ?></td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>Country: </td>
                                                                                                                            <td id="Country"><?php echo $data_users['country_name']; ?></td>
                                                                                                                        </tr>
                                                                                                                        <tr>
                                                                                                                            <td>Created: </td>
                                                                                                                            <td id="Created"><?php echo $data_users['created']; ?></td>
                                                                                                                        </tr>
                                                                                                                   </table>
                                                                                                            </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                        <!-- // End Module: Standard Content \\ -->
                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                		</td>
                                                    </tr>                                                    
                                                </table>                                                
                                            </td>
                                            <!-- // Begin Sidebar \\ -->
                                        	<td valign="top" width="200" id="templateSidebar">
                                            	<table border="0" cellpadding="0" cellspacing="0" width="200">
                                                	<tr>
                                                    	<td valign="top" class="sidebarContent">
                                                        
                                                            <!-- // Begin Module: Social Block with Icons \\ -->
                                                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td valign="top" style="padding-top:10px; padding-right:20px; padding-left:20px;">
                                                                        <table border="0" cellpadding="0" cellspacing="4">
                                                                            <tr mc:hideable>
                                                                                <td align="left" valign="middle" width="16">
                                                                                    <img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/sfs_icon_facebook.png" style="margin:0 !important;" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <div mc:edit="sbwi_link_one">
                                                                                        <a href="<?php echo SOCIAL_FCBOOK; ?>">Friend on Facebook</a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr mc:hideable>
                                                                                <td align="left" valign="middle" width="16">
                                                                                    <img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/sfs_icon_twitter.png" style="margin:0 !important;" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <div mc:edit="sbwi_link_two">
                                                                                        <a href="<?php echo SOCIAL_TWITTER; ?>">Follow on Twitter</a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                            <tr mc:hideable>
                                                                                <td align="left" valign="middle" width="16">
                                                                                    <img src="http://gallery.mailchimp.com/653153ae841fd11de66ad181a/images/sfs_icon_forward.png" style="margin:0 !important;" />
                                                                                </td>
                                                                                <td align="left" valign="top">
                                                                                    <div mc:edit="sbwi_link_three">
                                                                                        <a href="<?php echo SOCIAL_GPLUS; ?>">Like us on Google+</a>
                                                                                    </div>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Module: Social Block with Icons \\ -->

                                                            <!-- // Begin Module: Top Image with Content \\ -->
                                                            <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                                <tr mc:repeatable>
                                                                    <td valign="top">
                                                                        <img src="<?php echo WEB_URL."template/img/LogReg/email/internet_reload.png"; ?>" style="max-width:160px; margin-bottom:10px;" mc:edit="tiwc200_image00" />
                                                                        <div mc:edit="tiwc200_content00">
                                                                            <h4 class="h4"><?php echo ABOUT." ".WEB_NAME; ?> :</h4>
                                                                            is the place where people go to discover downloads.
                                                                            Featuring rated reviews, <?php echo WEB_NAME; ?> is the trusted, safe, and secure resource for applications, and game downloads.
                                                                            Our editors and staff use downloadable content in our daily lives, and we understand the 
                                                                            need for a Web site that accurately and independently presents detailed information, editorial opinions, 
                                                                            industry expertise, and media content.
                                                                            We are constantly working to earn your trust.
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            <!-- // End Module: Top Image with Content \\ -->
                                                        
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <!-- // End Sidebar \\ -->
                                        </tr>
                                    </table>
                                    <!-- // End Template Body \\ -->
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- // Begin Template Footer \\ -->
                                	<table border="0" cellpadding="10" cellspacing="0" width="600" id="templateFooter">
                                    	<tr>
                                        	<td valign="top" class="footerContent">
                                            
                                                <!-- // Begin Module: Standard Footer \\ -->
                                                <table border="0" cellpadding="10" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="social">
                                                            <div mc:edit="std_social">
                                                                &nbsp;<a href="<?php echo SOCIAL_TWITTER; ?>">follow on Twitter</a> | <a href="<?php echo SOCIAL_FCBOOK; ?>">friend on Facebook</a> | <a href="<?php echo SOCIAL_GPLUS; ?>">like on Google+</a>&nbsp;
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td valign="top" width="350">
                                                            <div mc:edit="std_footer">
																<em>Copyright &copy; <?php echo date('Y'); ?> <?php echo WEB_NAME; ?>, All rights reserved.</em>
																<br />
																<strong>Our mailing address is:</strong>
																<br />
																<?php echo EMAIL_INFO; ?>
                                                            </div>
                                                        </td>
                                                        <td valign="top" width="190" id="monkeyRewards">
                                                            <div mc:edit="monkeyrewards">
                                                                <strong>DO NOT REPLY TO THIS E-MAIL</strong>
                                                                Please do not reply to this email as this address is not monitored,
                                                                reply through your ticket link above. 
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" valign="middle" id="utility">
                                                            <div mc:edit="std_utility">
                                                                &nbsp;<a href="<?php echo WEB_URL."users/email/unsubscribe/".$data_users['salt']; ?>">unsubscribe from this list</a> | <a href="<?php echo WEB_URL."users/update/".$data_users['salt']; ?>">update subscription preferences</a>&nbsp;
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                                <!-- // End Module: Standard Footer \\ -->
                                            
                                            </td>
                                        </tr>
                                    </table>
                                    <!-- // End Template Footer \\ -->
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
            </table>
        </center>
    </body>
</html>