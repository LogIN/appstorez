<?php
require ('../../include/config.inc.php');
/*=====================*/
/*=== Session Start ===*/
/*=====================*/
session_name(md5(WEB_NAME));
session_start();
/*=======================*/
/*=== Forece No-Cache ===*/
/*=======================*/
ini_set('max_execution_time', 30);
header('Content-Type: text/javascript; charset=utf8');
header('Access-Control-Max-Age: 3628800');
header('Access-Control-Allow-Methods: GET');
header('content-type: application/json; charset=utf-8');
/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
require (ROOT.'include/databse.inc.php');
require (ROOT.'include/sphinxapi.php');
/*=======================*/
/*===  DB Connection ====*/
/*=======================*/
$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect(); 
/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
include ROOT.'include/functions.inc.php';

/*==================================*/
/*===  SITE LANGUAGE TODO:FIX   ====*/
/*==================================*/
$siteLanguage = 'GB';
include ROOT."include/lng/".$siteLanguage.".php";

$user = array();
// all the filter arrays
$sphinx_dev_id = array();
$sphinx_app_App2SD = array();
$sphinx_app_price = array();
$sphinx_app_category = array();
$sphinx_app_rating_avg = array();

/*===========================*/
/*=== Get Values From URL ===*/
/*===========================*/
/*
?salt=8809bef6b5c7ffcf41be279a127587f3
&q=com
&page=1
&callback=MyJson
&limit=20
&sortby=app_added   --> OPTIONAL
&sortorder=DESC     --> OPTIONAL
&dev_id=DESC        --> OPTIONAL
&app2SD=DESC        --> OPTIONAL
&price=DESC         --> OPTIONAL
&catrgory=DESC      --> OPTIONAL
&rating_avg=4       --> OPTIONAL
*/  
// Get SALT From URL, Check and Vaildate Search User
if(isset($_GET['salt'])){ $salt = trim(htmlspecialchars($_GET['salt'], ENT_QUOTES)); }else{ $salt = FALSE; }
// Get Main Query String
if(isset($_GET['q'])){ $query = trim(html_entity_decode($_GET['q'], ENT_QUOTES, 'UTF-8')); }else{ $query = FALSE; }
// Ajax Json CallBack
if(isset($_GET['callback'])){ $callback = $_GET["callback"]; }else{ $callback = false; }
// Get Current Page Number.
if(!isset($_GET['page']) || $_GET['page'] == '0'){ $pagenum = 1; }else{ $pagenum = (int)$_GET['page']; }
// Limit Results per page
if(!isset($_GET['limit']) || $_GET['limit'] == '0'){ $limit = 20; }else{ $limit = (int)$_GET['limit']; }
// SortBy Field -- Defoult BY Date Descending (Newest first)
if(!isset($_GET['sortby'])){ $sortby = FALSE; }else{ $sortby = $_GET['sortby']; }
// SortBy Attribute
if(!isset($_GET['sortorder'])){ $sortorder = 'ASC'; }else{ $sortorder = $_GET['sortorder']; }
// SPHINX FILTER VARIABLES
if(isset($_GET['dev_id']) && $_GET['dev_id'] != 'NONE'){ $sphinx_dev_id[] = $_GET["dev_id"]; }
if(isset($_GET['app2SD']) && $_GET['app2SD'] != 'NONE'){ $sphinx_app_App2SD[] = $_GET["app2SD"]; }
if(isset($_GET['price']) && $_GET['price'] != 'NONE'){ $sphinx_app_price[] = $_GET["price"]; }
if(isset($_GET['rating_avg']) && $_GET['rating_avg'] != 'NONE'){ $sphinx_app_rating_avg[] = $_GET["rating_avg"]; }
// DEFINE ON OR MORE CATEGORIES
if(isset($_GET['category']) && $_GET['category'] != 'NONE'){
    // IF exploadible
    if(strpos($_GET['category'], ',') !== false) {
        $sphinx_app_category = explode(",", $_GET['category']);
    // explodable
    } else {
        $sphinx_app_category[] = $_GET["category"]; 
    }    
}

// If User SALT Exsist Populate USER ARRAY
if($salt != FALSE){$user = userObject($salt); $user[query] = $query; }else{ exit(0); }
// If User Exsist And Valid, Query isnt "", and user has perrmision to search (user[type]) goon else quit
if ($user === FALSE || $user[user_verified] === 0 || $user[user_type] != '1' || $user[query] === FALSE){ exit(0); }

/*======================================*/
/*=== Count Requests to detect Robot ===*/
/*======================================*/
if(isSet($_SESSION['brute_detect'])){ $_SESSION['brute_detect'] += 1; }else{ $_SESSION['brute_detect'] = 0; }
if($_SESSION['brute_detect'] > 100){ exit(0); }

/*=======================*/
/*=== Configure Query ===*/
/*=======================*/
// basic setup
// http://gadelkareem.com/2012/03/12/install-sphinx-2-0-4-on-centos-6-2/
$cl = new SphinxClient ();
// Check if requested category is in common cats list
if (isset($common_sorted[$_GET['category']])){
    // Reset Sphinx Filter Ranges
    $sphinx_app_category = 0;
    // Reset Cat Filters
   
    if($_GET['category'] == 'CAT_T_P'){
        $sphinx_CAT_T_P = 0;
        $index = "CAT_T_P"; 
        
    }elseif($_GET['category'] == 'CAT_T_F'){
        $sphinx_CAT_T_F = 0;
        $index = "CAT_T_F"; 
        
    }elseif($_GET['category'] == 'CAT_T_N_P'){
        $sphinx_CAT_T_N_P = 0;
        $index = "CAT_T_N_P"; 
        
    }elseif($_GET['category'] == 'CAT_T_N_F'){
        $sphinx_CAT_T_N_F = 0;
        $index = "CAT_T_N_F"; 
        
    }elseif($_GET['category'] == 'CAT_M_V'){
        // NOT CONFIGURED IN SPHINX USING GENERAL INDEX
        $sphinx_CAT_M_V = 0;
        $index = "general";
        
    }elseif($_GET['category'] == 'CAT_M_C'){
        // NOT CONFIGURED IN SPHINX USING GENERAL INDEX
        $sphinx_CAT_M_C = 0;
        $index = "general";
        
    }elseif($_GET['category'] == 'CAT_M_R'){
        $sphinx_CAT_M_R = 0;
        $index = "CAT_M_R";
        
    }elseif($_GET['category'] == 'CAT_M_D'){
        // NOT CONFIGURED IN SPHINX USING GENERAL INDEX
        $sphinx_CAT_M_D = 0;
        $index = "general";        
    }    
}else{
    // the fake index that contains the full app data index
    $index = "general"; 
} 

// http://sphinxsearch.com/docs/manual-0.9.8.html#sorting-modes
$mode = SPH_MATCH_EXTENDED;
$ranker = SPH_RANK_PROXIMITY_BM25;
/*
$ranker = SPH_RANK_BM25;
$ranker = SPH_RANK_NONE;
$ranker = SPH_RANK_WORDCOUNT;
$ranker = SPH_RANK_FIELDMASK;
$ranker = SPH_RANK_SPH04;
$mode = SPH_MATCH_ANY;
$mode = SPH_MATCH_BOOLEAN;
$mode = SPH_MATCH_EXTENDED;
$mode = SPH_MATCH_EXTENDED2;
$mode = SPH_MATCH_PHRASE;
 */
$cl->SetServer(SPHINX_HOST, SPHINX_PORT);
$cl->SetLimits(($pagenum - 1) * $limit, $limit, ($pagenum*$limit));
$cl->SetMatchMode($mode);
$cl->SetConnectTimeout(1);
$cl->SetArrayResult(true);
$cl->SetWeights(array (100, 1));

// set the filters
if (count($sphinx_dev_id) > 0 ){         $cl->SetFilter ( 'dev_id', $sphinx_forumids);}
if (count($sphinx_app_App2SD) > 0 ){     $cl->SetFilter ( 'app_App2SD', $sphinx_app_App2SD);}
if (count($sphinx_app_price) > 0 ){      $cl->SetFilter ( 'app_price', $sphinx_app_price);}
if (count($sphinx_app_category) > 0 ){   $cl->SetFilter ( 'app_category', $sphinx_app_category);}
if (count($sphinx_app_rating_avg) > 0 ){ $cl->SetFilter ( 'app_rating_avg', $sphinx_app_rating_avg);}

/*
if (count($sphinx_CAT_T_F) > 0 ){ $cl->SetFilterRange($attribute, $min, $max, true);}
if (count($sphinx_CAT_T_N_P) > 0 ){ $cl->SetFilterRange($attribute, $min, $max, true);}
if (count($sphinx_CAT_T_N_F) > 0 ){ $cl->SetFilterRange($attribute, $min, $max, true);}
if (count($sphinx_CAT_M_V) > 0 ){ $cl->SetFilterRange($attribute, $min, $max, true);}
if (count($sphinx_CAT_M_C) > 0 ){ $cl->SetFilterRange($attribute, $min, $max, true);}
if (count($sphinx_CAT_M_R) > 0 ){ $cl->SetFilterRange($attribute, $min, $max, true);}
if (count($sphinx_CAT_M_D) > 0 ){ $cl->SetFilterRange($attribute, $min, $max, true);}
*/

// sorting options (a bit messy I know)
if ($sortby != FALSE){
    if($sortorder == 'ASC'){            
        $cl->SetSortMode ( SPH_SORT_ATTR_ASC, $sortby);
    }elseif($sortorder == 'DESC'){
        $cl->SetSortMode ( SPH_SORT_ATTR_DESC, $sortby); 
    }            
}else{
    $cl->SetSortMode (SPH_SORT_RELEVANCE);
}

$cl->SetRankingMode ( $ranker );
$cl->SetArrayResult(true);

// execute!
$res = $cl->Query ($user[query], $index); 

/*
$ids = implode(", ", $result);
SELECT * FROM table WHERE id IN ($ids) ORDER BY FIELD(id, $ids)
$sql = "SELECT apps.dev_id, apps.app_hash, apps.app_com, apps.app_name, apps.app_size, apps.app_installs, apps.app_rating, apps.app_rating_avg, apps.app_description, apps.app_youtube_id, apps.app_version, apps.app_App2SD, apps.app_os, apps.app_os_version, apps.app_added, apps.app_icon, apps.app_price, users.user_id, users.user_type, users_profile.user_prof_name, users_profile.user_prof_web, users_profile.user_prof_logo, list_categories.cat_name, list_countries.name AS country_name FROM `apps` LEFT JOIN users ON apps.dev_id = users.user_id LEFT JOIN users_profile ON users.user_id=users_profile.user_prof_guid LEFT JOIN list_categories ON apps.app_category = list_categories.cat_id LEFT JOIN list_countries ON users_profile.user_prof_country=list_countries.id WHERE apps.app_id IN (".$IDs.");";
*/

$result = array();
$result[requests] = $_SESSION['brute_detect'];

// we got results
if($res === FALSE ){
    $result[LastError] = $cl->GetLastError();
}else{
    if ( $cl->GetLastWarning() ){
        $result[LastWarning] = $cl->GetLastWarning();
    }

    $result[query] = $user[query];
    $result[total] = $res[total];
    $result[total_found] = $res[total_found];
    $result[time] = $res[time];

    if (is_array($res["matches"])){
        $n = 1;
        foreach($res["matches"] as $docinfo ){
                $result[ids][] = $docinfo;
                $n++;
        }
    }        
}
$debug = $result;
$result = json_encode($result);
if($callback != false && $callback != 'debug' && $callback != 'PHPSearch'){
    print ($callback."(".$result.");");
}elseif($callback == 'debug'){
    var_dump($debug);
}else{
    print $result;
}
unset($result);
unset($user);
// Unset Filter Arrays
unset($sphinx_dev_id);
unset($sphinx_app_App2SD);
unset($sphinx_app_price);
unset($sphinx_app_category);
unset($sphinx_app_rating_avg);
?>