#include <QtCore/QCoreApplication>
#include <QtGui>
#include <QtSql>
#include <QtWebKit>
#include <QWebPage>
#include <iostream>
#include <qtextstream.h>
#include <iostream>
#include <QProcess>
#include <QCryptographicHash>
#include <QByteArray>
#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include <QtXml/QDomText>



using namespace std;

int main(int argc, char *argv[]) {
    QTextStream cout(stdout, QIODevice::WriteOnly);
    char *urlfile;
    cout << "Program Started!! \n";

    if(argc > 0) //check input number of input parameter
    {
        QSqlDatabase db = QSqlDatabase::addDatabase("QMYSQL");
        db.setHostName("208.53.183.78");
        db.setDatabaseName("appstore_main");
        db.setUserName("appstore_main");
        db.setPassword("EiZ,$C1_e{2?");
        bool ok = db.open();

        // Get Url File with RSS Feeds, Parse rss feeds and insert them into DB
        cout << "Step 1 Started - Inserting RSS into MYSQL\n";
        urlfile = argv[1];

        QFile inputFile(urlfile);
        if (inputFile.open(QIODevice::ReadOnly))
        {
           QTextStream in(&inputFile);

           while ( !in.atEnd() )
           {
              QString domain = in.readLine();

              // After Thumb Get Category
              QString title = "0";
              QString link = "0";
              QString comstring = "0";
              QRegExp rx("http://www.appbrain.com/app/(.*)/(.*)");



              QString catcommand = "curl " + domain;
              QProcess *catProcess = new QProcess();
              catProcess->start(catcommand);

              if (catProcess->waitForStarted(1000) == false)
                  cout << "Error starting external program";

              if (catProcess->waitForFinished()){

                  QByteArray catresult = catProcess->readAllStandardOutput();
                  QString catresultstr(catresult.data());
                  catresultstr = catresultstr.trimmed();

                  if(!catresultstr.isEmpty()){
                      QDomDocument doc;
                      doc.setContent(catresult);
                      QDomNodeList nodeList = doc.elementsByTagName("channel").at(0).toElement().elementsByTagName("item");
                      if (nodeList.count() > 0)
                      {
                         for(int iDx = 1; iDx <= nodeList.count(); iDx++)
                             {
                                 link = doc.elementsByTagName("link").at(iDx).firstChild().nodeValue();
                                 link = Qt::escape(link);
                                 
                                 rx.indexIn(link);
                                 QString captured = rx.cap(2);
                                 comstring = captured.toUtf8().constData();

                                 QByteArray comIDHash = QCryptographicHash::hash(comstring.toUtf8(), QCryptographicHash::Md5).toHex();
                                 QString comIDHashHEX = QString::fromUtf8(comIDHash);

                                 if ( ok ) {
                                    QSqlQuery catqu;
                                    if( !catqu.exec( "INSERT IGNORE INTO appstore_main.`temp_appbrain` (`id`, `link`, `com`, `hash`) VALUES (NULL, '"+link+"', '"+comstring+"', '"+comIDHashHEX+"');" ) ){
                                        qDebug() << catqu.lastError();
                                    }
                                 }

                                 cout << "Com: " + comstring +"\n";
                             }
                      }
                  }
              }
              catProcess->close();

           }
        }
        inputFile.close();


        cout << "Step 1 ENDED - RSS Inserted\n";
        cout << "Step 2 Ended - Unique Table Made!\n";

        cout << "Step 3 Started - Parsing Unique Apps and Inserting into MYSQL\n";

        if (ok){
            QSqlQuery selectuniqueapps;
            selectuniqueapps.exec("SELECT com, hash FROM temp_appbrain WHERE hash NOT IN(SELECT app_hash FROM appstore_main.apps);");
            if ( !selectuniqueapps.isActive() )

                cout << "Query Error" + selectuniqueapps.lastError().text() << endl;

            else while (selectuniqueapps.next()) {

                QString appcomname = selectuniqueapps.value(0).toString();
                QString apphash = selectuniqueapps.value(1).toString();
                QString appURL = "http://www.appbrain.com/app/"+appcomname+"/" + appcomname;

                QString PopulateApps = "python /home/login/Desktop/parser/parser_remote.py " + appURL;
                QProcess *AppsProcess = new QProcess();
                AppsProcess->start(PopulateApps);

                if (AppsProcess->waitForStarted(1000) == false)
                    cout << "Error starting external program";

                if (AppsProcess->waitForFinished()){

                    cout << "FINISHED: " + appURL + "\n";

                    /*
                    QByteArray appResult = AppsProcess->readAllStandardOutput();
                    QString appResultstr(appResult.data());
                    appResultstr = appResultstr.trimmed();
                    cout << appResultstr + "\n";
                    */

                }

            }

        }
        cout << "Step 3 ENDED!\n";

        cout << "All Updates Done! Starting MYSQL Optimization\n";
        QSqlQuery truncateappcomid;
        truncateappcomid.exec("TRUNCATE temp_appbrain;");

        cout << "TRUNCATION Done! Process Finished!";



    }
    cout << "Program Ended \n";

    return 0;    
}
