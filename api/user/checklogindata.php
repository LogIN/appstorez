<?php
/*===============================*/
/*===  Include Global Config ====*/
/*===============================*/
require ('../../include/config.inc.php');
require (ROOT.'include/databse.inc.php');
/*=======================*/
/*===  DB Connection ====*/
/*=======================*/
$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect(); 

/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
include ROOT.'include/functions.inc.php';

/*============================*/
/*=== Get Language From Url ==*/
/*============================*/
if($_GET['lng'] && $_GET['lng'] != ''){
    if(strlen(trim($_GET['lng'])) >= 4){
        $siteLanguage = CleanUrlData($_GET['lng']);
    }else{
        $siteLanguage = 'GB';
    }
}
/*============================*/
/*=== Get Country  From Url ==*/
/*============================*/
if($_GET['c'] && $_GET['c'] != ''){
    if(strlen(trim($_GET['c'])) >= 4){
        $country = CleanUrlData($_GET['c']);        
    }else{
        $country = 'United Kingdom';
    }        
}
/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}

/*===========================*/
/*=== Session Cookie Life ===*/
/*===========================*/
if((int)$_POST['rememberMe'] == 1){
    // SESSION COOKIE 2 WEEKS
    session_set_cookie_params(2*7*24*60*60);
}elseif((int)$_POST['rememberMe'] == 0){
    // SESSION COOKIE 6 hours
    session_set_cookie_params(6*60*60);
}else{
    // SESSION COOKIE 3 hours
    session_set_cookie_params(3*60*60);    
}
/*=====================*/
/*=== Session Start ===*/
/*=====================*/
session_name(md5(WEB_NAME));
session_start();

/*=================*/
/*=== If LogOff ===*/
/*=================*/
if(isset($_GET['LogOff'])){ 
    
    $_SESSION = array();
    session_destroy();
    header("Location: ".WEB_URL);
    exit;
}

/*================*/
/*=== If LogIN ===*/
/*================*/
if($_POST['submit'] == 'Login')
{
	// Checking whether the Login form has been submitted	
	$err = array();

        /*========================================*/
        /*=== IF Captcha is Submited Check it! ===*/
        /*========================================*/
	// Will hold our errors	
        if(str_rot13(urldecode($_POST['captcha'])) != '1'){
            if($_SESSION['captcha_number'] != str_rot13(urldecode($_POST['captcha']))){
                    $err[0] = CAPTCHA_ERROR;
                    print_r($err[0]);
            }
        }
        /*===============================*/
        /*=== Check if Uname and Pass ===*/
        /*===============================*/
	if(!$_POST['username'] || !$_POST['password']){
		$err[1] = FIELDS_ERROR;
                print_r($err[1]);
        }
        /*======================*/
        /*=== No Errors GoON ==*/
        /*======================*/
	if(!count($err)){         
            
                $_POST['username'] = CleanUrlData($_POST['username']);
                $_POST['password'] = str_rot13(urldecode($_POST['password']));
                
                $_POST['password'] = hash('sha512', SEC_TOKEN.$_POST['password']);                
		$_POST['rememberMe'] = (int)$_POST['rememberMe'];
		
                /*=======================*/
                /*=== Get DB Instaince ==*/
                /*=======================*/
                $db = Database::obtain();
                $sql = "SELECT user_id, user_type, user_username, user_verified FROM users WHERE user_username = '".$db->escape($_POST['username'])."' AND user_password = '".$db->escape($_POST['password'])."' LIMIT 1";
                $row = $db->query_first($sql);
                /*=================================*/
                /*=== If User Exsist and Data OK ==*/
                /*=================================*/
		if($db->affected_rows == 1 && $row['user_verified'] == 1)
		{
                        $_SESSION['id'] = $row['user_id'];
                        $_SESSION['type'] = $row['user_type'];
                        $_SESSION['username'] = $row['user_username']; 
                        $_SESSION['rememberMe'] = $_POST['rememberMe'];
                        
                        /*==============================*/
                        /*=== Set Session Cookie Time ==*/
                        /*==============================*/                        
                        if((int)$_POST['rememberMe'] == 1){
                            // COOKIE 2 WEEKS
                            $CookieTime = time()+1209600;
                        }elseif((int)$_POST['rememberMe'] == 0){
                            // COOKIE 6 hours
                            $CookieTime = time()+21600;
                        }else{
                            // COOKIE 3 hours
                            $CookieTime = time()+10800;    
                        }
                        /*=========================*/
                        /*=== Set Session Cookie ==*/
                        /*=========================*/
                        setcookie(session_name(),session_id(), $CookieTime);
			
                        echo("OK");
		}else{
                    $err[2] = U_P_INVALID;
                    print_r($err[2]);

                }
                /*===============*/
                /*=== DB Close ==*/
                /*===============*/
                $db->close();
        }
	
	if($err){
            $_SESSION['msg']['login-err'] = "Error";
            exit;
        }
        
/*================*/
/*=== Register ===*/
/*================*/
}elseif($_POST['submit'] == 'Register'){

	// Checking whether the Register form has been submitted	
	$err = array();
        
        /*===============================*/
        /*=== Check if Uname and Pass ===*/
        /*===============================*/
	if(!$_POST['username'] || !$_POST['password']){
		$err[1] = FIELDS_ERROR;
                print_r($err[1]);
        }
        
        /*=========================*/
        /*=== Check if Uname OK ===*/
        /*=========================*/
	if(strlen($_POST['username']) < 5 || strlen($_POST['username']) > 73)
	{
		$err[2] = USERNAME_LENGHT;
	}
	
	if(preg_match('/[^a-z0-9\-\_\.]+/i',$_POST['username']))
	{
		$err[3] = USERNAME_INV_CH;
	}
        
        /*=========================*/
        /*=== Check if EMAIL OK ===*/
        /*=========================*/
	if(!checkEmail($_POST['email']))
	{
		$err[4] = EMAIL_INVALID;
	}   
        
        /*======================*/
        /*=== No Errors GoON ===*/
        /*======================*/
	if(!count($err))
	{
                /*======================*/
                /*=== Get Posted Data ==*/
                /*======================*/
            
		// INSERT INTO USERS TABLE
                $data_users = array();
                $data_users['user_type'] = CleanUrlData($_POST['type']);
                $data_users['user_username'] = CleanUrlData($_POST['username']);
                $data_users['user_password'] = hash('sha512', SEC_TOKEN.$_POST['password']);
		$data_users['user_email'] = CleanUrlData($_POST['email']);
                $data_users['user_verified'] = '0';
                $data_users['user_salt'] = md5($_POST['email'].SEC_TOKEN);

                $data_users['user_lastIP'] = "INET_ATON(".$_SERVER['REMOTE_ADDR'].")";  
                $data_users['user_created'] = 'NOW()';
                $data_users['user_accessed'] = 'NOW()';
                /*=========================================*/
                /*=== Get User Inseretd ID, and DoInsert ==*/
                /*=========================================*/
                $data_users_id = $db->insert(TABLE_USERS, $data_users);
                
                // INSERT INTO USERS_PROFILE TABLE
                $data_users_profile = array();
                $data_users_profile['user_prof_guid'] = $data_users_id;
                $data_users_profile['user_prof_name'] = CleanUrlData($_POST['name']);
                $data_users_profile['user_prof_surname'] = CleanUrlData($_POST['surname']);
                
                $data_users_profile['user_prof_age'] = CleanUrlData($_POST['age']);
                
                if(CleanUrlData($_POST['gender']) == 'Female'){
                    $data_users_profile['user_prof_gender'] = '2';
                }elseif(CleanUrlData($_POST['gender']) == 'Male'){             
                    $data_users_profile['user_prof_gender'] = '1';
                }else{
                    $data_users_profile['user_prof_gender'] = '1';
                }             
                
                $data_users_profile['user_prof_country'] = getCountryCode(CleanUrlData($_POST['country']), 1);
                
                $data_users_profile['user_prof_city'] = CleanUrlData($_POST['city']);
                $data_users_profile['user_prof_postal_code'] = CleanUrlData($_POST['postal']);
                
                /*====================================*/
                /*=== Get Developer Type Field Data ==*/
                /*====================================*/
                $data_users_profile['user_prof_address'] = isset($_POST['address']) ? $_POST['address'] : '0';  // DEVELOPER 
                
                $data_users_profile['user_prof_phone'] = isset($_POST['phone']) ? $_POST['phone'] : '0';  // DEVELOPER
                $data_users_profile['user_prof_phone'] = strStripWhitespace($data_users_profile['user_prof_phone']);
                
                $data_users_profile['user_prof_mobile'] = isset($_POST['mobile']) ? $_POST['mobile'] : '0';  // DEVELOPER
                $data_users_profile['user_prof_mobile'] = strStripWhitespace($data_users_profile['user_prof_mobile']);
                
                $data_users_profile['user_prof_web'] = isset($_POST['web']) ? $_POST['web'] : '0';  // DEVELOPER
               
                /* DATA NOT SUBMITED */
                $data_users_profile['user_prof_logo'] = isset($_POST['logo']) ? $_POST['logo'] : '0';  // DEVELOPER
                $data_users_profile['user_prof_devices'] = isset($_POST['devices']) ? $_POST['devices'] : '0';  // DEVELOPER
                $data_users_profile['user_prof_payment_type'] = isset($_POST['payment_type']) ? $_POST['payment_type'] : '0';  // DEVELOPER
                $data_users_profile['user_prof_earned'] = '0';  // DEVELOPER

                /*==========================*/
                /*=== Insert UsersProfile ==*/
                /*==========================*/
                $db->insert(TABLE_USERS_PROFILE, $data_users_profile);
                
		$db->close();
                /*=================================*/
                /*=== If User Exsist and Data OK ==*/
                /*=================================*/
		if($db->affected_rows  == 1 )
		{
                    /*===========================================*/
                    /*===   Generate Welcome Email HTML        ==*/
                    /*=== CURL() in functions.inc.php          ==*/
                    /*===========================================*/
                    
                    
                    /*=========================*/
                    /*=== Send Welcome EMAIL ==*/
                    /*=========================*/
                    $MailTo  = $data_users['user_email'];
                    $MailToSubject = WEL_EMAIL_SUBJ;
                    
                    $MailHtml = curl(WEB_URL.'users/email/welcome/'.$data_users['user_salt']);
                    
                    // To send HTML mail, the Content-type header must be set
                    $MailHeaders  = 'MIME-Version: 1.0' . "\r\n";
                    $MailHeaders .= "Content-Type: text/html; charset = \"UTF-8\";\n";
                    $MailHeaders .= "Content-Transfer-Encoding: 8bit\n";
                    $MailHeaders .= "Sensitivity: Company-Confidential\n";
                    // Additional headers
                    $MailHeaders .= 'To: '.$data_users_profile['user_prof_name'].' '.$data_users_profile['user_prof_surname'].' <'.$data_users['user_email'].'>' . "\r\n";
                    $MailHeaders .= 'From: '.WEB_NAME.' <'.EMAIL_WELLCOME.'>' . "\r\n";              
                    
                    mail($MailTo, $MailToSubject, $MailHtml, $MailHeaders);
                    
                    
                    echo("OK");
		}else{ 
                    $err[5] = UNAME_TAKEN;
                }
	}

	if(count($err))
	{
		print_r($err);
                exit(0);
	}
}else{
    echo("NOT_OK");
    exit(0);
}
?>
