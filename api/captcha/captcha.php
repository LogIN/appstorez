<?php
require ('../../include/config.inc.php');
require (ROOT.'include/databse.inc.php');
/*=======================*/
/*=== Forece No-Cache ===*/
/*=======================*/
header("Pragma: no-cache");
header("cache-Control: no-cache, must-revalidate"); // HTTP/1.1
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past

/*=====================*/
/*=== Session Start ===*/
/*=====================*/
session_name(md5(WEB_NAME));
session_start();

/*=================================*/
/*=== Destroy  "captcha_number" ===*/
/*=================================*/
unset($GLOBALS[_SESSION]['captcha_number']);


/*=================================*/
/*=== Count Captcha Generations ===*/
/*=================================*/
if(isSet($_SESSION['brute_detect'])){
    $_SESSION['brute_detect'] += 1;
}else{
    $_SESSION['brute_detect'] = 0;
}

/*=================================*/
/*=== Generate 2 random words   ===*/
/*=================================*/
$word_1 = '';
$word_2 = '';

for ($i = 0; $i < 4; $i++) 
{
	$word_1 .= chr(rand(97, 122));
}
for ($i = 0; $i < 4; $i++) 
{
	$word_2 .= chr(rand(97, 122));
}
/*======================================*/
/*=== Set "captcha_number" for check ===*/
/*======================================*/
$_SESSION['captcha_number'] = $word_1.' '.$word_2;

/*==========================*/
/*=== Generate PNG Image ===*/
/*==========================*/

$dir = ROOT.'template/fonts/';
$image = imagecreatetruecolor(165, 50);
// font style
$font = "recaptchaFont.ttf"; 
// color
$color = imagecolorallocate($image, 0, 0, 0);
// background color white
$white = imagecolorallocate($image, 255, 255, 255); 
imagefilledrectangle($image, 0,0, 709, 99, $white);

/*======================================*/
/*=== Set Text From "captcha_number" ===*/
/*======================================*/
imagettftext ($image, 22, 0, 5, 30, $color, $dir.$font, $_SESSION['captcha_number']);
header("Content-type: image/png");
imagepng($image);  
?>