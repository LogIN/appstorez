<?php
$user[ip] = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
$user[ref] = isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '';
$user[browser] = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';    
$user[hostaddress] = gethostbyaddr($user[ip]);
$user[country] = trim(str_replace("\"", "", curl("http://api.maskify.me/include/ip2country/api.php?type=NAME&ip=".$user[ip]))); 
$user[countryCode] = getCountryCode($user[country]);

//$siteLanguage = array();
$siteLanguage = calculateDesiredLNG($user[$countryCode], $user[$countryCode]);

$user[msg][reg-err] = LogRequestsFreq($user);
?>
