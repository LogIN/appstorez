<?php
/*=========================================*/
/*=== Returns UserData from UNIQUE salt ===*/
/*=========================================*/
function userObject($userSalt){
    $userSalt = trim($userSalt);
    
    /*======================*/
    /*=== Is it Real MD5 ===*/
    /*======================*/
    if(isValidMd5($userSalt) == 1){        
      
        $db = Database::obtain();
        /*======================*/
        /*=== Get User Data  ===*/
        /*======================*/  
        $sql = "SELECT users.user_id, users.user_type, users.user_username, users.user_password, users.user_email, users.user_verified, users.user_salt, users.user_lastIP, users.user_created, users.user_accessed, users_profile.user_prof_id, users_profile.user_prof_name, users_profile.user_prof_surname, users_profile.user_prof_age, users_profile.user_prof_gender, users_profile.user_prof_city, users_profile.user_prof_postal_code, users_profile.user_prof_address, users_profile.user_prof_phone, users_profile.user_prof_mobile, users_profile.user_prof_web, users_profile.user_prof_logo, users_profile.user_prof_devices, users_profile.user_prof_payment_type, users_profile.user_prof_earned, list_countries.name AS country_name FROM ".TABLE_USERS." LEFT JOIN ".TABLE_USERS_PROFILE." ON users.user_id=users_profile.user_prof_guid LEFT JOIN ".TABLE_LIST_COUNTRY." ON users_profile.user_prof_country=list_countries.id WHERE users.user_salt = '".$db->escape($userSalt)."' LIMIT 1";

        $row = $db->query_first($sql);
        if($db->affected_rows  == 1 ){
            return $row;
        }else{
            return false;
        }
        
        $db->close();

    }else{
        return false;
    }
}
/*===============================================================================================*/
/*=== Select MAX(id) mysq                                                                    ====*/
/*=== USAGE:                                                                                 ====*/
/*=== selectMAX(FIELD_NAME, TABLE_NAME, "WHERE TABLE_NAME = 0", FUNCTION(MAX, COUNT, SUM ...)====*/
/*===============================================================================================*/
function selectMAX($field = 'app_id', $table = 'apps', $where = '', $type = 'MAX'){
    $db = Database::obtain();
    
    $sql = "SELECT ".$db->escape($type)."(".$db->escape($field).") AS ".$db->escape($field)." FROM `".$db->escape($table)."` ".$where." LIMIT 1;";    
    $row = $db->query_first($sql);
    
    if($db->affected_rows  == 1 ){
        return $row;
    }else{
        return false;
    }

    $db->close();
}
/*======================*/
/*=== Is it Real MD5 ===*/
/*======================*/
function isValidMd5($md5)
{
    return !empty($md5) && preg_match('/^[a-f0-9]{32}$/', $md5);
}
/*===========================================*/
/*=== Clean/Strip(HTML) Recieved URL DATA ===*/
/*===========================================*/
function CleanUrlData($data){
    $data = urldecode(trim($data));
    return strip_tags($data);
}
        
/*================================================================*/
/*================== Encode and return image as data  ============*/
/*================== for direct inclusion in HTML     ============*/
/*=== src="<?php echo data_uri('elephant.png', 'image/png');?>"===*/
/*================================================================*/
function data_uri($file, $mime) {
    $contents = file_get_contents($file);
    $base64 = base64_encode($contents);
    return "data:$mime;base64,$base64";
}
/*=================================*/
/*=== Minify PHP => HTML Output ===*/
/*=================================*/
function replace_whitespace($html) {
    
    $html = preg_replace(array('/\s{2,}/', '/[\r\t\n]/'), ' ', $html);
    return $html;
}
/*==========================================*/
/*=== Calculates random string Lenght: 5 ===*/
/*==========================================*/
function genRandomString() { 
    $length = 5;
    $characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
    $string = null;

    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters)-1)];
    }
    return $string;
}

/*==============================================================*/
/*=== It gives the host (including the subdomain if exists). ===*/
/*=== getHost("example.com"); // Gives example.com           ===*/
/*=== getHost("http://example.com"); // Gives example.com    ===*/
/*=== getHost("www.example.com"); // Gives www.example.com   ===*/
/*=== getHost("http://example.com/xyz"); // Gives example.com===*/
/*==============================================================*/
function getHost($url) { 
   $parseUrl = parse_url(trim($url)); 
   return trim($parseUrl['host'] ? $parseUrl['host'] : array_shift(explode('/', $parseUrl['path'], 2))); 
}
/*===========================================*/
/*=== CURL Function (Temp.Cookie Support) ===*/
/*===========================================*/
function curl($url){
    
    $rand = md5($url);
    $cookie = ROOT."temp/cache/cookies/cookie_".$rand.".txt"; 
    $cookieFile = fopen($cookie, 'a') or die("can't open file");
    fclose($cookieFile);
 
    $refurl = getHost($url);
        
    $ch = curl_init();  
    curl_setopt ($ch, CURLOPT_URL, $url); 
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, FALSE); 
    curl_setopt ($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)"); 
    curl_setopt ($ch, CURLOPT_TIMEOUT, 30); 
    curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1); 
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt ($ch, CURLOPT_COOKIEJAR, $cookie);
    curl_setopt ($ch,CURLOPT_COOKIEFILE, $cookie);  
    curl_setopt ($ch, CURLOPT_REFERER, "http://search.yahoo.com/search;_ylt=AnN412_WuLKv7fu_myJdnEubvZx4?p=".$refurl."&toggle=1&cop=mss&ei=UTF-8&fr=yfp-t-701"); 

    $result = curl_exec ($ch); 
    curl_close($ch);
    unlink($cookie);
    
    return($result); 
}
/*=========================*/
/*=== Removes " " space ===*/
/*=========================*/
function strStripWhitespace($str) {
    return str_replace(' ', '', $str);
}
/*=========================================*/
/*=== Returns first X chars from string ===*/
/*=========================================*/
function getPreview($text, $minimumLength=60){
     $text = preg_replace( '/\s+/', ' ', $text );
     $return = utf8_encode(substr(utf8_decode($text),0,$minimumLength));
     return $return;
}

/*=======================*/
/*=== Returns SEO URL ===*/
/*=======================*/
function SEO($str, $replace=array(), $delimiter='-'){
    setlocale(LC_ALL, 'en_US.UTF8');
    if( !empty($replace) ) {
        $str = str_replace((array)$replace, ' ', $str);
    }else{
        $trArr = array('ç','Ç','ı','İ','ş','Ş','ğ','Ğ','ö','Ö','ü', 'Ü');
        $toArr = array('c','c','i','i','s','s','g','g','o','o','u', 'u');
        $str = str_replace($trArr,$toArr,$str);
    }
    $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
    $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
    $clean = mb_strtolower(trim($clean, '-'),'UTF-8');
    $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
    return $clean;
}
/*=======================*/
/*=== Returns SEO TAGS ===*/
/*=======================*/
function SEO_TAG($str, $replace=array(), $delimiter='-'){   
    if(!is_numeric($str)){
        
        setlocale(LC_ALL, 'en_US.UTF8');
        if( !empty($replace) ) {
            $str = str_replace((array)$replace, ' ', $str);
        }else{
            $trArr = array('-','*');
            $toArr = array(' ','');
            $str = str_replace($trArr,$toArr,$str);
        }           
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
        $clean = mb_strtolower(trim($clean, $delimiter),'UTF-8');
        return preg_replace('~\b(\w+)\b~e', 'ucfirst("\\1")', $clean);
    }else{
        return FALSE;
    }
}

/*=================================================*/
/*=== Return Country Code(ID) from Country Name ===*/
/*=================================================*/
function getCountryCode($country, $type = '0'){
    
    /*========================================*/
    /*=== If Type 0, return 2 letter code  ===*/
    /*=== If Type 1, return numeric ID     ===*/
    /*========================================*/
    
    // get the already existing instance of the $db object
    $db = Database::obtain();
    
    if($type == '0'){
    
        $sql = "SELECT `iso1_code`  FROM `".TABLE_LIST_COUNTRY."` WHERE `name` = '".$db->escape($country)."'";
        $row = $db->query_first($sql);

        // if code exists
        if($db->affected_rows > 0 || !empty($row['iso1_code'])){
            return $row['iso1_code'];
        }else{
            return 'GB';
        }
    }elseif($type == '1'){
    
        $sql = "SELECT `id`  FROM `".TABLE_LIST_COUNTRY."` WHERE `name` = '".$db->escape($country)."'";
        $row = $db->query_first($sql);

        // if code exists
        if($db->affected_rows > 0 || !empty($row['id'])){
            return $row['id'];
        }else{
            return '225';
        }
    }
    $db->close();    
}
/*===============================================*/
/*=== Returns html DropDown with country list ===*/
/*===============================================*/
function countryDropDown($default = 'United Kingdom'){
    $db = Database::obtain();    
    
    $countries = '';
    $sql = "SELECT * FROM `".TABLE_LIST_COUNTRY."` LIMIT 239";
    $rows = $db->query($sql);
    
    if($db->affected_rows > 0){
        while ($record = $db->fetch($rows)) {
            if($record[name] == $default){
                $countries .= "<option selected=\"selected\">".$record[name]."</option>";
            }else{
                $countries .= "<option>".$record[name]."</option>";
            }                
        }
    }else{
        $countries = "ERROR";
    }
    return $countries;
    $db->close(); 
}

/*=====================================*/
/*=== What Language to use on Web?? ===*/
/*=====================================*/
function calculateDesiredLNG($countryCode, $UserCountryCode){
    
    if($UserCountryCode != ''){
        return $UserCountryCode;
    }elseif($UserCountryCode == ''){
        return $countryCode;
    }elseif($countryCode == '' && $UserCountryCode == ''){
        return 'GB';
    }elseif($countryCode != '' && $UserCountryCode != ''){
        return $UserCountryCode;
    }else{
        return 'GB';
    }
    
}
/*=======================*/
/*=== GET FREQ LOGGER ===*/
/*=======================*/
function LogRequestsFreq($user){
    return '';
}
/*=======================*/
/*=== Vaildate Email  ===*/
/*=======================*/
function checkEmail($str){
    return preg_match("/^[\.A-z0-9_\-\+]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{1,4}$/", $str);
}
/*=========================================*/
/*=== Get Users From database to Array  ===*/
/*=========================================*/
function BulkUsers($number = '10', $type = 'all', $orderBy = 'users.user_id DESC'){
     
    $db = Database::obtain();
    /*======================*/
    /*=== Get User Data  ===*/
    /*======================*/
    if($type == 'all'){
        $sql = "SELECT users.user_id, users.user_type, users.user_username, users.user_password, users.user_email, users.user_verified, users.user_salt, users.user_lastIP, users.user_created, users.user_accessed, users_profile.user_prof_id, users_profile.user_prof_name, users_profile.user_prof_surname, users_profile.user_prof_age, users_profile.user_prof_gender, users_profile.user_prof_city, users_profile.user_prof_postal_code, users_profile.user_prof_address, users_profile.user_prof_phone, users_profile.user_prof_mobile, users_profile.user_prof_web, users_profile.user_prof_logo, users_profile.user_prof_devices, users_profile.user_prof_payment_type, users_profile.user_prof_earned, list_countries.name AS country_name FROM users LEFT JOIN users_profile ON users.user_id=users_profile.user_prof_guid LEFT JOIN list_countries ON users_profile.user_prof_country=list_countries.id ORDER BY ".$orderBy." LIMIT ".$db->escape($number);
    }else{
        $sql = "SELECT users.user_id, users.user_type, users.user_username, users.user_password, users.user_email, users.user_verified, users.user_salt, users.user_lastIP, users.user_created, users.user_accessed, users_profile.user_prof_id, users_profile.user_prof_name, users_profile.user_prof_surname, users_profile.user_prof_age, users_profile.user_prof_gender, users_profile.user_prof_city, users_profile.user_prof_postal_code, users_profile.user_prof_address, users_profile.user_prof_phone, users_profile.user_prof_mobile, users_profile.user_prof_web, users_profile.user_prof_logo, users_profile.user_prof_devices, users_profile.user_prof_payment_type, users_profile.user_prof_earned, list_countries.name AS country_name FROM users LEFT JOIN users_profile ON users.user_id=users_profile.user_prof_guid LEFT JOIN list_countries ON users_profile.user_prof_country=list_countries.id WHERE users.user_type = ".$db->escape($type)." ORDER BY ".$orderBy." LIMIT ".$db->escape($number);
    }    
    $rows = $db->query($sql);
    
    if($db->affected_rows){
        $i = 0;
        $users = array();
        while ($record = $db->fetch($rows)) {
            
            $users[$i] = $record; 
            $i++;
        }
        return $users;
        $db->close(); 
    }else{
        return false;
    }   
}
/*====================================*/
/*=== Main Search/Browse Function  ===*/
/*====================================*/

function DoSearch($query = "", $page = '1', $limit = '20', $salt = '8809bef6b5c7ffcf41be279a127587f3', $printType = 1, $category = 'NONE', $rating_avg = 'NONE', $price = 'NONE', $app2SD = 'NONE', $dev_id = 'NONE', $callback = 'PHPSearch', $sortby = 'app_added', $sortorder = 'DESC'){
    $query = urlencode($query);
    // PRIMITIVE SEARCH URL CONSTRUCTOR
    $searchurl = '';
    $searchurl .= WEB_URL.'api/search/search.api.php';
    $searchurl .= '?salt='.$salt;
    $searchurl .= '&q='.$query;
    $searchurl .= '&page='.$page;
    $searchurl .= '&callback='.$callback;
    $searchurl .= '&limit='.$limit;
    $searchurl .= '&sortby='.$sortby;
    $searchurl .= '&sortorder='.$sortorder;
    $searchurl .= '&dev_id='.$dev_id;
    $searchurl .= '&app2SD='.$app2SD;
    $searchurl .= '&price='.$price;
    $searchurl .= '&category='.$category;
    $searchurl .= '&rating_avg='.$rating_avg; 
    #print_r($searchurl);
    #exit();
    $result = curl($searchurl);

    if($printType == 1){ // Returns Json
        return $result;
    }elseif($printType == 2){ // Returns Browsing HTML
        $html = '';
        return $html;
    }
}
/*=========================================*/
/*=== Format Search Result JSON to HTML ===*/
/*=========================================*/
function SearchDetails($json){
    $jsonobj = json_decode($json, true);
    // If search error or something give no results
    if($jsonobj[LastError] != ''){
        return(false);
    }else{
        $html = '';
        // Foreach app Designe DIV for html display
        foreach ($jsonobj[ids] as $app) {
            $html .= htmlAppBox($app);    
        }    
        return($html);
    }
}
/*==============================*/
/*=== Generate HTML App BOX  ===*/
/*==============================*/
function htmlAppBox($app){
    // Empty Some html Varivales
    $html = '';
    $smallinfo = '';
    $pricehtmlclass = '';
    
    // Resize icon to 90px if needed
    $app[attrs][app_icon] = str_replace('=s48', '=s90', $app[attrs][app_icon]);
    
    // Round price to two decimals
    $app[attrs][app_price] = round($app[attrs][app_price], 2);
    // Format size to nicely string
    $app[attrs][app_size] = formatBytes($app[attrs][app_size]);
    
    // Calculate Unknow developer username
    ## FIX:
    $app[attrs][user_username] = substr($app[attrs][user_username], 0, strpos($app[attrs][user_username], '@'));

    
    // Define html class of price if paid or if free
    if($app[attrs][app_price] == 0){ 
        $app[attrs][app_price] = APP_FREE; 
        $pricehtmlclass = 'free';         
    }elseif($app[attrs][app_price] > 0){ 
        $app[attrs][app_price] .= " $"; 
        $pricehtmlclass = 'paid';            
    }else{
        $app[attrs][app_price] = "NONE"; 
        $pricehtmlclass = 'free';  
    }
    // Add size and date to smallinfo htm field
    if($app[attrs][app_size] != 0){
        $smallinfo .= APP_SIZE.': '.formatBytes($app[attrs][app_size]);
    }elseif($app[attrs][app_added] != ''){
        $smallinfo .= APP_ADDED.': '.date("j.n.y", $app[attrs][app_added]);
    }else{
        $smallinfo = 'NONE';
    }
    // App DIV Start
    $html .= '<div id="'.$app[id].'" class="app">';
    $html .= '<img class="app_icon"src="'.trim($app[attrs][app_icon]).'" alt="'.$app[attrs][app_name].'" width="96" height="96" />';
    $html .= '<h1>'.$app[attrs][app_name].'</h1>';
    $html .= '<h2>'.BY.' '.$app[attrs][user_username].'</h2>';
    // Get only first 80 chars from description for preview
    if($app[attrs][app_description] != 'NONE'){
        $html .= '<div class="desc">'.getPreview($app[attrs][app_description], 80).'</div>';
    }
    $html .= '<div class="badges"></div>';      
    $html .= '<div class="rating"><div class="classification"><div class="cover"></div><div class="progress" style="width: '.percent($app[attrs][app_rating_avg]).'%;"></div></div></div>';
    // Is it app price defined or not (free|paid)
    if($app[attrs][app_price] != 'NONE'){
        $html .= '<span class="price_'.$pricehtmlclass.'">'.$app[attrs][app_price].'</span>';
    }
    // If some info exsist put it in
    if($smallinfo != 'NONE'){
        $html .= '<div class="smallinfo">'.$smallinfo.'</div>';
    }
    $html .= '</div>';
    // App DIV End
    
    return($html);
}
/*================================*/
/*=== Compile Pagination HTML  ===*/
/*================================*/
function pagination($page, $user, $search){
    
    $search = json_decode($search, true);
    
    $total = $search[total_found];
    $adjacents = "2"; 
    $page = $page[number];
    $per_page = $search[total]; 
    $url = $page[url_no_number];
    
    $page = ($page == 0 ? 1 : $page); 
    
    $start = ($page - 1) * $per_page;
    $prev = $page - 1;	
    
    $next = $page + 1;
    $lastpage = ceil($total/$per_page);
    $lpm1 = $lastpage - 1;

    $pagination = "";
    if($lastpage > 1)
    {	
            $pagination .= "<div id='paginate'><ul class='pagination'>";
                $pagination .= "<li class='details'>".PAGE." ".$page." ".OF." ".$lastpage."</li>";
            if ($lastpage < 7 + ($adjacents * 2))
            {	
                    for ($counter = 1; $counter <= $lastpage; $counter++)
                    {
                            if ($counter == $page)
                                    $pagination.= "<li><a class='current'>".$counter."</a></li>";
                            else
                                    $pagination.= "<li><a href='{$url}page=".$counter."'>".$counter."</a></li>";					
                    }
            }
            elseif($lastpage > 5 + ($adjacents * 2))
            {
                    if($page < 1 + ($adjacents * 2))		
                    {
                            for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++)
                            {
                                    if ($counter == $page)
                                            $pagination.= "<li><a class='current'>".$counter."</a></li>";
                                    else
                                            $pagination.= "<li><a href='{$url}page=".$counter."'>".$counter."</a></li>";					
                            }
                            $pagination.= "<li class='dot'>...</li>";
                            $pagination.= "<li><a href='{$url}page=".$lpm1."'>".$lpm1."</a></li>";
                            $pagination.= "<li><a href='{$url}page=".$lastpage."'>".$lastpage."</a></li>";		
                    }
                    elseif($lastpage - ($adjacents * 2) > $page && $page > ($adjacents * 2))
                    {
                            $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                            $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                            $pagination.= "<li class='dot'>...</li>";
                            for ($counter = $page - $adjacents; $counter <= $page + $adjacents; $counter++)
                            {
                                    if ($counter == $page)
                                            $pagination.= "<li><a class='current'>".$counter."</a></li>";
                                    else
                                            $pagination.= "<li><a href='{$url}page=".$counter."'>".$counter."</a></li>";					
                            }
                            $pagination.= "<li class='dot'>..</li>";
                            $pagination.= "<li><a href='{$url}page=".$lpm1."'>".$lpm1."</a></li>";
                            $pagination.= "<li><a href='{$url}page=".$lastpage."'>".$lastpage."</a></li>";		
                    }
                    else
                    {
                            $pagination.= "<li><a href='{$url}page=1'>1</a></li>";
                            $pagination.= "<li><a href='{$url}page=2'>2</a></li>";
                            $pagination.= "<li class='dot'>..</li>";
                            for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++)
                            {
                                    if ($counter == $page)
                                            $pagination.= "<li><a class='current'>".$counter."</a></li>";
                                    else
                                            $pagination.= "<li><a href='{$url}page=".$counter."'>".$counter."</a></li>";					
                            }
                    }
            }

            if ($page < $counter - 1){ 
                    $pagination.= "<li><a href='{$url}page=".$next."'>".PAG_NEXT."</a></li>";
            $pagination.= "<li><a href='{$url}page=".$lastpage."'>".PAG_LAST."</a></li>";
            }else{
                    $pagination.= "<li><a class='current'>".PAG_NEXT."</a></li>";
            $pagination.= "<li><a class='current'>".PAG_LAST."</a></li>";
        }
            $pagination.= "</ul></div>";		
    }


    return $pagination;
}
/*===========================*/
/*=== Kb to MB or GB ...  ===*/
/*===========================*/
function formatBytes($size, $precision = 2)
{
    // Kb to bytes
    $size = $size * 1024;
    $base = log($size) / log(1024);
    $suffixes = array('', 'k', 'M', 'G', 'T');   

    return round(pow(1024, $base - floor($base)), $precision) . $suffixes[floor($base)];
}

/*========================================*/
/*=== Get percentage of desired range  ===*/
/*========================================*/
function percent($num_amount, $num_total = 5) {
    $count1 = $num_amount / $num_total;
    $count2 = $count1 * 100;
    $count = number_format($count2, 0);
    return $count;
}

/*========================================*/
/*=== Simple Json CallBack Validation  ===*/
/*========================================*/

function is_valid_callback($subject)
{
    $identifier_syntax
      = '/^[$_\p{L}][$_\p{L}\p{Mn}\p{Mc}\p{Nd}\p{Pc}\x{200C}\x{200D}]*+$/u';

    $reserved_words = array('break', 'do', 'instanceof', 'typeof', 'case',
      'else', 'new', 'var', 'catch', 'finally', 'return', 'void', 'continue',
      'for', 'switch', 'while', 'debugger', 'function', 'this', 'with',
      'default', 'if', 'throw', 'delete', 'in', 'try', 'class', 'enum',
      'extends', 'super', 'const', 'export', 'import', 'implements', 'let',
      'private', 'public', 'yield', 'interface', 'package', 'protected',
      'static', 'null', 'true', 'false');

    return preg_match($identifier_syntax, $subject)
        && ! in_array(mb_strtolower($subject, 'UTF-8'), $reserved_words);
}

/**
 * Removes the preceeding or proceeding portion of a string
 * relative to the last occurrence of the specified character.
 * The character selected may be retained or discarded. 
 * @param string $character the character to search for.
 * @param string $string the string to search through.
 * @param string $side determines whether text to the left or the right of the character is returned.
 * Options are: left, or right.
 * @param bool $keep_character determines whether or not to keep the character.
 * Options are: true, or false.
 * @return string 
 */
function cut_string_using_last($character, $string, $side, $keep_character=true) {
    $offset = ($keep_character ? 1 : 0);
    $whole_length = strlen($string);
    $right_length = (strlen(strrchr($string, $character)) - 1);
    $left_length = ($whole_length - $right_length - 1);
    switch($side) {
        case 'left':
            $piece = substr($string, 0, ($left_length + $offset));
            break;
        case 'right':
            $start = (0 - ($right_length + $offset));
            $piece = substr($string, $start);
            break;
        default:
            $piece = false;
            break;
    }
    return($piece);
} 
?>
