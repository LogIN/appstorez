<?php
//define('', '');
/*====================*/
/*======= META =======*/
/*====================*/
define('META_TITLE', 'Android Apps Market: Download Free & Paid Android Applications');
define('META_KEYWORDS', 'android,apps,tablet,applications,games,android market alternative,google phone,mobile software,catalog,appstore,downloads,free,paid,best android,market,marketplace');
define('META_DESC', 'AppStoreZ Android Application Market. Download Free &amp; Premium Android Apps.');
/*====================*/
/*======= PAGE =======*/
/*====================*/
define('PAGE', 'Page');
define('PAGE_SEARCH_VALUE', 'Search Apps ...');
define('PAGE_SEARCH_BUTTON', 'Search');
define('PAGE_WELLCOME', 'Wellcome');
define('PAGE_LOGIN', 'Log in');
define('PAGE_REGISTER', 'Register');
define('PAGE_LOGOFF', 'Log Out');
define('PAGE_BETA', 'Early Beta Version');

define('SITE_OFFLINE', 'is currently offline!');

/*==========================*/
/*== Search Nothing Found ==*/
/*==========================*/
define('PAGE_SEARCH_NOTFOUND_1', 'We couldn\'t find anything for your search - ');
define('PAGE_SEARCH_NOTFOUND_2', 'Search tips:');
define('PAGE_SEARCH_NOTFOUND_3', 'Make sure all words are spelled correctly.');
define('PAGE_SEARCH_NOTFOUND_4', 'Try different, more general, or fewer keywords.');
define('PAGE_SEARCH_NOTFOUND_5', 'Try rephrasing keywords or using synonyms.');
define('PAGE_SEARCH_NOTFOUND_6', 'Make your queries as concise as possible.');
define('PAGE_SEARCH_NOTFOUND_7', 'Note that the quality filter may be hiding results. Try a less restrictive level.');
define('PAGE_SEARCH_NOTFOUND_8', 'Explore  '.WEB_NAME.' using the Market categories.');
define('PAGE_SEARCH_NOTFOUND_9', 'Try asking a question on '.WEB_NAME.' forums.');

/*========================*/
/*===== Pagination =======*/
/*========================*/    
define('PAG_NEXT', 'Next');
define('PAG_LAST', 'Last');

/*====================*/
/*===== SLIDER =======*/
/*====================*/
define('SLIDER_NEXT', 'Next App');
define('SLIDER_PREV', 'Previous');
/*====================*/
/*======= MENU =======*/
/*====================*/
$category = array();
define('APPS', 'apps');
$category[SEO(APPS)] = '';
define('GAMES', 'Games');
$category[SEO(GAMES)] = '21,7,17,2,31,11,21';

define('GAMES_ARCADE', 'Arcade & Action');
$category[SEO(GAMES_ARCADE)] = '21';
define('GAMES_BRAIN', 'Brain & Puzzle');
$category[SEO(GAMES_BRAIN)] = '7';
define('GAMES_CARDS', 'Cards & Casino');
$category[SEO(GAMES_CARDS)] = '17';
define('GAMES_CASUAL', 'Casual');
$category[SEO(GAMES_CASUAL)] = '2';
define('GAMES_WALLPAPER', 'Live Wallpaper');
$category[SEO(GAMES_WALLPAPER)] = '2';
define('GAMES_RACING', 'Racing');
$category[SEO(GAMES_RACING)] = '31';
define('GAMES_SPORTS', 'Sports Games');
$category[SEO(GAMES_SPORTS)] = '11';
define('GAMES_WIDGETS', 'Widgets');
$category[SEO(GAMES_WIDGETS)] = '2';
define('GAMES_OTHER', 'Other');
$category[SEO(GAMES_OTHER)] = '21';

define('APP', 'Applications');
$category[SEO(APP)] = '4,19,8,15,10,1,23,28,20,27,14,25,22,29,5,6,13,14,30,3,24,12,9,16,18,16';

define('APP_BOOKS', 'Books & Reference');
$category[SEO(APP_BOOKS)] = '4';
define('APP_BUSINESS', 'Business');
$category[SEO(APP_BUSINESS)] = '19';
define('APP_COMICS', 'Comics');
$category[SEO(APP_COMICS)] = '8';
define('APP_COMMUNICATION', 'Communication');
$category[SEO(APP_COMMUNICATION)] = '15';
define('APP_EDUCATION', 'Education');
$category[SEO(APP_EDUCATION)] = '10';
define('APP_ENTERTAINMENT', 'Entertainment');
$category[SEO(APP_ENTERTAINMENT)] = '1';
define('APP_FINANCE', 'Finance');
$category[SEO(APP_FINANCE)] = '23';
define('APP_HELTH', 'Health & Fitness');
$category[SEO(APP_HELTH)] = '28';
define('APP_LIBRARIES', 'Libraries & Demo');
$category[SEO(APP_LIBRARIES)] = '20';
define('APP_LIFESTYLE', 'Lifestyle');
$category[SEO(APP_LIFESTYLE)] = '27';
define('APP_WALLPAPER', 'Live Wallpaper');
$category[SEO(APP_WALLPAPER)] = '14';
define('APP_MEDIA', 'Media & Video');
$category[SEO(APP_MEDIA)] = '25';
define('APP_MEDICAL', 'Medical');
$category[SEO(APP_MEDICAL)] = '22';
define('APP_MUSIC', 'Music & Audio');
$category[SEO(APP_MUSIC)] = '29';
define('APP_NEWS', 'News & Magazines');
$category[SEO(APP_NEWS)] = '5';
define('APP_PERSONAL', 'Personalization');
$category[SEO(APP_PERSONAL)] = '6';
define('APP_PHOTOGRAPHY', 'Photography');
$category[SEO(APP_PHOTOGRAPHY)] = '13';
define('APP_PRODUCTIVITY', 'Productivity');
$category[SEO(APP_PRODUCTIVITY)] = '14';
define('APP_SHOOPING', 'Shopping');
$category[SEO(APP_SHOOPING)] = '30';
define('APP_SOCIAL', 'Social');
$category[SEO(APP_SOCIAL)] = '3';
define('APP_SPORTS', 'Sports');
$category[SEO(APP_SPORTS)] = '24';
define('APP_TOOLS', 'Tools');
$category[SEO(APP_TOOLS)] = '12';
define('APP_TRANSPORT', 'Transportation');
$category[SEO(APP_TRANSPORT)] = '9';
define('APP_TRAVEL', 'Travel & Local');
$category[SEO(APP_TRAVEL)] = '16';
define('APP_WEATHER', 'Weather');
$category[SEO(APP_WEATHER)] = '18';
define('APP_WIDGETS', 'Widgets');
$category[SEO(APP_WIDGETS)] = '6';

define('CAT', 'Categories');
$category[SEO(CAT)] = '21,7,17,2,31,11,21,4,19,8,15,10,1,23,28,20,27,14,25,22,29,5,6,13,14,30,3,24,12,9,16,18,16';

define('CAT_T_P', 'Top Paid');
$category[SEO(CAT_T_P)] = 'CAT_T_P';
define('CAT_T_F', 'Top Free');
$category[SEO(CAT_T_F)] = 'CAT_T_F';
define('CAT_T_N_P', 'Top New Paid');
$category[SEO(CAT_T_N_P)] = 'CAT_T_N_P';
define('CAT_T_N_F', 'Top New Free');
$category[SEO(CAT_T_N_F)] = 'CAT_T_N_F';
define('CAT_M_V', 'Most Viewed');
$category[SEO(CAT_M_V)] = 'CAT_M_V';
define('CAT_M_C', 'Most Commented');
$category[SEO(CAT_M_C)] = 'CAT_M_C';
define('CAT_M_R', 'Most Rated');
$category[SEO(CAT_M_R)] = 'CAT_M_R';
define('CAT_M_D', 'Most Downloaded');
$category[SEO(CAT_M_D)] = 'CAT_M_D';

$common_sorted = array(
    'CAT_T_P' => 'CAT_T_P', 
    'CAT_T_F' => 'CAT_T_F',
    'CAT_T_N_P' => 'CAT_T_N_P',
    'CAT_T_N_F' => 'CAT_T_N_F',
    'CAT_M_V' => 'CAT_M_V',
    'CAT_M_C' => 'CAT_M_C',
    'CAT_M_R' => 'CAT_M_R',
    'CAT_M_D' => 'CAT_M_D');

define('APP_FREE', 'Free');
define('APP_PAID', 'Paid');
define('APP_SIZE', 'Size');
define('APP_ADDED', 'Added');       


define('MY_APPS', 'My Apps');
define('COMMUNITY', 'Community');
define('PARTNERS', 'Partners');
define('OUR_MARKET', 'Our Market');
define('BROWSE', 'Browse');
/*====================*/
/*== LOGIN/REGISTER ==*/
/*====================*/

define('USERNAME', 'Username');
define('PASSWORD', 'Password');

define('USERNAME_LENGHT', 'Your username must be between 5 and 72 characters!');
define('USERNAME_INV_CH', 'Your username contains invalid characters!');
define('EMAIL_INVALID', 'Your email is not valid!');
define('U_P_INVALID', 'Wrong username and/or password!');
define('FIELDS_ERROR', 'All fields must be filled in!');
define('CAPTCHA_ERROR', 'CAPTCHA_ERROR');
define('WEL_EMAIL_SENT', 'We sent you an welcome email with your credentials!');
define('UNAME_TAKEN', 'This username is already taken :/!');
define('WEL_EMAIL_SUBJ', 'Please verify the contact email address for your '.WEB_NAME.' ID.');

define('PERCENTAGE', 'Complete');

// Main Login FORM
define('LOGIN_H1_1', 'LOGIN IN YOUR ');
define('LOGIN_H1_2', ' ACCOUNT');
define('LOGIN_UNAME_MESS', 'Username you used to register.');
define('LOGIN_PASSW_MESS', 'Password used to register.');
define('LOGIN_REMEM_MESS', 'Keep me signed in!<br /><small>(Uncheck if on a shared computer)</small>');

/*====================*/
/*===== FOOTER =======*/
/*====================*/


/*====================*/
/*====== OTHER =======*/
/*====================*/
define('BY', 'by');
define('OF', 'of');
define('ANDROID', 'Android');
define('ABOUT', 'About');
define('ABOUT_US', 'About us');
?>
