<?php
require ('../include/config.inc.php');
require (ROOT.'include/databse.inc.php');
/*=======================*/
/*===  DB Connection ====*/ 
/*=======================*/
$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect(); 
/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/  
include ROOT.'include/functions.inc.php';
/*=================================================*/
/*=== For Load Balancing Session MYSql Handler  ===*/
/*=== include ROOT.'include/Zebra_Session.php'; ===*/
/*=== $session = new Zebra_Session();           ===*/
/*=================================================*/


/*=================================================*/
/*== visitor.info.inc.php calculate visitor info: =*/
/*== SiteLanguage, Visitor IP, Orgin Country etc  =*/
/*=================================================*/
// TODO : FIX Calculate Desired LNG ($countryCode $UserCountryCode)
// MAKE LANGUAGE IN DIFFERENT URL like: www.url.com/en/content.com // NO COOKIES OR SESSIONS
// http://googlewebmastercentral.blogspot.com/2010/03/working-with-multilingual-websites.html
include ROOT.'include/visitor.info.inc.php';

/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}
/*=====================*/
/*=== Session Start ===*/
/*=====================*/
session_name(md5(WEB_NAME));
session_start();
/*=========================================================*/
/*=== Check If User is LogedIn or Not (Destroy Session) ===*/
/*=========================================================*/
if($_SESSION['id'] && !isset($_COOKIE[session_name()]) && !$_SESSION['rememberMe']){
    $_SESSION = array();
    session_destroy();
    header("Location: ".WEB_URL."shell/pages/login.php");
    exit(0);
}elseif($_SESSION['id'] && isset($_COOKIE[session_name()]) && $_SESSION['rememberMe']){
    $user[isValid] = 1;  
    $user[username] = $_SESSION['username'];  
}elseif($_SESSION['id'] && isset($_COOKIE[session_name()])){
    $user[isValid] = 1;  
    $user[username] = $_SESSION['username'];  
}elseif(!$_SESSION['id']){
    header("Location: ".WEB_URL."shell/pages/login.php");
    exit(0);
}

/*============================*/
/*=== Populate User Object ===*/
/*============================*/
$data_users = array();
$data_users = userObject($_SESSION['salt']);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo WEB_NAME; ?> Administration</title>

<!-- CSS -->
<link href="template/style/css/transdmin.css" rel="stylesheet" type="text/css" media="screen" />
<!--[if IE 6]><link rel="stylesheet" type="text/css" media="screen" href="template/style/css/ie6.css" /><![endif]-->
<!--[if IE 7]><link rel="stylesheet" type="text/css" media="screen" href="template/style/css/ie7.css" /><![endif]-->

<!-- JavaScripts-->
<script type="text/javascript" src="template/style/js/jquery.js"></script>
<script type="text/javascript" src="template/style/js/jNice.js"></script>
</head>

<body>
    <?php 
    if(isset($_GET['page']) && file_exists(ROOT.'shell/pages/'.$_GET['page'].'.php')){
        include ROOT.'shell/pages/'.$_GET['page'].'.php'; 
    }else{
        include ROOT.'shell/pages/resources.php'; 
    }
    ?>
</body>
</html>


