<?php
// Set flag that this is a parent file
define( '_VALID_MOS', 1 );
define( '_VALID_EXT', 1 );

require_once( dirname(__FILE__).'/libraries/standalone.php');
ob_start();
include( dirname(__FILE__).'/admin.extplorer.php' );
$mainbody = ob_get_contents();
ob_end_clean();

extInitGzip();
header( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
header( 'Last-Modified: ' . gmdate( 'D, d M Y H:i:s' ) . ' GMT' );
header( 'Cache-Control: no-store, no-cache, must-revalidate' );
header( 'Cache-Control: post-check=0, pre-check=0', false );
header( 'Pragma: no-cache' );

echo '<?xml version="1.0" encoding="'. $GLOBALS["charset"].'">';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<?php echo $mainframe->getHead(); ?>
		<link rel="shortcut icon" href="<?php echo _EXT_URL ?>/eXtplorer.ico" />
 		<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $GLOBALS["charset"]; ?>" />
		<meta name="robots" content="noindex, nofollow" />
	</head>
	<body>
		<?php echo $mainbody; ?>
	</body>
</html>
<?php
extDoGzip();
?>