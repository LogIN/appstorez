<?php
// ensure this file is being included by a parent file
if( !defined( '_JEXEC' ) && !defined( '_VALID_MOS' ) ) die( 'Restricted access' );
function show_header($dirlinks='') {
	$url = str_replace( array('&dir=', '&action=', '&file_mode='), 
						array('&a=','&b=','&c='), 
						$_SERVER['REQUEST_URI'] );
	
	$url_appendix = strpos($url, '?') === false ? '?' : '&amp;';
	
	echo "<link rel=\"stylesheet\" href=\""._EXT_URL."/style/style.css\" type=\"text/css\" />\n";
	echo "<div id=\"ext_header\">\n";
	echo "<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"5\">\n";
	$mode = extGetParam( $_SESSION, 'file_mode', $GLOBALS['ext_conf']['authentication_method_default'] );
	$logoutlink = ' <a href="'.$GLOBALS['script_name'].'?option=com_extplorer&amp;action=logout" title="'.$GLOBALS['messages']['logoutlink'].'">['.$GLOBALS['messages']['logoutlink'].']</a>';
	$alternate_modes = array();
	foreach( $GLOBALS['ext_conf']['authentication_methods_allowed'] as $method ) {
		if( $method != $mode ) {
			$onclick = '';
			if( empty($_SESSION['credentials_'.$method])) {
				 $onclick = "onclick=\"openActionDialog('switch_file_mode', '".$method."_authentication');return false;\"";
			}
			$alternate_modes[] = "<a $onclick href=\"$url".$url_appendix."file_mode=$method\">$method</a>"; 
		}
	}
	echo "<tr><td style=\"color:black;\" width=\"10%\">";
	echo "<div style=\"margin-left:10px;float:right;\" width=\"305\" >";
	echo "</div>";
	echo "<td style=\"padding-left: 15px; color:black;\" id=\"bookmark_container\" width=\"35%\"></td>\n";
	echo "<td width=\"25%\" style=\"padding-left: 15px; color:black;\"></td>\n";

	echo '</tr></table>';
	echo '</div>';
}
//------------------------------------------------------------------------------
?>
