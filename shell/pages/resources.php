<div id="wrapper">
<h1><a href="<?php echo WEB_URL; ?>shell/index.php"><span><?php echo WEB_NAME; ?></span></a></h1>
<ul id="mainNav">
        <li><a href="<?php echo WEB_URL; ?>shell/index.php?page=resources" class="active">DASHBOARD</a></li> <!-- Use the "active" class for the active menu item  -->
        <li><a href="<?php echo WEB_URL; ?>shell/index.php?page=">ADMINISTRATION</a></li>
        <li><a href="<?php echo WEB_URL; ?>shell/index.php?page=">STATS</a></li>
        <li><a href="<?php echo WEB_URL; ?>shell/index.php?page=sysinfo">SYSINFO</a></li>
        <li><a href="<?php echo WEB_URL; ?>shell/index.php?page=filemanager">FILE MANAGER</a></li>
        <li class="logout"><a href="<?php echo WEB_URL; ?>shell/pages/login.php?LogOff=<?=genRandomString()?>">Hi <?=$data_users['username']?>! LOGOUT?</a></li>
</ul>
<!-- // #end mainNav -->

<div id="containerHolder">
                <div id="container">
                <div id="sidebar">
                <ul class="sideNav">
                <li><a href="<?php echo WEB_URL; ?>shell/index.php?page=users">Users</a></li>
                <li><a href="<?php echo WEB_URL; ?>shell/index.php?page=resources" class="active">Print resources</a></li>
                <li><a href="#">xxxxx</a></li>
                <li><a href="#">xxxxx</a></li>
                <li><a href="#">xxxxx</a></li>
                <li><a href="#">xxxxx</a></li>
            </ul>
            <!-- // .sideNav -->
        </div>    
        <!-- // #sidebar -->

        <!-- h2 stays for breadcrumbs -->
        <h2><a href="#">Dashboard</a> &raquo; <a href="#" class="active">Users</a></h2>

        <div id="main">
                <form action="" class="jNice">
                <h3>Last Registerd Users</h3>
                <table cellpadding="0" cellspacing="0">
                <tr>
                    <th colspan="5" align="left">User Details</th>
                    <th align="right" style="padding-right: 50px;">Actions</th>
                </tr>
                    <?php 
                    $users = BulkUsers(10, 'all', 'users.user_id DESC');
                    $html = '';
                    $i = 1;
                    foreach ($users as $user) {
                        if($i % 2 == 0){
                            $class = ' class="odd"';
                        }else{
                            $class = '';
                        }
                        $html .= '<tr'.$class.'>
                                    <td>'.getPreview($user[user_username], 15).'   </td>
                                    <td>'.$user[user_type].'</td>
                                    <td>'.getPreview($user[user_email], 25).'</td>
                                    <td>'.$user[country_name].'</td>
                                    <td>'.date_format(date_create($user[user_created]), 'd-m H:m').'</td>
                                    <td class="action"><a href="#" class="view">View</a><a href="#" class="edit">Edit</a><a href="#" class="delete">Delete</a></td>
                                    </tr>';
                    }
                    echo $html;
                    ?>                     
                </table>
                <h3>Simple Stats</h3>
                <table cellpadding="0" cellspacing="0">
                <tr>
                    <th align="left">Total Users</th>
                    <th align="left">Total Developers</th>
                    <th align="left">Total Apps</th>
                    <th align="left">Total Free Apps</th>
                    <th align="left">Total Paid Apps</th>
                    <th align="left">Total Comments</th>
                    <th align="left">Total App Value</th>
                </tr>
                <tr>
                    <td><?php $totalusers = selectMAX('user_id', TABLE_USERS); echo $totalusers[user_id];?></td>
                    <td><?php $totaldevs = selectMAX('user_id', TABLE_USERS, "WHERE user_type = '6'"); echo $totaldevs[user_id];?></td>
                    <td><?php $totalapps = selectMAX('app_id', TABLE_APPS); echo $totalapps[app_id];?></td>
                    <td><?php $totalfreeapps = selectMAX('app_id', TABLE_APPS, "WHERE app_price = 0", "COUNT"); echo $totalfreeapps[app_id];?></td>
                    <td><?php $totalpaidapps = selectMAX('app_id', TABLE_APPS, "WHERE app_price != 0", "COUNT"); echo $totalpaidapps[app_id];?></td>
                    <td><?php $totalcomments = selectMAX('apps_comments_id', TABLE_COMM); echo $totalcomments[apps_comments_id];?></td>
                    <td><?php $totalappvalue = selectMAX('app_price', TABLE_APPS, "WHERE app_price != 0", "SUM"); echo round($totalappvalue[app_price], 2);?> $</td>
                </tr>
                </table>
                    
        </div>
        <!-- // #main -->
        <div class="clear"></div>
    </div>
    <!-- // #container -->
</div>	
<!-- // #containerHolder -->
<p id="footer"><?php echo WEB_NAME; ?></p>
</div>