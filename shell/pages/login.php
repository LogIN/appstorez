<?php
require ('../../include/config.inc.php');
require (ROOT.'include/databse.inc.php');
/*=======================*/
/*===  DB Connection ====*/
/*=======================*/
$db = Database::obtain(DB_SERVER, DB_USER, DB_PASS, DB_DATABASE); 
$db->connect(); 

/*==================================*/
/*===  Include Global Functions ====*/
/*==================================*/
include ROOT.'include/functions.inc.php';

/*=================================================*/
/*== visitor.info.inc.php calculate visitor info: =*/
/*== SiteLanguage, Visitor IP, Orgin Country etc  =*/
/*=================================================*/
// TODO : FIX Calculate Desired LNG ($countryCode $UserCountryCode)
// MAKE LANGUAGE IN DIFFERENT URL like: www.url.com/en/content.com // NO COOKIES OR SESSIONS
// http://googlewebmastercentral.blogspot.com/2010/03/working-with-multilingual-websites.html
include ROOT.'include/visitor.info.inc.php';

/*==============================*/
/*=== Global Translation file ==*/
/*==============================*/
if (file_exists(ROOT."include/lng/".$siteLanguage.".php")) {
    include ROOT."include/lng/".$siteLanguage.".php";
}else{
    $siteLanguage = 'GB';
    include ROOT."include/lng/".$siteLanguage.".php";
}

/*=====================*/
/*=== Session Start ===*/
/*=====================*/
session_name(md5(WEB_NAME));
session_start();
/*=================*/
/*=== If LogOff ===*/
/*=================*/
if(isset($_GET['LogOff'])){ 
    
    $_SESSION = array();
    session_destroy();
    header("Location: ".WEB_URL."shell/pages/login.php");
    exit;
}
/*================*/
/*=== If LogIN ===*/
/*================*/
if($_POST['submit'] == 'Login'){
    // Checking whether the Login form has been submitted	
    $err = array();
    
    /*===============================*/
    /*=== Check if Uname and Pass ===*/
    /*===============================*/
    if(!$_POST['username'] || !$_POST['password']){
            $err[1] = FIELDS_ERROR;
            print_r($err[1]);
    }
    if(!count($err)){ 

            $_POST['password'] = hash('sha512', SEC_TOKEN.$_POST['password']);  
        
            /*=======================*/
            /*=== Get DB Instaince ==*/
            /*=======================*/
            $sql = "SELECT user_id, user_type, user_username, user_salt, user_verified FROM users WHERE user_username = '".$db->escape($_POST['username'])."' AND user_password = '".$db->escape($_POST['password'])."' LIMIT 1";
            $row = $db->query_first($sql);
            /*=================================*/
            /*=== If User Exsist and Data OK ==*/
            /*=================================*/
            if($db->affected_rows == 1 && $row['user_verified'] == 1)
            {
                if($row['user_type'] == 0 || $row['user_type'] == 1 || $row['user_type'] == 2 || $row['user_type'] == 3){
                    $_SESSION['id'] = $row['user_id'];
                    $_SESSION['type'] = $row['user_type'];
                    $_SESSION['username'] = $row['user_username']; 
                    $_SESSION['salt'] = $row['user_salt'];

                    $CookieTime = time()+21600;
                    setcookie(session_name(),session_id(), $CookieTime);

                    header("Location: ".WEB_URL."shell/index.php");
                    exit(0);
                }else{
                   $err[2] = "Invalid Action"; 
                }
                
            }else{
                $err[3] = U_P_INVALID;

            }
            /*===============*/
            /*=== DB Close ==*/
            /*===============*/
            $db->close();
        
    }
}

?>
<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<title><?php echo WEB_NAME; ?> Administration</title>
<link rel="stylesheet" type="text/css" href="http://appstorez.com/shell/pages/template/css/login.php" />
</head>
<body>
<div class="container">
	<section id="content">
		<form action="<?php echo WEB_URL."shell/pages/login.php";?>" method="post">
			<h1><?php echo WEB_NAME; ?></h1>
			<div>
                                <input type="text" placeholder="Username" required="" id="username"  name="username" />
			</div>
			<div>
                                <input type="password" placeholder="Password" required="" name="password" id="password" />
			</div>
			<div>
				<input type="submit" value="Login" name="submit">
                                <?php if(count($err)){ ?>
                                <a href="#"><?php print_r($err[3]);?></a>
                                <?php } ?>
			</div>
		</form><!-- form -->
		<div class="button">
			<a href="<?php echo WEB_URL; ?>">Visit <?php echo WEB_NAME; ?></a>
		</div><!-- button -->
	</section><!-- content -->
</div><!-- container -->
</body>
</html>